1
00:00:00,000 --> 00:00:09,680
 now let's go and see the different

2
00:00:03,980 --> 00:00:09,680
subsystems of HyperDbg device

3
00:00:09,840 --> 00:00:17,400
 every hybrid every hybrid energy

4
00:00:14,839 --> 00:00:21,119
subsystem consists of different

5
00:00:17,400 --> 00:00:24,500
implementation most of them you use VT-x

6
00:00:21,119 --> 00:00:28,740
or VMX features of the Intel processors

7
00:00:24,500 --> 00:00:32,700
but most of their completely designed

8
00:00:28,740 --> 00:00:37,800
separately and joined together work

9
00:00:32,700 --> 00:00:38,940
together to make make debugging

10
00:00:37,800 --> 00:00:41,820
possible

11
00:00:38,940 --> 00:00:44,940
so this is the general overview of the

12
00:00:41,820 --> 00:00:47,940
HyperDbg subsystem

13
00:00:44,940 --> 00:00:51,539
 they have a debugger program which

14
00:00:47,940 --> 00:00:55,320
has some of its parts in user mode and

15
00:00:51,539 --> 00:00:58,620
other parts are are in the kernel mode

16
00:00:55,320 --> 00:01:01,680
and we divided everything into the

17
00:00:58,620 --> 00:01:04,520
different excellent page tables I will

18
00:01:01,680 --> 00:01:04,520
explain it later

19
00:01:04,799 --> 00:01:10,020
and some of the parts are executed in

20
00:01:07,140 --> 00:01:12,659
naming suit mode while there is a

21
00:01:10,020 --> 00:01:15,659
connection between the guests and the

22
00:01:12,659 --> 00:01:19,439
horse the horse tries to debug the

23
00:01:15,659 --> 00:01:21,240
guest and this connection is a it's a

24
00:01:19,439 --> 00:01:23,700
Serial interface or can be other

25
00:01:21,240 --> 00:01:27,119
interfaces

26
00:01:23,700 --> 00:01:30,020
there's the interpreter in the guest and

27
00:01:27,119 --> 00:01:33,420
 there's a parser in the hospital

28
00:01:30,020 --> 00:01:37,259
these interface tries to connect the

29
00:01:33,420 --> 00:01:40,740
hospital guest so we can send our

30
00:01:37,259 --> 00:01:46,140
messages so we can send our debugging

31
00:01:40,740 --> 00:01:49,079
commands to the guests and ex and after

32
00:01:46,140 --> 00:01:51,600
that the guest tries to execute those

33
00:01:49,079 --> 00:01:54,180
commands so whenever the commands

34
00:01:51,600 --> 00:01:55,100
received by the guests that then we have

35
00:01:54,180 --> 00:01:58,200
different

36
00:01:55,100 --> 00:02:00,960
components to handle those commands like

37
00:01:58,200 --> 00:02:03,979
we have MMIO or PMIO dividing

38
00:02:00,960 --> 00:02:07,920
interrupt monitoring and also other

39
00:02:03,979 --> 00:02:11,000
subsystems that manage the hyper images

40
00:02:07,920 --> 00:02:15,300
high level behavior

41
00:02:11,000 --> 00:02:19,680
 there's also other parts in the guest

42
00:02:15,300 --> 00:02:21,680
like a different in hybrid vgv

43
00:02:19,680 --> 00:02:27,000
completely

44
00:02:21,680 --> 00:02:30,420
separated the VMX route memory from

45
00:02:27,000 --> 00:02:33,599
the user model it's like something 

46
00:02:30,420 --> 00:02:36,720
complex in design like you're not

47
00:02:33,599 --> 00:02:39,599
allowed to access a user mode memory

48
00:02:36,720 --> 00:02:41,459
from the remix root mode and these are

49
00:02:39,599 --> 00:02:44,640
these are the things that are handled

50
00:02:41,459 --> 00:02:47,099
through different subsystems

51
00:02:44,640 --> 00:02:49,819
in the debugger but it's not something

52
00:02:47,099 --> 00:02:53,340
that is exported to the user it's like

53
00:02:49,819 --> 00:02:56,160
 whenever the user wants to read an

54
00:02:53,340 --> 00:03:00,120
address through the script engine then

55
00:02:56,160 --> 00:03:02,459
it just sends a command and the the

56
00:03:00,120 --> 00:03:05,160
guest sub system will handle it and will

57
00:03:02,459 --> 00:03:10,739
eventually use some of these concepts to

58
00:03:05,160 --> 00:03:13,560
bring to make a good result and make the

59
00:03:10,739 --> 00:03:14,519
adequate result and send it back to the

60
00:03:13,560 --> 00:03:18,840
house

61
00:03:14,519 --> 00:03:21,180
don't worry about it these are we we are

62
00:03:18,840 --> 00:03:25,319
going to get familiar with these things

63
00:03:21,180 --> 00:03:28,739
later and yeah I just divided the

64
00:03:25,319 --> 00:03:31,680
different subsystems in a high over high

65
00:03:28,739 --> 00:03:34,980
level overview of debuggers and these

66
00:03:31,680 --> 00:03:36,180
are the subsystems that you see in

67
00:03:34,980 --> 00:03:39,959
hybrid switch

68
00:03:36,180 --> 00:03:43,920
okay now let's go and see one of the

69
00:03:39,959 --> 00:03:48,319
important parts of this session 

70
00:03:43,920 --> 00:03:51,420
and this is the operation mode

71
00:03:48,319 --> 00:03:55,860
different operation modes in hybrid

72
00:03:51,420 --> 00:03:58,680
reach as you see in the early slides the

73
00:03:55,860 --> 00:04:01,080
operation modes that are currently there

74
00:03:58,680 --> 00:04:04,620
are three operation modes in HyperDbg

75
00:04:01,080 --> 00:04:07,620
one of them is the VMI mode or virtual

76
00:04:04,620 --> 00:04:10,200
machine introspection mode it's

77
00:04:07,620 --> 00:04:12,840
exactly the same as local kernel

78
00:04:10,200 --> 00:04:16,260
debugging if you previously have an

79
00:04:12,840 --> 00:04:20,699
experience of using the windy widgets

80
00:04:16,260 --> 00:04:23,280
it works somehow like the local kernel

81
00:04:20,699 --> 00:04:27,120
debugging in this divider we also have

82
00:04:23,280 --> 00:04:30,419
debugger mode if I will explain them

83
00:04:27,120 --> 00:04:33,360
later but for now if we want to debug a

84
00:04:30,419 --> 00:04:35,400
VM machine or a physical machine we use

85
00:04:33,360 --> 00:04:38,639
the debugger mode and a hybrid division

86
00:04:35,400 --> 00:04:41,400
in the transparent mode the third mode

87
00:04:38,639 --> 00:04:44,660
which is transparent mode high producer

88
00:04:41,400 --> 00:04:48,560
just tries to bypass anti debug or

89
00:04:44,660 --> 00:04:53,520
anti-hypervisor okay me and my mode 

90
00:04:48,560 --> 00:04:56,060
it's like local debugging in both and it

91
00:04:53,520 --> 00:05:00,540
works in both kernel mode and user mode

92
00:04:56,060 --> 00:05:05,520
whenever you use it it that you don't

93
00:05:00,540 --> 00:05:07,080
need to have a separate machine like

94
00:05:05,520 --> 00:05:09,360
you can 

95
00:05:07,080 --> 00:05:12,360
execute everything in your current

96
00:05:09,360 --> 00:05:17,040
system and everything is fine now you

97
00:05:12,360 --> 00:05:20,600
can also connect a remote Machine by

98
00:05:17,040 --> 00:05:24,419
using a TCP TCP connection

99
00:05:20,600 --> 00:05:26,600
and make it a local debugging of that

100
00:05:24,419 --> 00:05:30,060
target machine

101
00:05:26,600 --> 00:05:32,759
generally in this mode of operation you

102
00:05:30,060 --> 00:05:35,820
can do everything except halting the

103
00:05:32,759 --> 00:05:38,880
kernel mode itself it's like it's a

104
00:05:35,820 --> 00:05:41,820
stand for Windows in local debugging of

105
00:05:38,880 --> 00:05:44,460
individual you can't break or you can't

106
00:05:41,820 --> 00:05:46,139
hold the entire system because it just

107
00:05:44,460 --> 00:05:49,380
breaks everything you cannot control

108
00:05:46,139 --> 00:05:52,160
your system the system that they're

109
00:05:49,380 --> 00:05:56,580
running the debugger

110
00:05:52,160 --> 00:05:59,580
and also it's it's pretty okay we can

111
00:05:56,580 --> 00:06:02,780
have as many as custom codes that we

112
00:05:59,580 --> 00:06:06,900
want or we can we're gonna have 

113
00:06:02,780 --> 00:06:09,780
as many scripts that we want

114
00:06:06,900 --> 00:06:12,240
and we can also inspect both kernel mode

115
00:06:09,780 --> 00:06:15,960
functions and user mode functions so

116
00:06:12,240 --> 00:06:18,960
it's like a VMI mode supports two

117
00:06:15,960 --> 00:06:22,380
actions the actions that is previously

118
00:06:18,960 --> 00:06:24,600
seen the previous slide it supports s

119
00:06:22,380 --> 00:06:27,660
script and custom code but it won't

120
00:06:24,600 --> 00:06:31,699
support break this mode of operation can

121
00:06:27,660 --> 00:06:36,600
also be used to 

122
00:06:31,699 --> 00:06:40,259
create a different logs from the

123
00:06:36,600 --> 00:06:43,259
functions from both scanner mode and

124
00:06:40,259 --> 00:06:45,840
user mode function so it's it aligns

125
00:06:43,259 --> 00:06:48,060
with the script engine it gives you a

126
00:06:45,840 --> 00:06:51,539
great ability and Powerful ability to

127
00:06:48,060 --> 00:06:53,160
monitor their system behavior in the

128
00:06:51,539 --> 00:06:57,199
cavity 

129
00:06:53,160 --> 00:07:01,199
for this VMI mode is that if you corrupt

130
00:06:57,199 --> 00:07:03,120
an OS structure or anything that you are

131
00:07:01,199 --> 00:07:06,720
not supposed to access in the operating

132
00:07:03,120 --> 00:07:09,360
system or any modifications are wrong

133
00:07:06,720 --> 00:07:14,300
then you definitely see a blue screen of

134
00:07:09,360 --> 00:07:17,720
death or abuse bsod because the your

135
00:07:14,300 --> 00:07:22,259
modifying your current machine

136
00:07:17,720 --> 00:07:24,060
if it fails then you will lose that

137
00:07:22,259 --> 00:07:27,300
debugging station and you have to

138
00:07:24,060 --> 00:07:30,300
restart your computer again and the

139
00:07:27,300 --> 00:07:35,419
debugger mode is the most powerful and

140
00:07:30,300 --> 00:07:38,280
the full feature mode of the hyper AWG

141
00:07:35,419 --> 00:07:41,300
currently at the time that I'm recording

142
00:07:38,280 --> 00:07:44,520
these as slides hybrid Vision only

143
00:07:41,300 --> 00:07:46,800
supports physical machines and supports

144
00:07:44,520 --> 00:07:50,180
VMware workstations nested

145
00:07:46,800 --> 00:07:54,960
virtualization it also supports

146
00:07:50,180 --> 00:07:57,180
different VMware products too but not

147
00:07:54,960 --> 00:07:59,699
it's not only limited to the I mean

148
00:07:57,180 --> 00:08:04,199
we're working station but in the future

149
00:07:59,699 --> 00:08:08,580
 this will support hyperving and hyper

150
00:08:04,199 --> 00:08:11,639
hyper and virtual box nested

151
00:08:08,580 --> 00:08:14,460
virtualization the thing about hypervis

152
00:08:11,639 --> 00:08:16,560
it's really hard to support it has a lot

153
00:08:14,460 --> 00:08:18,960
of things that should be implemented and

154
00:08:16,560 --> 00:08:20,900
should be considered by the VMM or by

155
00:08:18,960 --> 00:08:24,419
the HyperDbg

156
00:08:20,900 --> 00:08:26,460
so the reason why in the initial version

157
00:08:24,419 --> 00:08:30,180
don't support me and where is because

158
00:08:26,460 --> 00:08:32,339
it's easier and we we spend a lot of

159
00:08:30,180 --> 00:08:35,219
time on hyper-b but still it's the

160
00:08:32,339 --> 00:08:38,159
challenging part of supporting hyper-v

161
00:08:35,219 --> 00:08:42,000
and we are not fully supporting hyper-v

162
00:08:38,159 --> 00:08:44,700
yet but I think it's fine if you use a

163
00:08:42,000 --> 00:08:48,060
one core 

164
00:08:44,700 --> 00:08:50,420
a bang from machine hyperly but it's not

165
00:08:48,060 --> 00:08:55,080
generally recommended to use hyper-v yet

166
00:08:50,420 --> 00:08:58,560
also if you have a Serial cable or

167
00:08:55,080 --> 00:09:02,220
device or a virtual machine you can use

168
00:08:58,560 --> 00:09:04,500
it you can also use the name pipe to

169
00:09:02,220 --> 00:09:06,800
work with this debugger mode in the

170
00:09:04,500 --> 00:09:09,720
future versions We will also support

171
00:09:06,800 --> 00:09:13,320
network connections but for now it's

172
00:09:09,720 --> 00:09:16,380
only limited to using serial devices I

173
00:09:13,320 --> 00:09:19,620
also told you about the TCP connections

174
00:09:16,380 --> 00:09:21,300
whenever your connection over a TCP in

175
00:09:19,620 --> 00:09:23,600
the current version of the hybrid which

176
00:09:21,300 --> 00:09:27,600
is you can you can only

177
00:09:23,600 --> 00:09:31,019
debug the target machine in in the VMI

178
00:09:27,600 --> 00:09:34,560
motor it's like the target machine is in

179
00:09:31,019 --> 00:09:37,440
is running in VMI mode but we're gonna

180
00:09:34,560 --> 00:09:40,019
support in the future versions we'll

181
00:09:37,440 --> 00:09:42,120
support the debugging over the network

182
00:09:40,019 --> 00:09:45,600
connections it's exactly like what

183
00:09:42,120 --> 00:09:49,440
individually are currently using and we

184
00:09:45,600 --> 00:09:51,720
can also have the debugger mode in the

185
00:09:49,440 --> 00:09:53,459
by using a network connection but for

186
00:09:51,720 --> 00:09:56,040
now it's only possible through the

187
00:09:53,459 --> 00:09:58,019
stereo devices a caveat and of course

188
00:09:56,040 --> 00:10:00,540
the cavity for this method is that it

189
00:09:58,019 --> 00:10:02,880
needs a separate virtual or physical

190
00:10:00,540 --> 00:10:05,580
machine and the transparent mode is

191
00:10:02,880 --> 00:10:09,120
completely different from the

192
00:10:05,580 --> 00:10:11,459
previous modes HyperDbg in this mode

193
00:10:09,120 --> 00:10:13,519
tries to hide itself from density

194
00:10:11,459 --> 00:10:15,680
dividing and dividing and

195
00:10:13,519 --> 00:10:19,620
anti-hypervisor methods

196
00:10:15,680 --> 00:10:22,320
 you can start a transparent mode in

197
00:10:19,620 --> 00:10:25,620
both BMI mode and debugger mode there's

198
00:10:22,320 --> 00:10:28,980
no limitation for it and it won't reveal

199
00:10:25,620 --> 00:10:31,560
the person's life but hypervisor on a

200
00:10:28,980 --> 00:10:35,580
classic Delta timing methods or classic

201
00:10:31,560 --> 00:10:38,779
side channels for the cpuid or possible

202
00:10:35,580 --> 00:10:42,899
possibly other instructions

203
00:10:38,779 --> 00:10:47,519
of course it won't guarantee a 100

204
00:10:42,899 --> 00:10:52,940
percent of transparency but it makes 

205
00:10:47,519 --> 00:10:52,940
I need a substantially harder for 

206
00:10:53,180 --> 00:10:59,279
hypervisor methods to find this debugger

207
00:10:56,339 --> 00:11:02,720
and it's also an under the underactive

208
00:10:59,279 --> 00:11:06,240
development mushroom-like HyperDbg

209
00:11:02,720 --> 00:11:09,420
 just improves every time every

210
00:11:06,240 --> 00:11:12,540
time that you can't refined and new

211
00:11:09,420 --> 00:11:16,380
technique that detects HyperDbg

212
00:11:12,540 --> 00:11:21,200
 we will try to bypass them but for no

213
00:11:16,380 --> 00:11:21,200
 it's under active development

