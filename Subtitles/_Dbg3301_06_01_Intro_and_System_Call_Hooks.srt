1
00:00:00,280 --> 00:00:05,640
hi everyone welcome to the next part of

2
00:00:02,879 --> 00:00:08,599
the tutorial reversing with Hyper

3
00:00:05,640 --> 00:00:12,240
dbg in this part we're going to see how

4
00:00:08,599 --> 00:00:14,599
we can debug system calls and CS here is

5
00:00:12,240 --> 00:00:17,400
a quick overview of what we're going to

6
00:00:14,599 --> 00:00:21,840
see in this part we're going to see 

7
00:00:17,400 --> 00:00:27,359
system call hooks or or and C

8
00:00:21,840 --> 00:00:29,679
hooks then we dig into tracing

9
00:00:27,359 --> 00:00:31,360
Heaven's Gate mechanism and Heaven's

10
00:00:29,679 --> 00:00:33,680
Gate instruction in

11
00:00:31,360 --> 00:00:36,760
Windows another think is that we're

12
00:00:33,680 --> 00:00:39,520
going to analyze a a system call

13
00:00:36,760 --> 00:00:41,879
routine Windows system call routines

14
00:00:39,520 --> 00:00:46,480
to see what happens whenever a CIS call

15
00:00:41,879 --> 00:00:49,360
is executed in the Windows and we have a

16
00:00:46,480 --> 00:00:53,120
handson which is creating a basic

17
00:00:49,360 --> 00:00:56,559
system call Interceptor now let's see

18
00:00:53,120 --> 00:00:59,920
the details about system calls and how

19
00:00:56,559 --> 00:01:02,199
we can hook them okay as require

20
00:00:59,920 --> 00:01:05,760
probably know system calls are a

21
00:01:02,199 --> 00:01:08,600
programmatic way of requesting a

22
00:01:05,760 --> 00:01:11,200
special a privileged service from the

23
00:01:08,600 --> 00:01:15,040
operating systems for example when you

24
00:01:11,200 --> 00:01:18,320
want to contact when you want to 

25
00:01:15,040 --> 00:01:20,680
communicate with an IO device you

26
00:01:18,320 --> 00:01:25,200
probably you definitely don't have

27
00:01:20,680 --> 00:01:27,720
access to IO devices in Windows so

28
00:01:25,200 --> 00:01:30,560
you ask the operating system to perform

29
00:01:27,720 --> 00:01:33,640
this communication for you

30
00:01:30,560 --> 00:01:35,479
and the it's the responsibility of

31
00:01:33,640 --> 00:01:40,079
the operating system to check whether

32
00:01:35,479 --> 00:01:42,720
you have the needed privileges or not

33
00:01:40,079 --> 00:01:44,640
and this is why we have a system call

34
00:01:42,720 --> 00:01:46,280
because we want to request the service

35
00:01:44,640 --> 00:01:52,360
from the operating

36
00:01:46,280 --> 00:01:58,000
system in modern ex64 operating

37
00:01:52,360 --> 00:02:03,360
system a special instruction is 

38
00:01:58,000 --> 00:02:07,800
used for this purpose and it's the cisal

39
00:02:03,360 --> 00:02:12,319
instruction like all the other

40
00:02:07,800 --> 00:02:13,560
 features of HyperDbg CIS sculls or

41
00:02:12,319 --> 00:02:18,959


42
00:02:13,560 --> 00:02:21,239
events the these events are in in as you

43
00:02:18,959 --> 00:02:24,840
probably know from the previous sessions

44
00:02:21,239 --> 00:02:29,080
you can run scripts within events

45
00:02:24,840 --> 00:02:33,760
events or you can run your custom code

46
00:02:29,080 --> 00:02:36,319
and you have have the all the things

47
00:02:33,760 --> 00:02:39,840
that you can expect from an event in

48
00:02:36,319 --> 00:02:44,040
this system call events as well in

49
00:02:39,840 --> 00:02:48,400
HyperDbg this this describes how we

50
00:02:44,040 --> 00:02:51,360
implemented cisal this cisal

51
00:02:48,400 --> 00:02:55,840
interception mechanism in hyperd in

52
00:02:51,360 --> 00:03:01,000
hyperd we disabled or Set the

53
00:02:55,840 --> 00:03:05,200
cisal enable bit to zero in m s r

54
00:03:01,000 --> 00:03:08,799
EF  register in this model

55
00:03:05,200 --> 00:03:12,200
specific register so whenever CIS call

56
00:03:08,799 --> 00:03:13,360
or a CIS instruction is executed on

57
00:03:12,200 --> 00:03:19,080
entire

58
00:03:13,360 --> 00:03:20,519
system  a UD or an undefined

59
00:03:19,080 --> 00:03:25,680
exception is

60
00:03:20,519 --> 00:03:28,840
strong and then hyperd intercepts 

61
00:03:25,680 --> 00:03:33,080
this exception and tries to emulate the

62
00:03:28,840 --> 00:03:36,040
system call and C

63
00:03:33,080 --> 00:03:40,080
instructions and a limitation for

64
00:03:36,040 --> 00:03:43,599
this system for this event is that 

65
00:03:40,080 --> 00:03:45,319
you cannot use this event in whenever

66
00:03:43,599 --> 00:03:48,879
the PatchGuard is

67
00:03:45,319 --> 00:03:52,599
enabled if you want to just disable

68
00:03:48,879 --> 00:03:56,840
the PatchGuard you have to attach

69
00:03:52,599 --> 00:04:01,560
wind at the boot time on Windows so

70
00:03:56,840 --> 00:04:04,599
the PatchGuard is the disabled it's

71
00:04:01,560 --> 00:04:07,079
the the thing about PatchGuard is that

72
00:04:04,599 --> 00:04:09,840
it won't be disabled if you just disable

73
00:04:07,079 --> 00:04:12,599
the driver signature enforcement or just

74
00:04:09,840 --> 00:04:15,680
enable the local debuging by using wind

75
00:04:12,599 --> 00:04:19,239
you have to attach wind at the very

76
00:04:15,680 --> 00:04:22,840
first step when the system tries to boot

77
00:04:19,239 --> 00:04:27,160
the another thing is that hooking system

78
00:04:22,840 --> 00:04:30,800
calls are done by using the cisal

79
00:04:27,160 --> 00:04:33,520
command or this 

80
00:04:30,800 --> 00:04:35,000
bang cisal command or exclamation mark

81
00:04:33,520 --> 00:04:38,520
cisal

82
00:04:35,000 --> 00:04:44,000
command and another variant of these

83
00:04:38,520 --> 00:04:47,919
commands are cisal and cisal 2 and CIS 2

84
00:04:44,000 --> 00:04:51,639
which is a faster variant of the

85
00:04:47,919 --> 00:04:55,240
cisal command but in some cases it might

86
00:04:51,639 --> 00:04:59,199
cause a blue screen so it's generally

87
00:04:55,240 --> 00:05:02,240
recommended to use the first command 

88
00:04:59,199 --> 00:05:06,520
this just the simple cisal command do

89
00:05:02,240 --> 00:05:10,759
not use the cisal two command if

90
00:05:06,520 --> 00:05:14,240
you don't have a concern for the

91
00:05:10,759 --> 00:05:16,240
speed and if you want to know what

92
00:05:14,240 --> 00:05:18,160
what's the different what are

93
00:05:16,240 --> 00:05:21,560
differences between these two commands

94
00:05:18,160 --> 00:05:25,000
better to read the documentation but in

95
00:05:21,560 --> 00:05:28,520
 but if I want to briefly explain it

96
00:05:25,000 --> 00:05:31,080
whenever this the cisal 2 command

97
00:05:28,520 --> 00:05:33,840
just tries to ulate without accessing

98
00:05:31,080 --> 00:05:36,639
the memory it means that it won't

99
00:05:33,840 --> 00:05:40,800
check whether the instruction that

100
00:05:36,639 --> 00:05:46,199
caused the undefined exception is

101
00:05:40,800 --> 00:05:49,680
actually a cisal or not so it just 

102
00:05:46,199 --> 00:05:55,240
decides based on the situation based on

103
00:05:49,680 --> 00:05:59,000
the instruction pointer so it's

104
00:05:55,240 --> 00:06:02,280
generally safer to run the first varant

105
00:05:59,000 --> 00:06:05,000
the last note here is that even though

106
00:06:02,280 --> 00:06:07,440
it's possible but generally it's not a

107
00:06:05,000 --> 00:06:10,000
good idea to intercept all the system

108
00:06:07,440 --> 00:06:13,000
calls in the entire system because 

109
00:06:10,000 --> 00:06:16,039
there are hundreds and even thousands of

110
00:06:13,000 --> 00:06:19,240
system calls running on each second on

111
00:06:16,039 --> 00:06:22,160
Windows and probably other operating

112
00:06:19,240 --> 00:06:25,199
systems and it's not it's not really

113
00:06:22,160 --> 00:06:27,240
useful in most of the cases to intercept

114
00:06:25,199 --> 00:06:30,199
all of the cisal and it's generally not

115
00:06:27,240 --> 00:06:34,120
possible to create logs because you you

116
00:06:30,199 --> 00:06:37,199
will in just one second you can gener

117
00:06:34,120 --> 00:06:39,800
generate thousands of lcks so it's

118
00:06:37,199 --> 00:06:42,759
generally better to filter the system

119
00:06:39,800 --> 00:06:45,479
calls B based on your need by using the

120
00:06:42,759 --> 00:06:47,639
script engine using the conditional

121
00:06:45,479 --> 00:06:51,680
statements we have some examples

122
00:06:47,639 --> 00:06:55,120
about it later we will discuss it

123
00:06:51,680 --> 00:06:59,639
truly here's 

124
00:06:55,120 --> 00:07:04,240
the how you can how here's how you can

125
00:06:59,639 --> 00:07:07,120
use the cisal command if if you see

126
00:07:04,240 --> 00:07:12,199
here like all the other events it

127
00:07:07,120 --> 00:07:16,840
gets the P ID in hex format this is the

128
00:07:12,199 --> 00:07:20,520
process ID of the process that we try

129
00:07:16,840 --> 00:07:24,280
to intercept this system call we

130
00:07:20,520 --> 00:07:28,479
try to intercept each system calls and

131
00:07:24,280 --> 00:07:31,680
we have the script here in the script

132
00:07:28,479 --> 00:07:34,879
we just try to print the cisal number

133
00:07:31,680 --> 00:07:38,199
the cisal number is in rax register it's

134
00:07:34,879 --> 00:07:43,360
just a simple print that prints the

135
00:07:38,199 --> 00:07:46,520
cisal number in 64bit format and

136
00:07:43,360 --> 00:07:51,360
other parameters are also available on

137
00:07:46,520 --> 00:07:53,800
rcx rdx r8 and r9 because based on the

138
00:07:51,360 --> 00:07:56,680
fast call calling convention in

139
00:07:53,800 --> 00:07:58,680
Windows this is the way that Windows

140
00:07:56,680 --> 00:08:00,759
passes the parameters from the user mode

141
00:07:58,680 --> 00:08:05,120
to the current mod and also the rest of

142
00:08:00,759 --> 00:08:08,000
the parameters are available in a stack

143
00:08:05,120 --> 00:08:11,800
 again we have another example in

144
00:08:08,000 --> 00:08:15,840
this example we want to intercept the

145
00:08:11,800 --> 00:08:19,520
the the first argument or the only

146
00:08:15,840 --> 00:08:23,440
argument currently the only argument to

147
00:08:19,520 --> 00:08:27,319
the cisal command is a cisal number if

148
00:08:23,440 --> 00:08:31,080
you want to intercept just a specific 

149
00:08:27,319 --> 00:08:35,519
cisal number you can put its name 

150
00:08:31,080 --> 00:08:41,320
put its number here for example in

151
00:08:35,519 --> 00:08:43,959
this example we use 0x  55 in

152
00:08:41,320 --> 00:08:47,279
heximal format we just want to intercept

153
00:08:43,959 --> 00:08:50,519
this system call and in the in the

154
00:08:47,279 --> 00:08:56,040
system call I just try to debug or

155
00:08:50,519 --> 00:09:01,839
pause the entire debugger if the

156
00:08:56,040 --> 00:09:04,279
the Rd register is equal to 0 X10 and 

157
00:09:01,839 --> 00:09:07,880
r9 is not equal to

158
00:09:04,279 --> 00:09:11,480
0x5 this is this is how we check

159
00:09:07,880 --> 00:09:15,600
for the parameters as you see in

160
00:09:11,480 --> 00:09:20,839
the previous slide we passed in

161
00:09:15,600 --> 00:09:25,760
Windows pass the arguments in r RC

162
00:09:20,839 --> 00:09:32,000
RDS r8 and r9 so we can conclude that

163
00:09:25,760 --> 00:09:34,200
rdx is the second parameter and r9

164
00:09:32,000 --> 00:09:36,680
is the fourth parameter to the system

165
00:09:34,200 --> 00:09:40,959
called handle another important note

166
00:09:36,680 --> 00:09:44,519
about this cisal is that it it

167
00:09:40,959 --> 00:09:48,000
generally won't limit to a a special

168
00:09:44,519 --> 00:09:52,320
cisal or a special process so whenever

169
00:09:48,000 --> 00:09:55,240
you run cisal or CIS command you

170
00:09:52,320 --> 00:09:57,399
will events for the for these two

171
00:09:55,240 --> 00:09:59,760
instructions are triggered on the entire

172
00:09:57,399 --> 00:10:02,600
system so it makes

173
00:09:59,760 --> 00:10:05,320
system substantially slower but still

174
00:10:02,600 --> 00:10:07,560
you can use the system normally and you

175
00:10:05,320 --> 00:10:10,560
can debug your

176
00:10:07,560 --> 00:10:10,560
system

