1
00:00:00,060 --> 00:00:07,379
and I know another basic thing about

2
00:00:03,480 --> 00:00:11,160
debugger is that it's this is its

3
00:00:07,379 --> 00:00:15,540
ability to put breakpoints and setting

4
00:00:11,160 --> 00:00:18,180
break ones also true about the HyperDbg

5
00:00:15,540 --> 00:00:21,060
 breakpoints in hybrid which are

6
00:00:18,180 --> 00:00:25,619
exactly like wind dbg for setting

7
00:00:21,060 --> 00:00:28,019
breakpoints we use BP command one of

8
00:00:25,619 --> 00:00:30,840
the things to consider is that break

9
00:00:28,019 --> 00:00:35,420
points in high producing are not events

10
00:00:30,840 --> 00:00:39,320
I will truly explain about it but for no

11
00:00:35,420 --> 00:00:41,940
 as I mentioned here HyperDbg

12
00:00:39,320 --> 00:00:44,579
won't consider breakpoints as an event

13
00:00:41,940 --> 00:00:47,399
we have another specific event that

14
00:00:44,579 --> 00:00:49,260
exactly works like this for 

15
00:00:47,399 --> 00:00:52,860
breakpoints and it's called EPT hook

16
00:00:49,260 --> 00:00:56,219
and this is the event so basically it

17
00:00:52,860 --> 00:01:00,300
means that as the BP command is not a

18
00:00:56,219 --> 00:01:04,140
another event so it doesn't support the

19
00:01:00,300 --> 00:01:07,340
car the parameters like that we see in a

20
00:01:04,140 --> 00:01:10,799
previous session like PID or 

21
00:01:07,340 --> 00:01:14,640
is also different but something like

22
00:01:10,799 --> 00:01:18,360
core or a script is not supported in the

23
00:01:14,640 --> 00:01:21,420
BP command or the breakpoint

24
00:01:18,360 --> 00:01:24,420
BP sets the breakpoint on a target

25
00:01:21,420 --> 00:01:26,880
function PL shows a list of active

26
00:01:24,420 --> 00:01:30,180
enabled or disabled breakpoints BD

27
00:01:26,880 --> 00:01:33,060
disables a breakpoint b enables a

28
00:01:30,180 --> 00:01:36,000
previously in a disabled break one

29
00:01:33,060 --> 00:01:39,780
and install so PC which clears and

30
00:01:36,000 --> 00:01:42,720
remove the breakpoint yeah also you can

31
00:01:39,780 --> 00:01:45,600
use a BP read symbol commands like you

32
00:01:42,720 --> 00:01:48,060
can set breakpoints on the commands that

33
00:01:45,600 --> 00:01:50,040
you want but keep in mind you can also

34
00:01:48,060 --> 00:01:53,579
use the expressions too

35
00:01:50,040 --> 00:01:59,040
in this example I put a breakpoint on a

36
00:01:53,579 --> 00:02:01,460
ntx allocate pool we tag let's see if an

37
00:01:59,040 --> 00:02:01,460
action

38
00:02:02,100 --> 00:02:09,800
I will put I first I see 

39
00:02:07,259 --> 00:02:13,379
was

40
00:02:09,800 --> 00:02:17,239
used here

41
00:02:13,379 --> 00:02:17,239
I want to put a breakpoint here

42
00:02:17,599 --> 00:02:23,160
and I want to make it as an expression

43
00:02:20,640 --> 00:02:26,220
for example the ntx Advocate will detect

44
00:02:23,160 --> 00:02:29,580
is called here so I put a breakpoint

45
00:02:26,220 --> 00:02:31,260
here and plus a I can exactly directly

46
00:02:29,580 --> 00:02:34,620
put the

47
00:02:31,260 --> 00:02:37,140
this address here too but I want to use

48
00:02:34,620 --> 00:02:39,060
it as an expression so you can later use

49
00:02:37,140 --> 00:02:42,120
it in an expression form

50
00:02:39,060 --> 00:02:44,160
I I will study it as already available

51
00:02:42,120 --> 00:02:49,160
as you can see that I'm currently

52
00:02:44,160 --> 00:02:49,160
operating in the fourth core

53
00:02:51,500 --> 00:03:00,480
so close the credits process

54
00:02:56,879 --> 00:03:03,180
if I see the break point of by using

55
00:03:00,480 --> 00:03:05,040
the BL you can see that it's enabled

56
00:03:03,180 --> 00:03:07,980
and are working

57
00:03:05,040 --> 00:03:10,140
 so I continue and after continue as

58
00:03:07,980 --> 00:03:12,780
you can as you can imagine that this is

59
00:03:10,140 --> 00:03:16,440
a highly it's a function with a high

60
00:03:12,780 --> 00:03:18,780
rate of execution so I just get the

61
00:03:16,440 --> 00:03:21,900
break when hit immediately and says that

62
00:03:18,780 --> 00:03:24,780
the breakpoint one is hit

63
00:03:21,900 --> 00:03:27,659
and we are currently here

64
00:03:24,780 --> 00:03:30,659
can modify the registers or sync

65
00:03:27,659 --> 00:03:32,940
everything and also continue again and

66
00:03:30,659 --> 00:03:36,300
view the registers or

67
00:03:32,940 --> 00:03:39,239
things like this at last I wanna just

68
00:03:36,300 --> 00:03:42,959
clear this break one so I used its

69
00:03:39,239 --> 00:03:46,560
address so I use it ID along with the BC

70
00:03:42,959 --> 00:03:51,000
command which means that you should go

71
00:03:46,560 --> 00:03:54,060
and clear this break map so if I just

72
00:03:51,000 --> 00:03:58,400
press G again and continue to debugging

73
00:03:54,060 --> 00:04:02,340
you you can see that we won't see

74
00:03:58,400 --> 00:04:04,980
 anything about the current running

75
00:04:02,340 --> 00:04:08,599
Windows the end the break one is removed

76
00:04:04,980 --> 00:04:08,599
and it won't trigger anymore

