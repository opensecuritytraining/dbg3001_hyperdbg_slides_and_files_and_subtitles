1
00:00:00,179 --> 00:00:06,299
okay

2
00:00:01,680 --> 00:00:09,540
 in summary we used high producy in

3
00:00:06,299 --> 00:00:12,420
real world application we used it for a

4
00:00:09,540 --> 00:00:17,699
user mode scenario without assist call

5
00:00:12,420 --> 00:00:20,220
interception tricks and we also analyzed

6
00:00:17,699 --> 00:00:22,100
the network connection and we also

7
00:00:20,220 --> 00:00:26,160
sniffed the data

8
00:00:22,100 --> 00:00:28,740
of the network from the system calls

9
00:00:26,160 --> 00:00:31,980
and in the kernel mode part we also

10
00:00:28,740 --> 00:00:34,620
analyze the WinDbg and intercept

11
00:00:31,980 --> 00:00:38,040
many of its functionality use some of

12
00:00:34,620 --> 00:00:40,079
the automated s scripts to find and

13
00:00:38,040 --> 00:00:41,940
reverse some of the functionalities of

14
00:00:40,079 --> 00:00:45,480
individually

15
00:00:41,940 --> 00:00:48,059
and as you can as you probably know this

16
00:00:45,480 --> 00:00:49,680
is the last part of this series this

17
00:00:48,059 --> 00:00:54,960
series of tutorial

18
00:00:49,680 --> 00:01:00,180
and I think from now you can use hyper

19
00:00:54,960 --> 00:01:03,059
dvg more intelligently and efficiently

20
00:01:00,180 --> 00:01:04,760
to enhance your reverse engineering

21
00:01:03,059 --> 00:01:09,479
Journey

22
00:01:04,760 --> 00:01:12,180
as the final note for this tutorial I

23
00:01:09,479 --> 00:01:15,060
could say that HyperDbg is a big

24
00:01:12,180 --> 00:01:19,200
project and it requires a lot of time

25
00:01:15,060 --> 00:01:21,600
and efforts from the community and based

26
00:01:19,200 --> 00:01:24,720
on the current situation and the current

27
00:01:21,600 --> 00:01:27,420
number of developers and the Limited 

28
00:01:24,720 --> 00:01:29,640
time and resources we cannot develop

29
00:01:27,420 --> 00:01:31,460
every part of the high produces

30
00:01:29,640 --> 00:01:35,939
simultaneously

31
00:01:31,460 --> 00:01:38,460
so the new developers are always welcome

32
00:01:35,939 --> 00:01:43,020
to join and contribute to the project

33
00:01:38,460 --> 00:01:45,500
and please  whenever you want to

34
00:01:43,020 --> 00:01:49,680
open some discussions in the HyperDbg

35
00:01:45,500 --> 00:01:52,979
and if you want to assist on writing

36
00:01:49,680 --> 00:01:57,720
some of the features in HyperDbg and

37
00:01:52,979 --> 00:02:00,240
you heard some discussions about it to

38
00:01:57,720 --> 00:02:02,840
discuss about possible future

39
00:02:00,240 --> 00:02:06,000
assistance

40
00:02:02,840 --> 00:02:09,840
and another thing another good thing

41
00:02:06,000 --> 00:02:13,200
about HyperDbg is most of the time I

42
00:02:09,840 --> 00:02:15,599
see those who use HyperDbg of for

43
00:02:13,200 --> 00:02:18,900
example some issues happen to HyperDbg

44
00:02:15,599 --> 00:02:21,780
I I let the I let those who open the

45
00:02:18,900 --> 00:02:25,620
issue to fixation and they are pretty

46
00:02:21,780 --> 00:02:28,319
okay so I I we all believe that those

47
00:02:25,620 --> 00:02:31,080
who use HyperDbg all of them are

48
00:02:28,319 --> 00:02:34,500
professional computer programmers and of

49
00:02:31,080 --> 00:02:38,640
course reverse engineers and almost all

50
00:02:34,500 --> 00:02:42,420
of them can help in this project so I

51
00:02:38,640 --> 00:02:45,620
think it's time to go and add some

52
00:02:42,420 --> 00:02:45,620
contributions if you can

53
00:02:46,640 --> 00:02:51,900
will come if you want to contribute on

54
00:02:49,620 --> 00:02:57,000
this project

55
00:02:51,900 --> 00:03:02,400
and I think more I hope you enjoyed

56
00:02:57,000 --> 00:03:06,140
this series and I hope to see you guys

57
00:03:02,400 --> 00:03:09,000
in future tutorials

58
00:03:06,140 --> 00:03:10,920
make sure to reach me whenever you want

59
00:03:09,000 --> 00:03:13,680
 whenever if you have any question

60
00:03:10,920 --> 00:03:16,560
that I think that you think I can help I

61
00:03:13,680 --> 00:03:21,019
would be good I I would

62
00:03:16,560 --> 00:03:24,120
happy to help and nothing more

63
00:03:21,019 --> 00:03:29,780
thanks a lot thanks for watching this

64
00:03:24,120 --> 00:03:29,780
series and hope to see you soon goodbye

