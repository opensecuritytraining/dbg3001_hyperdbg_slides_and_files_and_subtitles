1
00:00:00,120 --> 00:00:07,080
no let me come to a very juicy part

2
00:00:04,560 --> 00:00:09,300
of the HyperDbg debugger and it's it's

3
00:00:07,080 --> 00:00:12,059
a stepping mechanism

4
00:00:09,300 --> 00:00:14,040
HyperDbg uses different methods for

5
00:00:12,059 --> 00:00:17,279
estimating mechanism it's it's really

6
00:00:14,040 --> 00:00:19,320
interesting and we have three types of

7
00:00:17,279 --> 00:00:21,840
in the current version of HyperDbg we

8
00:00:19,320 --> 00:00:24,660
have three types of estimate the first

9
00:00:21,840 --> 00:00:27,180
one is estepine which is a regular

10
00:00:24,660 --> 00:00:30,660
stepping I will explain in details later

11
00:00:27,180 --> 00:00:32,759
we have a step over and a new thing and

12
00:00:30,660 --> 00:00:36,239
a unique feature of HyperDbg is

13
00:00:32,759 --> 00:00:38,719
this is its in instrumentation and

14
00:00:36,239 --> 00:00:38,719
stepping

15
00:00:39,379 --> 00:00:48,180
I will explain each of them in details

16
00:00:44,540 --> 00:00:53,039
the regular stepping command which is

17
00:00:48,180 --> 00:00:54,840
implemented as T command is exactly

18
00:00:53,039 --> 00:00:57,780
like 

19
00:00:54,840 --> 00:00:59,760
what what is implemented in the

20
00:00:57,780 --> 00:01:02,579
individually and also other classic

21
00:00:59,760 --> 00:01:05,339
debuggers as you can see in the picture

22
00:01:02,579 --> 00:01:07,619
if if we are currently escaping through

23
00:01:05,339 --> 00:01:09,299
these instructions or instrumenting

24
00:01:07,619 --> 00:01:12,600
these instructions

25
00:01:09,299 --> 00:01:15,920
 the devices program at first executes

26
00:01:12,600 --> 00:01:19,920
this move Rex

27
00:01:15,920 --> 00:01:22,860
0x15 and then the next instruction it

28
00:01:19,920 --> 00:01:25,920
works like this it sets the r for lag

29
00:01:22,860 --> 00:01:28,280
registers the Trap flag and after

30
00:01:25,920 --> 00:01:31,080
each of them 

31
00:01:28,280 --> 00:01:33,119
exception is thrown and exception is

32
00:01:31,080 --> 00:01:36,000
handled through the exception bitmap of

33
00:01:33,119 --> 00:01:39,119
the VMCS don't worry about these details

34
00:01:36,000 --> 00:01:41,159
I will if you just not interested in the

35
00:01:39,119 --> 00:01:44,400
details I just want to explain it here

36
00:01:41,159 --> 00:01:47,100
but if you're not really interested 

37
00:01:44,400 --> 00:01:48,600
in the details you can escape and see

38
00:01:47,100 --> 00:01:51,659
the slides related to the

39
00:01:48,600 --> 00:01:53,880
instrumentation and skipping it's just I

40
00:01:51,659 --> 00:01:56,640
just want to show how it works by

41
00:01:53,880 --> 00:02:01,020
setting an exception with bitmap me mxd

42
00:01:56,640 --> 00:02:03,479
you can just after each instruction 

43
00:02:01,020 --> 00:02:07,740
exception is thrown and is handled by

44
00:02:03,479 --> 00:02:11,480
hybrid region so you can think about

45
00:02:07,740 --> 00:02:14,819
it that before each instructions you can

46
00:02:11,480 --> 00:02:17,340
definitely run anything else because

47
00:02:14,819 --> 00:02:19,800
everything like other debuggers like

48
00:02:17,340 --> 00:02:23,420
individually every other course and

49
00:02:19,800 --> 00:02:26,459
processes are continued and whenever 

50
00:02:23,420 --> 00:02:31,860
or program finds a chance to get

51
00:02:26,459 --> 00:02:33,360
executed again and stf is three TF is

52
00:02:31,860 --> 00:02:36,180
triggered after running the instruction

53
00:02:33,360 --> 00:02:39,300
we will be notified against

54
00:02:36,180 --> 00:02:43,739
but there are other processes in other

55
00:02:39,300 --> 00:02:47,660
cores and are also in our current core

56
00:02:43,739 --> 00:02:51,060
that might get a chance to get executed

57
00:02:47,660 --> 00:02:53,220
so this method doesn't guarantee that

58
00:02:51,060 --> 00:02:54,900
nothing happens in the system this is

59
00:02:53,220 --> 00:02:57,780
exactly like other devices for example

60
00:02:54,900 --> 00:02:59,760
if you see a syscall or instruction or

61
00:02:57,780 --> 00:03:01,940
system call instruction in the user

62
00:02:59,760 --> 00:03:06,060
mode you can easily escape it like

63
00:03:01,940 --> 00:03:08,040
exactly think T and after executing

64
00:03:06,060 --> 00:03:10,440
the ciscollet goes to the instruction

65
00:03:08,040 --> 00:03:12,840
after discarding the user mode and

66
00:03:10,440 --> 00:03:14,580
nothing happens just like syscall is

67
00:03:12,840 --> 00:03:17,040
handled in the kernel and nothing

68
00:03:14,580 --> 00:03:18,959
happened in the future mode please just

69
00:03:17,040 --> 00:03:23,040
see as simple as slipping the same is

70
00:03:18,959 --> 00:03:25,980
the true about P or P command which

71
00:03:23,040 --> 00:03:28,440
stands for a step over the only

72
00:03:25,980 --> 00:03:32,040
different thing from this instruction

73
00:03:28,440 --> 00:03:36,120
and the key command is that if it sees

74
00:03:32,040 --> 00:03:39,060
the call instruction then tries to put a

75
00:03:36,120 --> 00:03:42,560
hardware breakpoint there and continues

76
00:03:39,060 --> 00:03:46,440
the debugging until the hardware

77
00:03:42,560 --> 00:03:48,019
triggered so it just steps over the call

78
00:03:46,440 --> 00:03:51,840
instructions

79
00:03:48,019 --> 00:03:55,140
this is really interesting and I

80
00:03:51,840 --> 00:03:58,260
think as as I mentioned which is unique

81
00:03:55,140 --> 00:04:01,440
to the hypertevity is it's

82
00:03:58,260 --> 00:04:06,060
instrumentation as the pink or eye

83
00:04:01,440 --> 00:04:08,519
command this command it's

84
00:04:06,060 --> 00:04:12,120
completely hypervisor based command as

85
00:04:08,519 --> 00:04:18,739
it's implemented by using the monitor

86
00:04:12,120 --> 00:04:21,959
traps like a feature of the Intel VT-x

87
00:04:18,739 --> 00:04:25,080
and it's somehow transparent the MTF

88
00:04:21,959 --> 00:04:30,060
itself is transparent from the OS so

89
00:04:25,080 --> 00:04:33,180
what is MTS MTF is a feature in the

90
00:04:30,060 --> 00:04:36,660
Intel is a bit actually Whenever you set

91
00:04:33,180 --> 00:04:40,259
an MTF and continue the debugging or the

92
00:04:36,660 --> 00:04:43,020
guess the Guess will execute one

93
00:04:40,259 --> 00:04:47,520
instruction and after an instruction a

94
00:04:43,020 --> 00:04:50,340
VMX it happens so we will handle it 

95
00:04:47,520 --> 00:04:55,020
handle the next instructions by using 

96
00:04:50,340 --> 00:04:57,919
MCL it's a exactly as something exactly

97
00:04:55,020 --> 00:05:01,320
like the Trap flagged in a regular

98
00:04:57,919 --> 00:05:05,300
executing environment but this is a

99
00:05:01,320 --> 00:05:09,479
strap flag for the hypervisor this

100
00:05:05,300 --> 00:05:12,060
implementation as as you can imagine

101
00:05:09,479 --> 00:05:14,040
that is transparent from the user mode

102
00:05:12,060 --> 00:05:18,120
applications or from the operating

103
00:05:14,040 --> 00:05:20,880
system it's a and HyperDbg guarantees

104
00:05:18,120 --> 00:05:23,580
that no other course and other processes

105
00:05:20,880 --> 00:05:26,639
or threats get a chance to be executed

106
00:05:23,580 --> 00:05:30,120
guerns is that only one instruction from

107
00:05:26,639 --> 00:05:31,580
target thread get executed you can think

108
00:05:30,120 --> 00:05:34,500
about it again

109
00:05:31,580 --> 00:05:37,020
it's really helpful in debugging for

110
00:05:34,500 --> 00:05:39,900
example there are two threads in two

111
00:05:37,020 --> 00:05:43,680
different parts in two different classes

112
00:05:39,900 --> 00:05:46,740
or same processes that are will monitor

113
00:05:43,680 --> 00:05:49,680
each other to see whether they are

114
00:05:46,740 --> 00:05:52,199
working or not or whether they are

115
00:05:49,680 --> 00:05:54,479
operating or not and some of the

116
00:05:52,199 --> 00:05:57,000
anti-debagging methods use this method

117
00:05:54,479 --> 00:05:58,979
to for example detect that one the one

118
00:05:57,000 --> 00:06:01,440
thread is debugging so another thread

119
00:05:58,979 --> 00:06:03,840
tries to kill the kill those debugger or

120
00:06:01,440 --> 00:06:06,660
that thread or behave differently or

121
00:06:03,840 --> 00:06:09,060
something like this but in this scenario

122
00:06:06,660 --> 00:06:11,580
HyperDbg it just guarantees that

123
00:06:09,060 --> 00:06:15,120
nothing happens to your system

124
00:06:11,580 --> 00:06:16,699
only one instruction from one process of

125
00:06:15,120 --> 00:06:21,360
one threat

126
00:06:16,699 --> 00:06:24,960
and other cores I'll also keep kept

127
00:06:21,360 --> 00:06:27,840
in a whole set of four another really

128
00:06:24,960 --> 00:06:31,220
interesting thing about this 

129
00:06:27,840 --> 00:06:36,240
instrumentation and stepping is that it

130
00:06:31,220 --> 00:06:38,160
works so bridge to the kernel and user

131
00:06:36,240 --> 00:06:40,199
for example you can step through the

132
00:06:38,160 --> 00:06:44,400
instructions from the user mode to the

133
00:06:40,199 --> 00:06:46,680
kernel it's a really you can use a

134
00:06:44,400 --> 00:06:50,520
module for reverse engineering binders

135
00:06:46,680 --> 00:06:53,160
for example you want to know how a

136
00:06:50,520 --> 00:06:57,240
specific function or however specific

137
00:06:53,160 --> 00:07:00,300
system Cloud works in a C as a system 

138
00:06:57,240 --> 00:07:04,639
in the Windows system you just prepare

139
00:07:00,300 --> 00:07:06,979
everything in the user mode and

140
00:07:04,639 --> 00:07:10,199
just

141
00:07:06,979 --> 00:07:13,500
execute just trigger a breakpoint and

142
00:07:10,199 --> 00:07:15,780
after that execute a step through the

143
00:07:13,500 --> 00:07:18,780
instructions to find what are what are

144
00:07:15,780 --> 00:07:21,900
the exact functions that are responsible

145
00:07:18,780 --> 00:07:24,300
for supporting that spatial system curve

146
00:07:21,900 --> 00:07:26,759
this is how it works and also it's able

147
00:07:24,300 --> 00:07:31,580
to return from a system call for

148
00:07:26,759 --> 00:07:34,139
example you will end up executing a

149
00:07:31,580 --> 00:07:36,180
your system call and know you want to

150
00:07:34,139 --> 00:07:38,160
step through the instructions until you

151
00:07:36,180 --> 00:07:41,400
reach to the user mode again for example

152
00:07:38,160 --> 00:07:43,560
after a certain instruction the

153
00:07:41,400 --> 00:07:45,599
processor will need to return from the

154
00:07:43,560 --> 00:07:47,880
kernel to user mode and you can just

155
00:07:45,599 --> 00:07:51,919
continue a stepping need continue

156
00:07:47,880 --> 00:07:55,020
running the step assist instruction

157
00:07:51,919 --> 00:07:57,180
it's also true about the syscall whenever

158
00:07:55,020 --> 00:07:59,099
you want to go from the user mode to the

159
00:07:57,180 --> 00:08:03,660
carry-on mod you will execute as syscall

160
00:07:59,099 --> 00:08:06,900
or for example exception happens at it

161
00:08:03,660 --> 00:08:08,280
just continues its normal execution I

162
00:08:06,900 --> 00:08:13,020
have a

163
00:08:08,280 --> 00:08:15,500
I've made a very small program that just

164
00:08:13,020 --> 00:08:21,780
tries to

165
00:08:15,500 --> 00:08:24,479
 shows a very simple string an sdd

166
00:08:21,780 --> 00:08:28,220
would put like a printf HyperDbg

167
00:08:24,479 --> 00:08:32,520
debug test one and Hyper division device

168
00:08:28,220 --> 00:08:38,300
it just executes a single breakpoint 

169
00:08:32,520 --> 00:08:42,539
in a debug form after running this

170
00:08:38,300 --> 00:08:44,760
line of code break minus trigger so we

171
00:08:42,539 --> 00:08:46,740
will be notified and after that we can

172
00:08:44,760 --> 00:08:49,440
instrument through the instructions to

173
00:08:46,740 --> 00:08:50,880
reach to a point where printf is

174
00:08:49,440 --> 00:08:54,360
executed

175
00:08:50,880 --> 00:08:58,040
so I try to compile it and I will return

176
00:08:54,360 --> 00:08:58,040
back to my dividing

177
00:08:59,300 --> 00:09:02,660
here it is

178
00:09:07,320 --> 00:09:14,779


179
00:09:08,839 --> 00:09:14,779
copy and paste it on my market debuggee

180
00:09:15,300 --> 00:09:24,839
okay the hybrid which is running in

181
00:09:19,080 --> 00:09:27,600
the in this gas system and it's also

182
00:09:24,839 --> 00:09:30,060
as there's a break point here hyper

183
00:09:27,600 --> 00:09:32,760
division will definitely notice about

184
00:09:30,060 --> 00:09:35,399
this break one and probably how and

185
00:09:32,760 --> 00:09:39,360
definitely how everything is I just

186
00:09:35,399 --> 00:09:40,820
executed this application and as

187
00:09:39,360 --> 00:09:44,000
you can see everything is halted

188
00:09:40,820 --> 00:09:46,860
actually the 

189
00:09:44,000 --> 00:09:49,380
application it starts working on the

190
00:09:46,860 --> 00:09:52,860
back backslide of the system and as you

191
00:09:49,380 --> 00:09:55,860
can see we can see it it it's not

192
00:09:52,860 --> 00:09:59,279
appeared yet on a screen but the hybrid

193
00:09:55,860 --> 00:10:01,220
which is notified about it

194
00:09:59,279 --> 00:10:05,940
so

195
00:10:01,220 --> 00:10:07,920
 if I put that process here you can

196
00:10:05,940 --> 00:10:13,019
see that we are currently running the

197
00:10:07,920 --> 00:10:17,220
console applica this field is limited

198
00:10:13,019 --> 00:10:20,120
to C 16 bytes and the EPROCESS is

199
00:10:17,220 --> 00:10:24,000
located here and the process ID is here

200
00:10:20,120 --> 00:10:26,940
so I can just continue normal execution

201
00:10:24,000 --> 00:10:30,680
like putting a fee by stepping for

202
00:10:26,940 --> 00:10:30,680
installation by using the D command

203
00:10:36,080 --> 00:10:41,160
running it

204
00:10:38,600 --> 00:10:44,459
by using the instrumentation and

205
00:10:41,160 --> 00:10:46,620
sleeping for example I put you can

206
00:10:44,459 --> 00:10:52,079
specify the number of times that you

207
00:10:46,620 --> 00:10:55,760
want to execute the stepping

208
00:10:52,079 --> 00:10:55,760
instructions I will put

209
00:10:55,860 --> 00:11:01,740
maybe let's say

210
00:10:58,459 --> 00:11:04,320
300 and starts executing an instrument

211
00:11:01,740 --> 00:11:07,200
in the instructions we are currently in

212
00:11:04,320 --> 00:11:09,660
the user mode because as you can see

213
00:11:07,200 --> 00:11:13,140
the addresses that start with zero zero

214
00:11:09,660 --> 00:11:16,519
seven f f and this is the user mode

215
00:11:13,140 --> 00:11:18,420
address based on Ultra standing fee

216
00:11:16,519 --> 00:11:21,240
of the

217
00:11:18,420 --> 00:11:24,779
inter process source and we keep waiting

218
00:11:21,240 --> 00:11:27,779
until we reach to a point where we go to

219
00:11:24,779 --> 00:11:32,300
the kernel mode from the user mode

220
00:11:27,779 --> 00:11:32,300
and we will investigate that one

221
00:11:45,720 --> 00:11:49,500
you can also whenever you want you can

222
00:11:48,420 --> 00:11:54,000
press

223
00:11:49,500 --> 00:11:58,040
control plus C to stop this stepping

224
00:11:54,000 --> 00:12:02,100
mechanism if you think that your target

225
00:11:58,040 --> 00:12:03,600
needs to be stopped or you made a very

226
00:12:02,100 --> 00:12:06,720
big number

227
00:12:03,600 --> 00:12:10,100
for your rest the pink

228
00:12:06,720 --> 00:12:10,100
or things like this

229
00:12:35,160 --> 00:12:41,220
okay okay

230
00:12:37,399 --> 00:12:44,339
 let me just stop it as you can see we

231
00:12:41,220 --> 00:12:47,100
go through the kernel mode routines and

232
00:12:44,339 --> 00:12:51,240
here's where

233
00:12:47,100 --> 00:12:53,760
the user mode stopped executions or a

234
00:12:51,240 --> 00:12:56,220
system call happens we can see that

235
00:12:53,760 --> 00:12:58,920
the last instruction before entering the

236
00:12:56,220 --> 00:13:01,740
camera mode is the system call here

237
00:12:58,920 --> 00:13:04,820
there are also other instructions here

238
00:13:01,740 --> 00:13:08,639
that executed before the system call

239
00:13:04,820 --> 00:13:14,279
we can see that it moves eax or the

240
00:13:08,639 --> 00:13:19,320
system call number 0 x 8 so it's 

241
00:13:14,279 --> 00:13:21,200
a system cognizes numbers 0x 

242
00:13:19,320 --> 00:13:24,480
zero eight

243
00:13:21,200 --> 00:13:28,139
system call and after that you can see

244
00:13:24,480 --> 00:13:31,380
that it starts running anti car Ki

245
00:13:28,139 --> 00:13:32,720
system call 64 and it starts with the

246
00:13:31,380 --> 00:13:35,459
swap JS

247
00:13:32,720 --> 00:13:38,040
instructions and other instructions that

248
00:13:35,459 --> 00:13:40,260
are related to making making the system

249
00:13:38,040 --> 00:13:44,000
available for the device

250
00:13:40,260 --> 00:13:47,880
for for handling the system call

251
00:13:44,000 --> 00:13:50,760
so you can imagine that there are also

252
00:13:47,880 --> 00:13:53,459
other instructions that are here related

253
00:13:50,760 --> 00:13:56,100
to my system before making this

254
00:13:53,459 --> 00:13:58,680
available and you can see that there are

255
00:13:56,100 --> 00:14:03,420
different functions that are cutting

256
00:13:58,680 --> 00:14:06,480
here this is simply not possible by

257
00:14:03,420 --> 00:14:08,279
any other debuggers because they won't

258
00:14:06,480 --> 00:14:10,260
instrument from the user much the

259
00:14:08,279 --> 00:14:13,260
channel mode so you can trace your

260
00:14:10,260 --> 00:14:15,480
execution also if you know about the

261
00:14:13,260 --> 00:14:18,240
Heaven's Gate mechanism it's also

262
00:14:15,480 --> 00:14:22,579
possible to easily investigate here have

263
00:14:18,240 --> 00:14:24,320
engaged mechanism currently it is 

264
00:14:22,579 --> 00:14:26,639
64-bit

265
00:14:24,320 --> 00:14:28,740
application so it's not that there's two

266
00:14:26,639 --> 00:14:32,100
bit application to investigate the

267
00:14:28,740 --> 00:14:34,260
heavens but if you have a 32-bit

268
00:14:32,100 --> 00:14:38,040
applications you can also investigate

269
00:14:34,260 --> 00:14:41,279
the way that Windows tries to emulate or

270
00:14:38,040 --> 00:14:45,260
run a 32-bit application in a 64-bit

271
00:14:41,279 --> 00:14:48,860
operating system and

272
00:14:45,260 --> 00:14:48,860
have a research

273
00:14:49,620 --> 00:14:56,300
also hypertension tells you that this is

274
00:14:52,560 --> 00:14:59,699
the user multiple kernel mode 

275
00:14:56,300 --> 00:15:01,740
transfer hybrid also have the ability to

276
00:14:59,699 --> 00:15:04,860
tell you that currently it's running on

277
00:15:01,740 --> 00:15:06,899
a heaving Gates or a return from having

278
00:15:04,860 --> 00:15:10,100
it or

279
00:15:06,899 --> 00:15:13,740
it will notify you about this different

280
00:15:10,100 --> 00:15:16,220
 situations where you're currently

281
00:15:13,740 --> 00:15:16,220
running

