1
00:00:00,000 --> 00:00:06,660
for now let's see how we can trace

2
00:00:03,300 --> 00:00:10,160
Heaven's Gate mechanism in Windows we

3
00:00:06,660 --> 00:00:13,219
need x 

4
00:00:10,160 --> 00:00:15,320
x86 processor or

5
00:00:13,219 --> 00:00:19,619
32-bit processors

6
00:00:15,320 --> 00:00:22,800
 if a thread wants to switch

7
00:00:19,619 --> 00:00:26,240
between the compatibility mode to the

8
00:00:22,800 --> 00:00:27,800
long mode or the 64 bit mode

9
00:00:26,240 --> 00:00:31,320


10
00:00:27,800 --> 00:00:35,100
then a mechanism is used which is

11
00:00:31,320 --> 00:00:38,040
called Heaven's Gate this Heavens

12
00:00:35,100 --> 00:00:41,000
gate is located at the code segment

13
00:00:38,040 --> 00:00:45,480
selector of 0x 33

14
00:00:41,000 --> 00:00:50,000
and it identifies a call gate inside the

15
00:00:45,480 --> 00:00:50,000
global descriptor table or GGG table

16
00:00:50,719 --> 00:00:56,460
you can read more about it on the

17
00:00:53,520 --> 00:01:00,780
internet there are a lot of resources in

18
00:00:56,460 --> 00:01:04,799
which they talk about why Heaven

19
00:01:00,780 --> 00:01:09,080
gate must exist and this mechanism makes

20
00:01:04,799 --> 00:01:12,900
us able to run a 32-bit application on a

21
00:01:09,080 --> 00:01:15,900
64-bit operating system or on a 64-bit

22
00:01:12,900 --> 00:01:15,900
processor

23
00:01:16,460 --> 00:01:23,400
 the context switching process occurs

24
00:01:20,820 --> 00:01:27,780
multiple times throughout the lifestyle

25
00:01:23,400 --> 00:01:29,939
of about 64 process and it's essential

26
00:01:27,780 --> 00:01:34,080
for its compatibility with the Windows

27
00:01:29,939 --> 00:01:39,240
64 bit kernel in HyperDbg it's super

28
00:01:34,080 --> 00:01:42,060
easy to trace these mechanisms we have a

29
00:01:39,240 --> 00:01:45,540
command which we previously introduced

30
00:01:42,060 --> 00:01:48,979
it it's in instrumentation a stepping

31
00:01:45,540 --> 00:01:52,799
command or I command in HyperDbg

32
00:01:48,979 --> 00:01:55,259
and as we have access to both this

33
00:01:52,799 --> 00:01:58,799
Thread instructions we can see how

34
00:01:55,259 --> 00:02:02,520
Windows returns from Heaven's Gate and

35
00:01:58,799 --> 00:02:05,360
we will we can see that how they

36
00:02:02,520 --> 00:02:09,479
implement the C Street instruction

37
00:02:05,360 --> 00:02:10,879
in the compatibility mode so we have a

38
00:02:09,479 --> 00:02:16,319
demo of

39
00:02:10,879 --> 00:02:19,500
this tracing later but for now we

40
00:02:16,319 --> 00:02:23,879
want to analyze a Windows system call

41
00:02:19,500 --> 00:02:28,160
service routine this is just a brief

42
00:02:23,879 --> 00:02:31,500
explanation of how syscall instruction

43
00:02:28,160 --> 00:02:33,080
works before 

44
00:02:31,500 --> 00:02:37,739
before

45
00:02:33,080 --> 00:02:40,980
giving some examples I want to just

46
00:02:37,739 --> 00:02:44,060
inter just explain some of the basic

47
00:02:40,980 --> 00:02:47,640
concepts about the syscall instruction

48
00:02:44,060 --> 00:02:51,720
whenever a user with application tries

49
00:02:47,640 --> 00:02:55,920
to run syscall instruction Windows jobs 

50
00:02:51,720 --> 00:02:58,080
to the address located the address of

51
00:02:55,920 --> 00:03:02,599
Revenue is continues normal executions

52
00:02:58,080 --> 00:03:08,280
located at ia32 LS star

53
00:03:02,599 --> 00:03:10,800
MSR which is there and after executing a

54
00:03:08,280 --> 00:03:13,319
bunch of instructions it dispatches the

55
00:03:10,800 --> 00:03:15,440
address from the SSD table

56
00:03:13,319 --> 00:03:20,040
SSDT table

57
00:03:15,440 --> 00:03:23,000
SSDT or system service dispatch table is

58
00:03:20,040 --> 00:03:26,519
an array of addresses

59
00:03:23,000 --> 00:03:28,640
and it just shows the user mode

60
00:03:26,519 --> 00:03:30,200
applications

61
00:03:28,640 --> 00:03:34,920


62
00:03:30,200 --> 00:03:37,200
that what what is the exact routine

63
00:03:34,920 --> 00:03:40,440
in the kernel that is responsible for

64
00:03:37,200 --> 00:03:43,500
handling a special system call when a

65
00:03:40,440 --> 00:03:46,500
programming userspace calls of

66
00:03:43,500 --> 00:03:50,519
functions let's say create file or we

67
00:03:46,500 --> 00:03:53,940
want to execute create file a in the

68
00:03:50,519 --> 00:03:58,980
user mode then the code execution is

69
00:03:53,940 --> 00:04:02,220
transferred to a Unicode version of this

70
00:03:58,980 --> 00:04:07,379
 function and then it passes to the

71
00:04:02,220 --> 00:04:09,900
ntdll which is the user mode get of the

72
00:04:07,379 --> 00:04:13,319
system calls for the regular system

73
00:04:09,900 --> 00:04:18,380
calls in the Windows and you eventually

74
00:04:13,319 --> 00:04:22,860
call integrate five yes bisces called

75
00:04:18,380 --> 00:04:25,860
 and then a system call is executed

76
00:04:22,860 --> 00:04:27,780
and we will run some instruction which

77
00:04:25,860 --> 00:04:31,740
is which are related in the kernels

78
00:04:27,780 --> 00:04:34,139
which are related to finding the exact

79
00:04:31,740 --> 00:04:37,740
routine which is responsible for

80
00:04:34,139 --> 00:04:43,440
handling NtCreateFile and internal

81
00:04:37,740 --> 00:04:47,220
and in antimazole or ntos kernel

82
00:04:43,440 --> 00:04:50,580
and to create file the same name as

83
00:04:47,220 --> 00:04:53,520
ntdll in the kernel side is also

84
00:04:50,580 --> 00:04:55,460
responsible for handling the calls to

85
00:04:53,520 --> 00:04:59,479
the entry create one

86
00:04:55,460 --> 00:05:03,600
this call is merely an index in a system

87
00:04:59,479 --> 00:05:08,460
 service dispatch table which contains

88
00:05:03,600 --> 00:05:13,680
an array of pointers for a 32-bit Os or

89
00:05:08,460 --> 00:05:18,240
a relative offset to the SSDT for a

90
00:05:13,680 --> 00:05:20,220
64-bit arrest and well it's this address

91
00:05:18,240 --> 00:05:23,699
is available to all the critical system

92
00:05:20,220 --> 00:05:27,180
APIs like ZW create files and W open

93
00:05:23,699 --> 00:05:29,580
file and so on combined with Heaven's

94
00:05:27,180 --> 00:05:32,460
Gate we want to see a demo of HyperDbg

95
00:05:29,580 --> 00:05:33,860
how we can trace these mechanisms step

96
00:05:32,460 --> 00:05:39,000
by step

97
00:05:33,860 --> 00:05:40,979
so let's see the them okay we just

98
00:05:39,000 --> 00:05:44,460
want to see how we can use the

99
00:05:40,979 --> 00:05:45,800
instrumentation as the pin mechanism 

100
00:05:44,460 --> 00:05:50,759
to

101
00:05:45,800 --> 00:05:53,280
trace the hyper detectors the Windows

102
00:05:50,759 --> 00:05:57,120
routines which are which are related to

103
00:05:53,280 --> 00:06:01,500
the Heaven's Gate for this purpose I

104
00:05:57,120 --> 00:06:03,600
wrote as a very simple application 

105
00:06:01,500 --> 00:06:05,539
the source code of this affiliate this

106
00:06:03,600 --> 00:06:09,539
application is also available

107
00:06:05,539 --> 00:06:12,180
 on the website you can as you can see

108
00:06:09,539 --> 00:06:16,680
in this simple application I try to

109
00:06:12,180 --> 00:06:20,460
directly call a system call from

110
00:06:16,680 --> 00:06:23,180
ntdll function first I just print their

111
00:06:20,460 --> 00:06:26,759
process ID and the thread ID

112
00:06:23,180 --> 00:06:30,560
and after that we will cut after

113
00:06:26,759 --> 00:06:33,919
pressing the enter the entire

114
00:06:30,560 --> 00:06:37,340
application will run

115
00:06:33,919 --> 00:06:42,240
 reload ntdll

116
00:06:37,340 --> 00:06:45,180
as a module here and we find the address

117
00:06:42,240 --> 00:06:48,960
for NtCreateFile and enter write

118
00:06:45,180 --> 00:06:51,440
file and NtCloseFile close 

119
00:06:48,960 --> 00:06:55,860
function

120
00:06:51,440 --> 00:06:58,199
saved it here we also have their 

121
00:06:55,860 --> 00:07:01,199
definitions here

122
00:06:58,199 --> 00:07:06,180
definition of these functions here and

123
00:07:01,199 --> 00:07:09,080
at last I try to run just run this

124
00:07:06,180 --> 00:07:11,479
simple

125
00:07:09,080 --> 00:07:17,220
code that

126
00:07:11,479 --> 00:07:18,840
saves the hello world to a file name in

127
00:07:17,220 --> 00:07:21,780
my desktop which is called high

128
00:07:18,840 --> 00:07:25,160
producer.txt you can change its username

129
00:07:21,780 --> 00:07:28,860
to your username if you want to test it

130
00:07:25,160 --> 00:07:31,020
and this just makes the environment

131
00:07:28,860 --> 00:07:33,539
ready for the system call making the

132
00:07:31,020 --> 00:07:36,660
instruction making the structures ready

133
00:07:33,539 --> 00:07:40,020
then we have a breakpoint here because

134
00:07:36,660 --> 00:07:41,720
we want to test Heaven's Gate we have we

135
00:07:40,020 --> 00:07:45,080
put a

136
00:07:41,720 --> 00:07:49,800
breakpoint here it's just like a simple

137
00:07:45,080 --> 00:07:52,979
0xcc or in three instruction it acts

138
00:07:49,800 --> 00:07:56,759
like this and after that I try to run

139
00:07:52,979 --> 00:08:01,440
this system call and we will see how it

140
00:07:56,759 --> 00:08:03,900
works if it was successful then and then

141
00:08:01,440 --> 00:08:06,240
we continue our normal execution which

142
00:08:03,900 --> 00:08:08,940
we eventually want to put the hello

143
00:08:06,240 --> 00:08:12,060
world on this file but if it was not

144
00:08:08,940 --> 00:08:15,840
successful then failed to create file

145
00:08:12,060 --> 00:08:18,720
with the error number is shown here

146
00:08:15,840 --> 00:08:20,759
and the problem the program exceeds with

147
00:08:18,720 --> 00:08:24,960
an error

148
00:08:20,759 --> 00:08:29,160
so here it is it's just this is just

149
00:08:24,960 --> 00:08:32,099
a simple example so let's run it

150
00:08:29,160 --> 00:08:33,419
and or

151
00:08:32,099 --> 00:08:35,419
machine

152
00:08:33,419 --> 00:08:35,419


153
00:08:35,820 --> 00:08:42,919
I just try to build this and also 

154
00:08:40,500 --> 00:08:46,440
note that this is a 32-bit application

155
00:08:42,919 --> 00:08:50,820
because I want to show you the heavens

156
00:08:46,440 --> 00:08:54,000
gate I compiled it in 32-bit mode if

157
00:08:50,820 --> 00:08:58,040
you I just compiled it in 64-bit mode

158
00:08:54,000 --> 00:08:58,040
then there there was no Heaven's Gate

159
00:08:58,459 --> 00:09:06,560
 I attached the the

160
00:09:03,200 --> 00:09:11,420
miniviji here 

161
00:09:06,560 --> 00:09:11,420
we just run or Windows

162
00:09:14,820 --> 00:09:19,820
find the executable files related

163
00:09:26,040 --> 00:09:29,300
yeah here it is

164
00:09:39,260 --> 00:09:42,500
you know

165
00:09:48,600 --> 00:09:55,680
I also compile HyperDbg you can use the

166
00:09:52,860 --> 00:09:58,700
HyperDbg

167
00:09:55,680 --> 00:09:58,700
binary parts

168
00:10:00,420 --> 00:10:06,779
again we try to connect the target

169
00:10:03,180 --> 00:10:06,779
system 

170
00:10:21,480 --> 00:10:31,200
and yeah it's connected

171
00:10:24,959 --> 00:10:35,000
 after that I run

172
00:10:31,200 --> 00:10:35,000
this program

173
00:10:37,980 --> 00:10:44,040
and

174
00:10:40,560 --> 00:10:47,640
it's not executed yet so

175
00:10:44,040 --> 00:10:50,220
it says that press any key to continue

176
00:10:47,640 --> 00:10:52,860
so I press enter here and as you can see

177
00:10:50,220 --> 00:10:55,079
that the system is now locked or the

178
00:10:52,860 --> 00:10:58,200
system is not halted

179
00:10:55,079 --> 00:11:01,940
and we pass the execution back to the

180
00:10:58,200 --> 00:11:08,880
hybrid uge here so we encountered

181
00:11:01,940 --> 00:11:12,140
 Cc or E3 instructions and now we can

182
00:11:08,880 --> 00:11:15,800
continue our normal executions

183
00:11:12,140 --> 00:11:18,480
I just tried to run 

184
00:11:15,800 --> 00:11:21,720
instrumentation a stepping command to

185
00:11:18,480 --> 00:11:25,279
single step through the instructions so

186
00:11:21,720 --> 00:11:25,279
a simple I command

187
00:11:26,360 --> 00:11:33,420
and the next instruction is this so I

188
00:11:29,579 --> 00:11:37,320
try to run more commands like

189
00:11:33,420 --> 00:11:41,220
100 commands and still we are in the

190
00:11:37,320 --> 00:11:44,959
user mode but as you mentioned here

191
00:11:41,220 --> 00:11:48,060
let's see let's see the source code

192
00:11:44,959 --> 00:11:52,820
first in the source code as you can see

193
00:11:48,060 --> 00:11:52,820
here we are calling NtCreateFile

194
00:11:53,540 --> 00:12:01,440
there is a website in this link here

195
00:11:59,480 --> 00:12:05,339


196
00:12:01,440 --> 00:12:08,760
that you can see there

197
00:12:05,339 --> 00:12:11,399
system call numbers it's not currently

198
00:12:08,760 --> 00:12:13,680
available at the time that I'm recording

199
00:12:11,399 --> 00:12:17,579
it's not available for Windows 11

200
00:12:13,680 --> 00:12:20,579
however it's this special system because

201
00:12:17,579 --> 00:12:23,040
it's the same as Windows 10 so I just

202
00:12:20,579 --> 00:12:24,779
search for NtCreateFile and as you

203
00:12:23,040 --> 00:12:26,160
can see

204
00:12:24,779 --> 00:12:28,440
here

205
00:12:26,160 --> 00:12:33,480
the

206
00:12:28,440 --> 00:12:39,720
syscall number for indicate file

207
00:12:33,480 --> 00:12:46,079
remains the same to 0x55 in Windows 10.

208
00:12:39,720 --> 00:12:48,839
so here we're gonna intercept the 55 

209
00:12:46,079 --> 00:12:53,339
system calling Windows

210
00:12:48,839 --> 00:12:55,579
now let's go back to this

211
00:12:53,339 --> 00:12:58,940
code and

212
00:12:55,579 --> 00:13:03,600
this debugger session

213
00:12:58,940 --> 00:13:06,779
here we run a instruction the call

214
00:13:03,600 --> 00:13:08,300
instructions here and as you can see

215
00:13:06,779 --> 00:13:13,279
somewhere here

216
00:13:08,300 --> 00:13:17,700
the Windows or ntdli tries to put

217
00:13:13,279 --> 00:13:19,680
0x55 or 55 on eax which represents the

218
00:13:17,700 --> 00:13:23,279
system call number

219
00:13:19,680 --> 00:13:30,720
and then we call the Eds and here we

220
00:13:23,279 --> 00:13:35,880
have a far jump which on GDT 33 entry

221
00:13:30,720 --> 00:13:37,820
so it just changes the execution from 32

222
00:13:35,880 --> 00:13:41,339
bits to

223
00:13:37,820 --> 00:13:45,060
64-bit and in the 64-bit you can see

224
00:13:41,339 --> 00:13:47,839
that these instructions are running in

225
00:13:45,060 --> 00:13:52,620
Windows Windows tries to

226
00:13:47,839 --> 00:13:56,220
 save some of the registers here and

227
00:13:52,620 --> 00:13:58,860
after that we are running in a 64-bit

228
00:13:56,220 --> 00:14:01,680
mode of the processor here we don't have

229
00:13:58,860 --> 00:14:03,839
access to the 64-bit registers and the

230
00:14:01,680 --> 00:14:05,040
execution is incompatibility mode

231
00:14:03,839 --> 00:14:09,540
however

232
00:14:05,040 --> 00:14:11,399
here we are in 64-bit mode and as you

233
00:14:09,540 --> 00:14:14,360
can see that the addresses will

234
00:14:11,399 --> 00:14:14,360
eventually change

235
00:14:16,639 --> 00:14:23,720
eventually change to the 64-bit

236
00:14:19,680 --> 00:14:26,060
addresses and this is how Windows

237
00:14:23,720 --> 00:14:29,100
executes the

238
00:14:26,060 --> 00:14:31,339
Heaven's Gate mechanism and you can you

239
00:14:29,100 --> 00:14:34,860
can just try continue

240
00:14:31,339 --> 00:14:37,740
the execution until you reach the kernel

241
00:14:34,860 --> 00:14:40,459
I think saw it in the previous sessions

242
00:14:37,740 --> 00:14:45,019
but let's just continue

243
00:14:40,459 --> 00:14:45,019
the execution from here

244
00:14:45,839 --> 00:14:54,000
so yeah I just paused it here and I as

245
00:14:49,680 --> 00:14:57,899
you can see a bunch of instructions are

246
00:14:54,000 --> 00:15:00,180
running here and at last Windows runs

247
00:14:57,899 --> 00:15:03,240
a syscall instructions and after the

248
00:15:00,180 --> 00:15:06,660
syscall instruction the execution is

249
00:15:03,240 --> 00:15:09,120
more is changed from the user mode to

250
00:15:06,660 --> 00:15:12,180
the kernel mode we have a swapgs

251
00:15:09,120 --> 00:15:18,079
instructions and the these instructions

252
00:15:12,180 --> 00:15:20,639
are related to converting the

253
00:15:18,079 --> 00:15:22,459
for dispatching the system because if

254
00:15:20,639 --> 00:15:27,180
you want to just see them

255
00:15:22,459 --> 00:15:29,579
 function names you can also load

256
00:15:27,180 --> 00:15:31,740
the symbols in this session I didn't

257
00:15:29,579 --> 00:15:35,480
load the symbol

258
00:15:31,740 --> 00:15:35,480
so let's just make the

259
00:15:35,540 --> 00:15:39,139
symbol table

260
00:15:39,959 --> 00:15:43,040
I think I

261
00:15:43,680 --> 00:15:49,279
don't have

262
00:15:46,079 --> 00:15:49,279
let's start

263
00:15:52,079 --> 00:16:00,920
okay the system call numbers

264
00:15:55,980 --> 00:16:00,920
the symbol is not available

265
00:16:10,220 --> 00:16:16,680
yeah no no we have the 

266
00:16:13,740 --> 00:16:21,899
steam balls and 

267
00:16:16,680 --> 00:16:25,040
 the symbols are now configuring or 

268
00:16:21,899 --> 00:16:25,040
debugger session

