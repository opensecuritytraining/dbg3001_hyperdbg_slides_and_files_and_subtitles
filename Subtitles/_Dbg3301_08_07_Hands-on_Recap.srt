1
00:00:00,140 --> 00:00:06,359
now the next section is the hands-on

2
00:00:03,540 --> 00:00:09,059
section in the hands-on section we want

3
00:00:06,359 --> 00:00:11,519
to monitor some of the some of the

4
00:00:09,059 --> 00:00:14,160
interrupts especially clock interrupts

5
00:00:11,519 --> 00:00:17,520
and after that you want to trace the

6
00:00:14,160 --> 00:00:20,300
contents of the apps in user mode and

7
00:00:17,520 --> 00:00:23,279
trace the interrupt handlers

8
00:00:20,300 --> 00:00:26,779
then we want to change the flow of a

9
00:00:23,279 --> 00:00:29,939
program perform some page table level

10
00:00:26,779 --> 00:00:32,820
modification and change the codes and

11
00:00:29,939 --> 00:00:36,440
then return to the previous code so

12
00:00:32,820 --> 00:00:36,440
let's see let's

13
00:00:43,320 --> 00:00:49,399
I again try to connect to HyperDbg

14
00:01:13,100 --> 00:01:20,580
for the for this hands and I made two

15
00:01:17,640 --> 00:01:25,200
application named

16
00:01:20,580 --> 00:01:27,979
process a and process B

17
00:01:25,200 --> 00:01:27,979


18
00:01:29,280 --> 00:01:36,479
 let's try to close them

19
00:01:32,640 --> 00:01:39,240
yeah process a is just a simple

20
00:01:36,479 --> 00:01:42,720
thread application a simple

21
00:01:39,240 --> 00:01:46,619
application that creates a thread

22
00:01:42,720 --> 00:01:50,579
 shows its process ID creates a simple

23
00:01:46,619 --> 00:01:53,960
thread and waits for the thread to for

24
00:01:50,579 --> 00:01:59,159
the infinite thread to just 

25
00:01:53,960 --> 00:02:02,220
closed or just end the execution so

26
00:01:59,159 --> 00:02:06,000
and in the target trade it just tries

27
00:02:02,220 --> 00:02:10,319
to run some loops let's spend some times

28
00:02:06,000 --> 00:02:12,720
in the user mode and after that it shows

29
00:02:10,319 --> 00:02:14,459
some messages along with the special

30
00:02:12,720 --> 00:02:17,400
index

31
00:02:14,459 --> 00:02:19,280
and this is an infinite loop that tries

32
00:02:17,400 --> 00:02:23,840
to continue

33
00:02:19,280 --> 00:02:29,640
infinitely years and execution of this

34
00:02:23,840 --> 00:02:33,840
 process it just tries to show some it

35
00:02:29,640 --> 00:02:38,400
just tries to show some numbers

36
00:02:33,840 --> 00:02:41,120
and the second application or process

37
00:02:38,400 --> 00:02:41,120
B

38
00:02:43,620 --> 00:02:50,300
 this is a simple shell code for

39
00:02:47,220 --> 00:02:50,300
showing 

40
00:02:52,800 --> 00:02:57,780


41
00:02:54,480 --> 00:03:00,180
the message box I derived or copied the

42
00:02:57,780 --> 00:03:03,660
the show code from here a little bit

43
00:03:00,180 --> 00:03:07,500
modified the how this shell code

44
00:03:03,660 --> 00:03:10,800
works is out of the content scope of

45
00:03:07,500 --> 00:03:13,800
this tutorial but if you want to learn

46
00:03:10,800 --> 00:03:16,920
more you can read this address

47
00:03:13,800 --> 00:03:22,140
 this is basically the shell code that

48
00:03:16,920 --> 00:03:25,379
tries to find some functions from

49
00:03:22,140 --> 00:03:27,840
kernels the third two and eventually

50
00:03:25,379 --> 00:03:31,280
find the the

51
00:03:27,840 --> 00:03:36,300
message box function from the target dll

52
00:03:31,280 --> 00:03:38,819
and after that it tries to run run the

53
00:03:36,300 --> 00:03:41,159
message box

54
00:03:38,819 --> 00:03:43,080
also there are some routines here that I

55
00:03:41,159 --> 00:03:44,940
didn't use this in the shell code but in

56
00:03:43,080 --> 00:03:49,200
case if you want to save or restore

57
00:03:44,940 --> 00:03:52,620
register you can use these 

58
00:03:49,200 --> 00:03:54,840
 codes that basically push some of the

59
00:03:52,620 --> 00:03:57,420
instructions some of the registers into

60
00:03:54,840 --> 00:04:00,540
the spec can eventually pop them from

61
00:03:57,420 --> 00:04:04,379
the stack and this is an infinite loop

62
00:04:00,540 --> 00:04:07,500
 this just tries to infinitely run

63
00:04:04,379 --> 00:04:10,680
some instruction I try to use it you

64
00:04:07,500 --> 00:04:12,120
will find find out why we used it

65
00:04:10,680 --> 00:04:16,519
because we just want to keep the

66
00:04:12,120 --> 00:04:21,900
execution in user mode I added at the

67
00:04:16,519 --> 00:04:24,900
bottom of this circle so when the

68
00:04:21,900 --> 00:04:28,020
when the message box is shown then the

69
00:04:24,900 --> 00:04:30,600
process the processor just tries to run

70
00:04:28,020 --> 00:04:35,160
some infinix loop in the process speed

71
00:04:30,600 --> 00:04:37,699
so if I try to run it here you can see

72
00:04:35,160 --> 00:04:37,699
that

73
00:04:38,040 --> 00:04:45,840
it's a breakpoint here

74
00:04:40,320 --> 00:04:48,540
it shows yes pound 

75
00:04:45,840 --> 00:04:49,740
and yeah it just tries to continue the

76
00:04:48,540 --> 00:04:52,320
execution

77
00:04:49,740 --> 00:04:54,180
you will you will you will know why we

78
00:04:52,320 --> 00:04:57,419
just continue the execution because we

79
00:04:54,180 --> 00:05:00,240
want this program to just sustain the

80
00:04:57,419 --> 00:05:02,400
user mode so we can intercept it can 

81
00:05:00,240 --> 00:05:06,060
context

82
00:05:02,400 --> 00:05:09,540
I know a lot to know let's try to 

83
00:05:06,060 --> 00:05:13,680
move to the target

84
00:05:09,540 --> 00:05:17,060
VM and test 

85
00:05:13,680 --> 00:05:17,060
these processes

86
00:05:33,960 --> 00:05:42,180
so this is the process idable process 

87
00:05:38,820 --> 00:05:45,840
what process a and this is also the

88
00:05:42,180 --> 00:05:48,840
process ID for process B so we see the

89
00:05:45,840 --> 00:05:51,199
message here let's try to copy the

90
00:05:48,840 --> 00:05:55,039
message

91
00:05:51,199 --> 00:05:58,320
 I make

92
00:05:55,039 --> 00:06:00,660
an a script here I will let you know

93
00:05:58,320 --> 00:06:03,840
what is this a script I will try to

94
00:06:00,660 --> 00:06:07,100
describe it and this is also the plus ID

95
00:06:03,840 --> 00:06:07,100
for the second course

96
00:06:08,300 --> 00:06:16,699
and let's pause the target debuggee and

97
00:06:12,900 --> 00:06:16,699
return to the 

98
00:06:17,660 --> 00:06:24,740
so basically here we want to intercept

99
00:06:21,300 --> 00:06:24,740
the clock interrupts

100
00:06:25,560 --> 00:06:29,780
let's just try to separate them

101
00:06:33,180 --> 00:06:42,180
 as you can see here the monitor 

102
00:06:38,280 --> 00:06:44,759
D1 interrupts which which we basically

103
00:06:42,180 --> 00:06:47,720
know that relates to the clock

104
00:06:44,759 --> 00:06:47,720
interrupts

105
00:06:48,780 --> 00:06:56,460
and in case of any clock interrupt in

106
00:06:53,160 --> 00:06:58,139
this process or in process a we'll check

107
00:06:56,460 --> 00:07:00,600
whether

108
00:06:58,139 --> 00:07:05,220
rip matches with

109
00:07:00,600 --> 00:07:07,319
this mask or not if it matches the then

110
00:07:05,220 --> 00:07:10,819
it means that the clock interrupt is

111
00:07:07,319 --> 00:07:10,819
received at the kernel

112
00:07:11,300 --> 00:07:17,639
so it just tries to continue the

113
00:07:14,280 --> 00:07:20,400
execution without any Interruption

114
00:07:17,639 --> 00:07:24,000
without pausing the debugger but in case

115
00:07:20,400 --> 00:07:27,720
if it didn't match to this pattern or to

116
00:07:24,000 --> 00:07:30,240
this mask bits then it's a probably a

117
00:07:27,720 --> 00:07:33,360
user mode 

118
00:07:30,240 --> 00:07:35,699
 module or the

119
00:07:33,360 --> 00:07:39,979
interrupt or the clock interrupt

120
00:07:35,699 --> 00:07:42,720
received at the user mode part of the

121
00:07:39,979 --> 00:07:46,740
process so the user would receive the

122
00:07:42,720 --> 00:07:49,220
clock interrupt that's why we put

123
00:07:46,740 --> 00:07:52,440
some in infinite loops

124
00:07:49,220 --> 00:07:56,759
in the shell code or we put some

125
00:07:52,440 --> 00:08:00,120
infinite execution here we put it here

126
00:07:56,759 --> 00:08:02,580
because we want the target thread to

127
00:08:00,120 --> 00:08:05,400
spend a lot of time in the user mode so

128
00:08:02,580 --> 00:08:07,919
we can intercept it while it's operating

129
00:08:05,400 --> 00:08:10,860
in the user mode the same is also

130
00:08:07,919 --> 00:08:13,639
applied to the second process we use an

131
00:08:10,860 --> 00:08:13,639
infinite no

132
00:08:13,759 --> 00:08:21,660
so I will use it for both of the

133
00:08:17,400 --> 00:08:24,800
processes first let's go to the

134
00:08:21,660 --> 00:08:24,800
process B

135
00:08:27,900 --> 00:08:32,720
and intercept its execution

136
00:08:33,180 --> 00:08:42,060
yeah as you can see as we expect here

137
00:08:36,959 --> 00:08:44,279
versus we get here where the infinite

138
00:08:42,060 --> 00:08:47,640
loop is located we are in the user mode

139
00:08:44,279 --> 00:08:50,220
section of process B and it's just

140
00:08:47,640 --> 00:08:52,920
tries to run some loops and some

141
00:08:50,220 --> 00:08:55,800
infinite notes and we are in the middle

142
00:08:52,920 --> 00:08:59,100
of this process so if I want to use the

143
00:08:55,800 --> 00:09:03,260
dot process command here you can see

144
00:08:59,100 --> 00:09:08,339
that we are currently in process B

145
00:09:03,260 --> 00:09:12,360
 let's try to see the rip which is

146
00:09:08,339 --> 00:09:17,300
there the address of the program counter

147
00:09:12,360 --> 00:09:17,300
and the program counter is located here

148
00:09:17,899 --> 00:09:27,000
and let's see the actual pte for this

149
00:09:23,820 --> 00:09:30,899
program counter to see where is the

150
00:09:27,000 --> 00:09:34,160
actual page table and what it contains I

151
00:09:30,899 --> 00:09:38,480
try to make copy out of it

152
00:09:34,160 --> 00:09:41,820
paste it on process B no we know that

153
00:09:38,480 --> 00:09:47,279
these target our IP address contains

154
00:09:41,820 --> 00:09:49,980
this value and its PT is located here

155
00:09:47,279 --> 00:09:51,140
that that's it that's it for now I I try

156
00:09:49,980 --> 00:09:55,560
to

157
00:09:51,140 --> 00:09:58,640
 clear all of the events and continue

158
00:09:55,560 --> 00:10:02,220
the debugging normally

159
00:09:58,640 --> 00:10:06,080
 I try to copy this shell code address

160
00:10:02,220 --> 00:10:06,080
this is the shell code address

161
00:10:07,820 --> 00:10:16,399
 this is where the shell code address

162
00:10:11,220 --> 00:10:20,220
is located at this process as process B

163
00:10:16,399 --> 00:10:22,140
so I try to write it here shell code is

164
00:10:20,220 --> 00:10:24,860
looking good

165
00:10:22,140 --> 00:10:30,300
here okay

166
00:10:24,860 --> 00:10:34,620
 no let's try to go to the process a

167
00:10:30,300 --> 00:10:36,480
to see what this process tries to do and

168
00:10:34,620 --> 00:10:38,700
you reach somewhere here

169
00:10:36,480 --> 00:10:41,640
it tries to basically make some

170
00:10:38,700 --> 00:10:47,240
computation the user mode again as we

171
00:10:41,640 --> 00:10:50,220
used this script we only receive 

172
00:10:47,240 --> 00:10:52,620
interrupts we only pass the debugger

173
00:10:50,220 --> 00:10:54,779
when we are when it 

174
00:10:52,620 --> 00:10:57,480
clock interrupt received in the user

175
00:10:54,779 --> 00:11:00,720
mode so if I just want to see its

176
00:10:57,480 --> 00:11:04,079
registers we have these registers and if

177
00:11:00,720 --> 00:11:08,399
I just try to 

178
00:11:04,079 --> 00:11:11,459
read read the PT or the page table for

179
00:11:08,399 --> 00:11:14,279
this art for the rip address of the of

180
00:11:11,459 --> 00:11:19,800
this process I can use the PT command

181
00:11:14,279 --> 00:11:23,660
again I try to make copy out of it

182
00:11:19,800 --> 00:11:23,660
and paste it here

183
00:11:25,700 --> 00:11:31,560
now let's try to manipulate the this

184
00:11:29,579 --> 00:11:33,959
process you know for example sometimes

185
00:11:31,560 --> 00:11:36,720
we need to inject some codes in the

186
00:11:33,959 --> 00:11:39,720
target process or for example use a

187
00:11:36,720 --> 00:11:42,720
create remote thread some some

188
00:11:39,720 --> 00:11:45,120
sometimes it's black but for example by

189
00:11:42,720 --> 00:11:47,420
using some anti debugging methods it's

190
00:11:45,120 --> 00:11:51,000
black in the target process

191
00:11:47,420 --> 00:11:53,160
 so there's no we have to find some

192
00:11:51,000 --> 00:11:55,500
other ways of injecting codes in the

193
00:11:53,160 --> 00:11:59,040
target process but here is an easy way

194
00:11:55,500 --> 00:12:00,200
of how we can inject or shell code in

195
00:11:59,040 --> 00:12:05,660
the target

196
00:12:00,200 --> 00:12:10,279
process so basically we are currently 

197
00:12:05,660 --> 00:12:13,920
executing this IP address we are here

198
00:12:10,279 --> 00:12:16,440
know what happens if we completely

199
00:12:13,920 --> 00:12:18,920
manipulate the page table at this

200
00:12:16,440 --> 00:12:22,920
address and change it

201
00:12:18,920 --> 00:12:26,579
to the page the actual physical

202
00:12:22,920 --> 00:12:30,720
address or actual page table of this

203
00:12:26,579 --> 00:12:32,760
address from process b as you can see as

204
00:12:30,720 --> 00:12:34,920
you can imagine the page the physical

205
00:12:32,760 --> 00:12:38,399
addresses change so if the physical

206
00:12:34,920 --> 00:12:42,720
address is changed then the content of

207
00:12:38,399 --> 00:12:46,200
the memory is changed and we can somehow

208
00:12:42,720 --> 00:12:48,839
remap the shell code from process B to

209
00:12:46,200 --> 00:12:52,820
the addresses space of process a so

210
00:12:48,839 --> 00:12:57,360
let's try to do that I try to put

211
00:12:52,820 --> 00:12:59,120
some EQ or edit 

212
00:12:57,360 --> 00:13:03,959
keyword

213
00:12:59,120 --> 00:13:08,760
we want to modify the process a which

214
00:13:03,959 --> 00:13:12,120
it's PT is located at this address

215
00:13:08,760 --> 00:13:15,720
and we try to change its content to the

216
00:13:12,120 --> 00:13:18,240
PT of the process B

217
00:13:15,720 --> 00:13:20,880
so basically this should change the

218
00:13:18,240 --> 00:13:22,800
memory layout of this process so I try

219
00:13:20,880 --> 00:13:24,600
to

220
00:13:22,800 --> 00:13:27,800
run

221
00:13:24,600 --> 00:13:27,800
this command

222
00:13:28,459 --> 00:13:35,579
and another thing another important

223
00:13:31,500 --> 00:13:38,220
thing to mention is that we are we

224
00:13:35,579 --> 00:13:40,200
want to execute the the shell code the

225
00:13:38,220 --> 00:13:42,839
start byte of the share code is

226
00:13:40,200 --> 00:13:45,839
located at this address but

227
00:13:42,839 --> 00:13:45,839


228
00:13:46,160 --> 00:13:52,320
we change this address so we are we this

229
00:13:50,160 --> 00:13:54,260
address might not be valid in this

230
00:13:52,320 --> 00:13:58,380
target process

231
00:13:54,260 --> 00:14:01,440
we only have to change the rip because

232
00:13:58,380 --> 00:14:04,260
we changed one page or four kilobyte of

233
00:14:01,440 --> 00:14:07,260
the data we have to just set these three

234
00:14:04,260 --> 00:14:12,980
bits to the same three bits of the here

235
00:14:07,260 --> 00:14:12,980
so we basically have to change the rip

236
00:14:13,560 --> 00:14:15,980
to

237
00:14:16,079 --> 00:14:23,700
this address because in this case

238
00:14:20,339 --> 00:14:27,180
we are sure that we want to start from

239
00:14:23,700 --> 00:14:29,540
the start address or start byte of the

240
00:14:27,180 --> 00:14:35,000
target page that is changed

241
00:14:29,540 --> 00:14:35,000
so that's it for now let's try to

242
00:14:35,899 --> 00:14:42,480
 continue the debuggee

243
00:14:39,959 --> 00:14:46,079
I try to disable events and continue the

244
00:14:42,480 --> 00:14:49,100
debuggee yeah there is a there is also

245
00:14:46,079 --> 00:14:50,820
another 

246
00:14:49,100 --> 00:14:54,540


247
00:14:50,820 --> 00:14:58,500
panel there's another message box here

248
00:14:54,540 --> 00:15:01,800
that says yes but this is this this

249
00:14:58,500 --> 00:15:04,260
message box comes from process a not

250
00:15:01,800 --> 00:15:06,060
process B because we change it let's

251
00:15:04,260 --> 00:15:09,540
let's see to make sure whether it's

252
00:15:06,060 --> 00:15:11,880
coming from the process A or B I use a

253
00:15:09,540 --> 00:15:13,980
program called inspect from the Windows

254
00:15:11,880 --> 00:15:18,180
SDK you can find it in your target

255
00:15:13,980 --> 00:15:21,860
Windows SDK I try to run it and it shows

256
00:15:18,180 --> 00:15:26,519
me the different panels or different

257
00:15:21,860 --> 00:15:29,459
dialogues we can see that pond

258
00:15:26,519 --> 00:15:34,500
panel is here

259
00:15:29,459 --> 00:15:34,500
I can even highlight it yeah

260
00:15:38,760 --> 00:15:46,699
and the process ID for this panel

261
00:15:42,959 --> 00:15:51,360
 December here it's two

262
00:15:46,699 --> 00:15:54,720
 thousand five hundred let's try to

263
00:15:51,360 --> 00:15:57,360
convert it to the heximal format

264
00:15:54,720 --> 00:16:00,320
all right

265
00:15:57,360 --> 00:16:05,519
9c4

266
00:16:00,320 --> 00:16:06,420
and this 9 C4 as you can see is the same

267
00:16:05,519 --> 00:16:08,940
as

268
00:16:06,420 --> 00:16:10,220
the process a

269
00:16:08,940 --> 00:16:14,399
so

270
00:16:10,220 --> 00:16:18,060
9c4 is executed at process a

271
00:16:14,399 --> 00:16:20,820
and in this target process we were able

272
00:16:18,060 --> 00:16:22,579
to run some codes we inject our code we

273
00:16:20,820 --> 00:16:27,899
inject our shell code

274
00:16:22,579 --> 00:16:31,079
 the reason why I just don't match

275
00:16:27,899 --> 00:16:33,260
the process entirely is because there

276
00:16:31,079 --> 00:16:35,820
might be some changes in the

277
00:16:33,260 --> 00:16:38,759
functions for example the kernels

278
00:16:35,820 --> 00:16:41,279
Theodore or other other deals that you

279
00:16:38,759 --> 00:16:43,860
use by process B might not be also

280
00:16:41,279 --> 00:16:46,880
mapped to the process a so I just try to

281
00:16:43,860 --> 00:16:51,320
 run some shell codes that try to find

282
00:16:46,880 --> 00:16:55,259
the API calls by themselves

283
00:16:51,320 --> 00:16:58,740
 this may be inject some codes on this

284
00:16:55,259 --> 00:17:02,940
target process now let's try to return

285
00:16:58,740 --> 00:17:09,439
the process to its normal execution flow

286
00:17:02,940 --> 00:17:09,439
again I try to go to process a

287
00:17:12,480 --> 00:17:22,520
run it and we want to intercept its

288
00:17:17,459 --> 00:17:25,319
execution in the user mode I I try to

289
00:17:22,520 --> 00:17:26,640
the the thing is that the thread is

290
00:17:25,319 --> 00:17:30,480
currently

291
00:17:26,640 --> 00:17:35,419
 in the kernel waiting for the

292
00:17:30,480 --> 00:17:35,419
message box to be clicked so I tried I

293
00:17:36,419 --> 00:17:44,760
click OK and again

294
00:17:40,380 --> 00:17:48,360
and go to this

295
00:17:44,760 --> 00:17:52,860
process around the same command

296
00:17:48,360 --> 00:17:56,039
yeah we reach again to this address

297
00:17:52,860 --> 00:17:58,200
which is right we are in the process

298
00:17:56,039 --> 00:18:03,240
a

299
00:17:58,200 --> 00:18:06,419
and running some infinite notes that we

300
00:18:03,240 --> 00:18:11,000
added to the process B shell code

301
00:18:06,419 --> 00:18:11,000
so let's just try to 

302
00:18:11,360 --> 00:18:16,820
 make the program to return to its

303
00:18:14,340 --> 00:18:20,520
normal execution flow

304
00:18:16,820 --> 00:18:23,419
I try to 

305
00:18:20,520 --> 00:18:23,419
clear

306
00:18:25,400 --> 00:18:32,960
interrupts return and see as you can see

307
00:18:28,320 --> 00:18:37,679
here it's paused in 20 index

308
00:18:32,960 --> 00:18:41,460
no longer we see new indexes here so

309
00:18:37,679 --> 00:18:43,860
I try to run the command

310
00:18:41,460 --> 00:18:48,260
script command again to intercept the

311
00:18:43,860 --> 00:18:52,620
execution in the user mode we are here

312
00:18:48,260 --> 00:18:55,980
 basically you can add some of the

313
00:18:52,620 --> 00:19:00,410
some of the

314
00:18:55,980 --> 00:19:01,700
shell codes for saving and returning 

315
00:19:00,410 --> 00:19:04,799
[pause]

316
00:19:01,700 --> 00:19:08,400
registers so we are not forced to change

317
00:19:04,799 --> 00:19:09,419
the registers here but but I prefer to

318
00:19:08,400 --> 00:19:14,059


319
00:19:09,419 --> 00:19:14,059
random or restore them manually

320
00:19:15,000 --> 00:19:21,320
the first thing that we should solve is

321
00:19:18,900 --> 00:19:25,679
this address we have to

322
00:19:21,320 --> 00:19:28,919
change it to to the previous 

323
00:19:25,679 --> 00:19:32,480
PT address and the previous video or the

324
00:19:28,919 --> 00:19:37,220
original PD address is here so I try to

325
00:19:32,480 --> 00:19:42,179
restore it the original PT address

326
00:19:37,220 --> 00:19:45,120
 and after that I try to restore

327
00:19:42,179 --> 00:19:47,460
the registers these registers are

328
00:19:45,120 --> 00:19:50,580
useless because this is a 32-bit

329
00:19:47,460 --> 00:19:54,720
application but I have to restore these

330
00:19:50,580 --> 00:19:57,980
registers so let's try to make some a

331
00:19:54,720 --> 00:19:57,980
register commands

332
00:20:06,360 --> 00:20:13,320
I want to return to the previous estator

333
00:20:09,840 --> 00:20:15,919
of the processor the previous state of

334
00:20:13,320 --> 00:20:15,919
the process

335
00:20:22,320 --> 00:20:24,980
okay

336
00:20:50,400 --> 00:20:55,559
so basically running these command

337
00:20:53,280 --> 00:21:00,440
should change the state to the previous

338
00:20:55,559 --> 00:21:00,440
state and also we change the

339
00:21:01,340 --> 00:21:09,299
actual PT enters with original

340
00:21:05,340 --> 00:21:13,020
address so it should be fine let me

341
00:21:09,299 --> 00:21:15,900
disable all events and try to run the

342
00:21:13,020 --> 00:21:19,740
program and as you can see it starts to

343
00:21:15,900 --> 00:21:22,980
Counting again because we return to the

344
00:21:19,740 --> 00:21:25,080
program's normal flow and the program is

345
00:21:22,980 --> 00:21:27,539
just nothing happens it tries to

346
00:21:25,080 --> 00:21:31,320
continue its normal execution

347
00:21:27,539 --> 00:21:33,360
and in the middle of this execution we

348
00:21:31,320 --> 00:21:36,740
just inject some codes yeah

349
00:21:33,360 --> 00:21:36,740
that's how it works

