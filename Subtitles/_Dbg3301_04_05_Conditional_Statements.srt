1
00:00:00,000 --> 00:00:06,960
I know let's see some of the conditional

2
00:00:02,899 --> 00:00:09,660
statements which is used in the script

3
00:00:06,960 --> 00:00:13,320
engine it's pretty like the C

4
00:00:09,660 --> 00:00:15,120
language everything hybrid which is

5
00:00:13,320 --> 00:00:18,900
just supports basic conditional

6
00:00:15,120 --> 00:00:21,420
statements like if else if or else and

7
00:00:18,900 --> 00:00:23,699
this is some examples for example if we

8
00:00:21,420 --> 00:00:27,240
want to check that if there are is

9
00:00:23,699 --> 00:00:30,779
register is equal to 55 or equal to 0 x

10
00:00:27,240 --> 00:00:34,739
55 in hexadecimal format then we print

11
00:00:30,779 --> 00:00:38,520
that the RX is equal to 0 is 55. this is

12
00:00:34,739 --> 00:00:42,960
basically used for checking for some

13
00:00:38,520 --> 00:00:45,180
conditions in whenever an SQL wherever

14
00:00:42,960 --> 00:00:48,300
an event is triggered then we can simply

15
00:00:45,180 --> 00:00:52,500
check for the conditions to see that if

16
00:00:48,300 --> 00:00:54,960
the trigger element is from is

17
00:00:52,500 --> 00:00:57,420
basically interesting for us or not if

18
00:00:54,960 --> 00:00:59,340
it's not interesting then we can simply

19
00:00:57,420 --> 00:01:02,340
continue the debugging but if it's an

20
00:00:59,340 --> 00:01:04,739
interesting and needs and needs an

21
00:01:02,340 --> 00:01:07,320
investigation then you can use this

22
00:01:04,739 --> 00:01:12,119
conditional statements another thing is

23
00:01:07,320 --> 00:01:15,180
that you can also use the expressions in

24
00:01:12,119 --> 00:01:17,100
the statements or the in the conditional

25
00:01:15,180 --> 00:01:21,000
statements like for example in the

26
00:01:17,100 --> 00:01:24,659
second example I made a very simple 

27
00:01:21,000 --> 00:01:29,040
script that tries to different the value

28
00:01:24,659 --> 00:01:30,960
or just the add 0x10 to the RS and then

29
00:01:29,040 --> 00:01:33,119
during friends it and we will check it

30
00:01:30,960 --> 00:01:36,659
whether it's equal to this value or not

31
00:01:33,119 --> 00:01:40,140
and if it was equal to this value then

32
00:01:36,659 --> 00:01:44,340
we have a second check here and we can

33
00:01:40,140 --> 00:01:46,740
use these operators here to check with

34
00:01:44,340 --> 00:01:50,159
different conditions another thing is

35
00:01:46,740 --> 00:01:52,439
that we can use the functions just like

36
00:01:50,159 --> 00:01:54,479
C programming languages I will explain

37
00:01:52,439 --> 00:01:58,200
about the check address function later

38
00:01:54,479 --> 00:02:01,259
but you can also use the church use

39
00:01:58,200 --> 00:02:03,479
the functions and the result of the

40
00:02:01,259 --> 00:02:06,060
functions in the conditional instead

41
00:02:03,479 --> 00:02:10,800
here's another condition if we want to

42
00:02:06,060 --> 00:02:14,340
use if an else if an else command a

43
00:02:10,800 --> 00:02:18,780
statements just keep in mind that there

44
00:02:14,340 --> 00:02:21,660
is no else if a space if in the escape

45
00:02:18,780 --> 00:02:24,660
engine of HyperDbg but it's just like

46
00:02:21,660 --> 00:02:24,660
e-l-s-i-f

