1
00:00:00,359 --> 00:00:07,859
 now it's time to read and modify the

2
00:00:04,500 --> 00:00:10,320
virtual memory by using HyperDbg the

3
00:00:07,859 --> 00:00:14,340
syntax is also the same for both main

4
00:00:10,320 --> 00:00:16,440
dvg and HyperDbg if you previously have

5
00:00:14,340 --> 00:00:19,320
any experience when with individually

6
00:00:16,440 --> 00:00:21,359
you can probably you you definitely know

7
00:00:19,320 --> 00:00:25,439
that the

8
00:00:21,359 --> 00:00:27,439
DB commands is used for showing an

9
00:00:25,439 --> 00:00:32,360
address in the

10
00:00:27,439 --> 00:00:35,360
format of the by a single byte

11
00:00:32,360 --> 00:00:39,059
so you can use the

12
00:00:35,360 --> 00:00:41,480
DB and see something like this you can

13
00:00:39,059 --> 00:00:44,879
specify the

14
00:00:41,480 --> 00:00:50,100
address after this command and also we

15
00:00:44,879 --> 00:00:52,379
have a DC command or it shows in third

16
00:00:50,100 --> 00:00:58,039
two week formats

17
00:00:52,379 --> 00:01:01,739
or in DB in default format

18
00:00:58,039 --> 00:01:04,739
yeah this is pretty common in using a

19
00:01:01,739 --> 00:01:07,640
debugger there's also another command

20
00:01:04,739 --> 00:01:10,200
for sure to beat which is called DWORD

21
00:01:07,640 --> 00:01:12,780
it's exactly like the previous command

22
00:01:10,200 --> 00:01:16,500
but the difference is that it doesn't

23
00:01:12,780 --> 00:01:20,159
shows the text also you can view the

24
00:01:16,500 --> 00:01:24,060
memory in queue word format 2 or in 64

25
00:01:20,159 --> 00:01:27,560
bits by using DQ command and the thing

26
00:01:24,060 --> 00:01:32,939
is that if you you can specify both

27
00:01:27,560 --> 00:01:38,000
symbols and to both as a function

28
00:01:32,939 --> 00:01:40,740
name and both a symbol name from

29
00:01:38,000 --> 00:01:44,100
the target debug for example I can

30
00:01:40,740 --> 00:01:46,280
specify that I want to see a function

31
00:01:44,100 --> 00:01:50,040
the memory of the function

32
00:01:46,280 --> 00:01:52,740
let's see I want to see the X allocate

33
00:01:50,040 --> 00:01:56,700
all with tag

34
00:01:52,740 --> 00:01:59,960
function in DB format so it says that it

35
00:01:56,700 --> 00:02:04,340
cannot resolve the symbol and it's

36
00:01:59,960 --> 00:02:04,340
allocate pull tag

37
00:02:06,320 --> 00:02:12,180
oh here it is

38
00:02:09,000 --> 00:02:13,940
and this is this is the memory located

39
00:02:12,180 --> 00:02:17,220
at this address

40
00:02:13,940 --> 00:02:21,540
we can also change it by using EB

41
00:02:17,220 --> 00:02:23,520
command or E the Ed command for EQ

42
00:02:21,540 --> 00:02:27,540
command each of them have different

43
00:02:23,520 --> 00:02:31,800
formats for example if I want to change

44
00:02:27,540 --> 00:02:35,099
the the function if we want patch this

45
00:02:31,800 --> 00:02:38,540
function which is definitely not it a

46
00:02:35,099 --> 00:02:40,819
good thing but for no I can just

47
00:02:38,540 --> 00:02:45,360
modify the

48
00:02:40,819 --> 00:02:49,260
ntx allocate pull with tag with nope

49
00:02:45,360 --> 00:02:55,080
filled with nope doesn't make sense but

50
00:02:49,260 --> 00:02:57,660
in order to see how it works

51
00:02:55,080 --> 00:03:00,180
you can see that it's now changed now

52
00:02:57,660 --> 00:03:02,519
definitely if I continue this debuggee we

53
00:03:00,180 --> 00:03:06,540
will see a blue screen of that

54
00:03:02,519 --> 00:03:11,599
so I just try to return it to the to the

55
00:03:06,540 --> 00:03:14,959
previous day just to avoid a blue screen

56
00:03:11,599 --> 00:03:14,959
let's see

57
00:03:16,920 --> 00:03:22,739
yeah that's

58
00:03:19,260 --> 00:03:25,700
no I press G and continue the debugging

59
00:03:22,739 --> 00:03:25,700
normally

