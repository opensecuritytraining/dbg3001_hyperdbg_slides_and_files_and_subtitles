1
00:00:00,000 --> 00:00:07,319
the same thing is also true about

2
00:00:03,140 --> 00:00:10,440
resistors we can see different general

3
00:00:07,319 --> 00:00:12,840
purpose registered thereby using a r

4
00:00:10,440 --> 00:00:16,080
command exactly like green Division and

5
00:00:12,840 --> 00:00:19,800
the format is also same as WinDbg if

6
00:00:16,080 --> 00:00:21,779
you want to just see any specific if

7
00:00:19,800 --> 00:00:24,240
you want to see all of the registers

8
00:00:21,779 --> 00:00:28,080
then you can use or without any

9
00:00:24,240 --> 00:00:31,260
parameters but if you want to just see a

10
00:00:28,080 --> 00:00:35,000
special registers then you should you

11
00:00:31,260 --> 00:00:38,760
can use its name or

12
00:00:35,000 --> 00:00:43,559
 you can also modify the register by

13
00:00:38,760 --> 00:00:45,960
using r r a x which is equal to 0 is

14
00:00:43,559 --> 00:00:48,180
five five or fifty five this is the

15
00:00:45,960 --> 00:00:51,420
exactly same as WinDbg but in hybrid

16
00:00:48,180 --> 00:00:54,480
VG it's better to use an at sign mark

17
00:00:51,420 --> 00:00:57,000
before the registers even though it even

18
00:00:54,480 --> 00:01:01,379
though it works without at sign and no

19
00:00:57,000 --> 00:01:03,780
notices that you are not using a that

20
00:01:01,379 --> 00:01:06,360
you are referring to register but a

21
00:01:03,780 --> 00:01:09,060
student speaker to use an ad sign I

22
00:01:06,360 --> 00:01:13,380
didn't use it here but if I want to use

23
00:01:09,060 --> 00:01:18,540
like R that's an i instant you can see

24
00:01:13,380 --> 00:01:22,619
that there are X register is like this

25
00:01:18,540 --> 00:01:24,600
also the registers are available in that

26
00:01:22,619 --> 00:01:26,280
format so if we want to see the

27
00:01:24,600 --> 00:01:29,220
registers in different formats like

28
00:01:26,280 --> 00:01:31,680
hexadecimal decimal octal binary or

29
00:01:29,220 --> 00:01:33,780
other formats you can use the dot

30
00:01:31,680 --> 00:01:36,659
formats and in that format you can also

31
00:01:33,780 --> 00:01:38,659
specify expressions like adding two

32
00:01:36,659 --> 00:01:41,579
registers or

33
00:01:38,659 --> 00:01:44,820
computing anything by using the script

34
00:01:41,579 --> 00:01:47,220
engine or the escape the or really

35
00:01:44,820 --> 00:01:49,820
simple expression simple mathematical

36
00:01:47,220 --> 00:01:49,820
expression

