1
00:00:00,000 --> 00:00:05,279
now let's see another important feature

2
00:00:03,000 --> 00:00:07,319
of the HyperDbg which is the emulating

3
00:00:05,279 --> 00:00:10,440
debug register this is this one is

4
00:00:07,319 --> 00:00:12,660
really interesting I really like this

5
00:00:10,440 --> 00:00:15,960
feature of the hybrid which you probably

6
00:00:12,660 --> 00:00:19,320
used hyper hardware debug which is

7
00:00:15,960 --> 00:00:21,240
there's this is exact including

8
00:00:19,320 --> 00:00:24,539
the same functionality for the memory

9
00:00:21,240 --> 00:00:27,240
monitor or monitor command of the hyper

10
00:00:24,539 --> 00:00:30,359
dvg in a hardware debug registers you

11
00:00:27,240 --> 00:00:34,320
put monitor on a special boundary of the

12
00:00:30,359 --> 00:00:36,540
memory like two one two four or eight

13
00:00:34,320 --> 00:00:39,420
bikes that's the memory by using

14
00:00:36,540 --> 00:00:42,899
hardware debug resistors and whenever

15
00:00:39,420 --> 00:00:46,920
any access to those memory like

16
00:00:42,899 --> 00:00:48,899
memory access to that address a read or

17
00:00:46,920 --> 00:00:53,340
write happens to the that special

18
00:00:48,899 --> 00:00:56,940
address then the DB or debug interrupt

19
00:00:53,340 --> 00:00:58,920
is invoked and you will be notified that

20
00:00:56,940 --> 00:01:02,699
something is changing in that address

21
00:00:58,920 --> 00:01:04,440
it's exactly the same for HyperDbg

22
00:01:02,699 --> 00:01:07,560
HyperDbg mining towards the range

23
00:01:04,440 --> 00:01:09,380
of address for any reads rights or a

24
00:01:07,560 --> 00:01:13,080
combination of read and write

25
00:01:09,380 --> 00:01:15,119
HyperDbg brings it as events as

26
00:01:13,080 --> 00:01:17,640
it's an event you can run customers

27
00:01:15,119 --> 00:01:20,460
scripts create logs or change the system

28
00:01:17,640 --> 00:01:23,400
 state or do whatever you want with

29
00:01:20,460 --> 00:01:26,220
the script engine another thing is that

30
00:01:23,400 --> 00:01:29,460
you can change the results for example

31
00:01:26,220 --> 00:01:32,460
 if something happens in the user mode

32
00:01:29,460 --> 00:01:35,759
or a kernel mode a code and for example

33
00:01:32,460 --> 00:01:38,520
a function tries to read a special value

34
00:01:35,759 --> 00:01:41,640
or change a special value in the memory

35
00:01:38,520 --> 00:01:45,119
then you can just stop changing it like

36
00:01:41,640 --> 00:01:47,939
just ignore the changes that are applied

37
00:01:45,119 --> 00:01:49,920
to that address and the park the program

38
00:01:47,939 --> 00:01:53,280
won't know that it's changed for example

39
00:01:49,920 --> 00:01:55,439
you change an the program tries to

40
00:01:53,280 --> 00:01:58,380
change a special address the simple move

41
00:01:55,439 --> 00:02:00,960
instruction and then you will ignore it

42
00:01:58,380 --> 00:02:02,939
and the program won't know that is that

43
00:02:00,960 --> 00:02:06,180
is ignored that his changes won't apply

44
00:02:02,939 --> 00:02:09,840
but the thing that makes high proteology

45
00:02:06,180 --> 00:02:14,040
unique and usable usable in these cases

46
00:02:09,840 --> 00:02:16,200
it is that it doesn't limit the car it

47
00:02:14,040 --> 00:02:19,739
doesn't have any limitation about the

48
00:02:16,200 --> 00:02:22,440
current and the size and as you know 

49
00:02:19,739 --> 00:02:25,980
there are only four tea bag registers

50
00:02:22,440 --> 00:02:29,220
and as I mentioned the size is limited

51
00:02:25,980 --> 00:02:31,020
to one two four or eight bytes this is

52
00:02:29,220 --> 00:02:33,239
naturally usable in most of the

53
00:02:31,020 --> 00:02:36,480
scenarios because most of the time if

54
00:02:33,239 --> 00:02:40,260
you want to debug a structure or if you

55
00:02:36,480 --> 00:02:43,860
want to find the functions that try to

56
00:02:40,260 --> 00:02:46,920
modify or read from a structure then the

57
00:02:43,860 --> 00:02:51,000
structure is most of the time more than

58
00:02:46,920 --> 00:02:53,879
eight bytes so it's it's not really

59
00:02:51,000 --> 00:02:56,099
usable to use this device registers for

60
00:02:53,879 --> 00:02:58,620
those functions but in high producing

61
00:02:56,099 --> 00:03:02,340
you don't have any limitation in account

62
00:02:58,620 --> 00:03:04,980
and in the size and also only 40 bike

63
00:03:02,340 --> 00:03:07,739
registers are available in in the system

64
00:03:04,980 --> 00:03:10,800
but you don't have any limitation about

65
00:03:07,739 --> 00:03:13,260
the count in hybrid images manager

66
00:03:10,800 --> 00:03:16,920
everything here is implemented in the

67
00:03:13,260 --> 00:03:22,440
EPT level like we modify the kernel page

68
00:03:16,920 --> 00:03:25,500
level routines to export these events

69
00:03:22,440 --> 00:03:28,739
in HyperDbg and some of the use cases

70
00:03:25,500 --> 00:03:31,080
for these monitor commands that you can

71
00:03:28,739 --> 00:03:34,860
as I mentioned you can ignore the memory

72
00:03:31,080 --> 00:03:36,440
rights or ignore or read something else

73
00:03:34,860 --> 00:03:39,300
from the memory

74
00:03:36,440 --> 00:03:41,819
you can also change the memory content

75
00:03:39,300 --> 00:03:44,340
without modifying the memory which means

76
00:03:41,819 --> 00:03:46,799
that you can simply change it and

77
00:03:44,340 --> 00:03:49,260
whenever the program tries to read that

78
00:03:46,799 --> 00:03:54,659
special address then you show something

79
00:03:49,260 --> 00:03:57,659
else and also this is also true about

80
00:03:54,659 --> 00:04:00,299
both user mode and kernel mode so this

81
00:03:57,659 --> 00:04:03,239
is a really important command in terms

82
00:04:00,299 --> 00:04:05,760
of reverse engineering as you can see

83
00:04:03,239 --> 00:04:08,099
monitor or bang monitor extension

84
00:04:05,760 --> 00:04:11,340
command is used for simulating hardware

85
00:04:08,099 --> 00:04:13,980
device registers for example in the

86
00:04:11,340 --> 00:04:18,799
first example it's a monitor command on

87
00:04:13,980 --> 00:04:23,240
which only interests on a read attempts

88
00:04:18,799 --> 00:04:27,780
to this student register or talk

89
00:04:23,240 --> 00:04:31,860
points to current process EPROCESS we

90
00:04:27,780 --> 00:04:36,120
only add plus FF to to the EPROCESS so

91
00:04:31,860 --> 00:04:39,180
we want to intercept any reads starting

92
00:04:36,120 --> 00:04:42,060
from currently process to EPROCESS plus

93
00:04:39,180 --> 00:04:45,180
FF in hexadecimal format the second

94
00:04:42,060 --> 00:04:49,139
command is also a monitor at this time

95
00:04:45,180 --> 00:04:52,500
we are interested on both reads and

96
00:04:49,139 --> 00:04:55,560
rights and it starts from this address

97
00:04:52,500 --> 00:04:58,080
and ends on this address the second

98
00:04:55,560 --> 00:05:01,919
command you use the the script engine

99
00:04:58,080 --> 00:05:05,520
only for this time only for rights and

100
00:05:01,919 --> 00:05:08,040
the range differs from starting from

101
00:05:05,520 --> 00:05:11,460
this address to this address and in the

102
00:05:08,040 --> 00:05:15,300
script it shows the context and the

103
00:05:11,460 --> 00:05:19,020
context for one byte in this range of

104
00:05:15,300 --> 00:05:24,419
memory is modified and context shows the

105
00:05:19,020 --> 00:05:28,320
target byte and last one is the monitor

106
00:05:24,419 --> 00:05:31,560
command interested in rights for this

107
00:05:28,320 --> 00:05:33,900
range of memory and this time we check

108
00:05:31,560 --> 00:05:36,120
whether the context is target memory

109
00:05:33,900 --> 00:05:38,100
address equals to this address or not

110
00:05:36,120 --> 00:05:41,820
and if the address that is being

111
00:05:38,100 --> 00:05:44,460
modified is equals to this address then

112
00:05:41,820 --> 00:05:48,000
we will change it to something else we

113
00:05:44,460 --> 00:05:49,620
will change it to this value this 64 we

114
00:05:48,000 --> 00:05:52,259
takes a small value

115
00:05:49,620 --> 00:05:54,780
and after that we ignore the memory

116
00:05:52,259 --> 00:05:58,680
right don't worry about it I want to see

117
00:05:54,780 --> 00:06:02,880
we will explain this command in the

118
00:05:58,680 --> 00:06:05,160
future part this function actually so

119
00:06:02,880 --> 00:06:08,520
let's see a demo

120
00:06:05,160 --> 00:06:11,940
or let's test one of these commands

121
00:06:08,520 --> 00:06:16,320
for example this one is a good

122
00:06:11,940 --> 00:06:20,960
example and instead of it I try to write

123
00:06:16,320 --> 00:06:24,960
a proc plus if in hexadecimal format so

124
00:06:20,960 --> 00:06:27,900
if anybody in the entire carener or even

125
00:06:24,960 --> 00:06:30,539
in the user mode I mean generally

126
00:06:27,900 --> 00:06:34,500
it's also usable in user mode but if

127
00:06:30,539 --> 00:06:37,199
anybody in the kernel wants to write on

128
00:06:34,500 --> 00:06:41,300
the EPROCESS starting from the starting

129
00:06:37,199 --> 00:06:44,880
point and to this amount of bytes

130
00:06:41,300 --> 00:06:49,580
then we are interested in intercepting

131
00:06:44,880 --> 00:06:49,580
it so let's run it on HyperDbg

132
00:06:50,340 --> 00:06:57,960
I paste it here after continuing the

133
00:06:54,539 --> 00:06:57,960
target debuggee

134
00:06:58,319 --> 00:07:04,740
you can see that there are some

135
00:07:00,539 --> 00:07:08,460
locations that try to write on this 

136
00:07:04,740 --> 00:07:11,780
this address so let me just intercept

137
00:07:08,460 --> 00:07:14,880
some of them for example this address

138
00:07:11,780 --> 00:07:17,039
provides to

139
00:07:14,880 --> 00:07:19,819
the memory address between the range

140
00:07:17,039 --> 00:07:23,520
that we specified so

141
00:07:19,819 --> 00:07:28,039
let's see what are this album is it

142
00:07:23,520 --> 00:07:31,080
located at ntx acquired spin like

143
00:07:28,039 --> 00:07:36,240
exclusive at DPC level

144
00:07:31,080 --> 00:07:36,240
so what's the other address

145
00:07:38,819 --> 00:07:47,039
 it's an anti-ex release exclusive

146
00:07:43,740 --> 00:07:51,360
from this DPC level so now now let's

147
00:07:47,039 --> 00:07:54,479
just clear this event and this

148
00:07:51,360 --> 00:07:57,120
time we are we're gonna intercept only

149
00:07:54,479 --> 00:08:00,360
reads not rights if we want to intercept

150
00:07:57,120 --> 00:08:03,539
both read and write we should specify RW

151
00:08:00,360 --> 00:08:05,400
but this time we are interested only and

152
00:08:03,539 --> 00:08:08,419
reads

153
00:08:05,400 --> 00:08:12,240
let's do it again

154
00:08:08,419 --> 00:08:14,520
these are the memory addresses or these

155
00:08:12,240 --> 00:08:17,400
are the rip addresses that try to

156
00:08:14,520 --> 00:08:20,060
actually read on this

157
00:08:17,400 --> 00:08:20,060
address

158
00:08:28,020 --> 00:08:31,020
foreign

159
00:08:46,100 --> 00:08:54,000
these addresses try to write try to read

160
00:08:51,000 --> 00:08:57,500
from an EPROCESS of the current process

161
00:08:54,000 --> 00:08:57,500
let's see some of them

162
00:09:00,060 --> 00:09:04,160
for example this one

163
00:09:10,040 --> 00:09:19,279
is located on ntps P reference CID table

164
00:09:15,480 --> 00:09:19,279
entry another one

165
00:09:19,940 --> 00:09:27,300
stack attached process another 

166
00:09:23,760 --> 00:09:32,519
feature that recently added to the hyper

167
00:09:27,300 --> 00:09:36,060
dvg is its ability to intercept any

168
00:09:32,519 --> 00:09:38,420
execution in that in different web pages

169
00:09:36,060 --> 00:09:41,540
or in other words

170
00:09:38,420 --> 00:09:44,580
the monitor command for hyperduino

171
00:09:41,540 --> 00:09:47,279
supports Peaks attribute or execution

172
00:09:44,580 --> 00:09:50,399
attribute and right now you can

173
00:09:47,279 --> 00:09:53,240
intercept any instances of reads and

174
00:09:50,399 --> 00:09:57,500
instances of Rights and

175
00:09:53,240 --> 00:10:02,160
executes you can

176
00:09:57,500 --> 00:10:05,459
detect whether something is executed in

177
00:10:02,160 --> 00:10:07,860
your target page or in your target or

178
00:10:05,459 --> 00:10:11,279
range of addresses in both kernel mode

179
00:10:07,860 --> 00:10:14,399
and user mode for example you can detect

180
00:10:11,279 --> 00:10:18,600
whether your module your pre-section got

181
00:10:14,399 --> 00:10:20,839
executed or not or you can intercept

182
00:10:18,600 --> 00:10:25,019
its context

183
00:10:20,839 --> 00:10:28,620
and you can even block the execution

184
00:10:25,019 --> 00:10:31,459
so here is some of the examples for

185
00:10:28,620 --> 00:10:34,580
example in the first example the monitor

186
00:10:31,459 --> 00:10:38,940
rwx is used which

187
00:10:34,580 --> 00:10:41,420
monitors and  and holds the

188
00:10:38,940 --> 00:10:45,600
debugger pause the debugging in case of

189
00:10:41,420 --> 00:10:49,760
 any read write or execution in this

190
00:10:45,600 --> 00:10:51,680
address range in other instances WX

191
00:10:49,760 --> 00:10:55,620
which

192
00:10:51,680 --> 00:10:59,640
stands for write and execute if any

193
00:10:55,620 --> 00:11:02,579
write or execute happens in this

194
00:10:59,640 --> 00:11:06,300
range then this event will be triggered

195
00:11:02,579 --> 00:11:10,640
but it won't get triggered by any

196
00:11:06,300 --> 00:11:14,160
rights or if you just want to intercept

197
00:11:10,640 --> 00:11:17,579
executions you can also use the X

198
00:11:14,160 --> 00:11:24,800
without R and W

199
00:11:17,579 --> 00:11:24,800
 so in this example it says that 

200
00:11:24,899 --> 00:11:32,040
detect the execution from starting from

201
00:11:28,260 --> 00:11:34,800
this address to this address and as a

202
00:11:32,040 --> 00:11:38,279
message just right here that instruction

203
00:11:34,800 --> 00:11:40,040
is executed and this address which is

204
00:11:38,279 --> 00:11:44,100
the context

205
00:11:40,040 --> 00:11:48,120
points the address that got executed

206
00:11:44,100 --> 00:11:51,300
and also another example is the same but

207
00:11:48,120 --> 00:11:54,480
we used event SC or eventual circuiting

208
00:11:51,300 --> 00:11:58,019
 to block the execution don't worry

209
00:11:54,480 --> 00:12:01,560
about this event SC we will talk about

210
00:11:58,019 --> 00:12:04,680
it later but just keep in mind that this

211
00:12:01,560 --> 00:12:08,040
function can block the execution and

212
00:12:04,680 --> 00:12:11,899
won't let the target to be executed so

213
00:12:08,040 --> 00:12:11,899
let's just see an example

214
00:12:13,519 --> 00:12:22,320
 I just prepared a very simple hello

215
00:12:18,540 --> 00:12:27,240
world application that that is in an

216
00:12:22,320 --> 00:12:30,000
in an infinite loop and this application

217
00:12:27,240 --> 00:12:31,980
just shows hello world messages and

218
00:12:30,000 --> 00:12:37,519
slips for two seconds

219
00:12:31,980 --> 00:12:44,100
so let's return to HyperDbg

220
00:12:37,519 --> 00:12:44,100
and connect to the debuggee

221
00:12:55,560 --> 00:12:59,120
I run from here

222
00:13:13,860 --> 00:13:16,399
thank you

223
00:13:18,800 --> 00:13:27,000
this binary file is compiled and it's

224
00:13:23,279 --> 00:13:27,000
now available here

225
00:13:27,800 --> 00:13:34,820
so I just run it we can use the Doug

226
00:13:32,700 --> 00:13:39,920
start command for example to run it

227
00:13:34,820 --> 00:13:39,920
let's try it with the data start command

228
00:13:47,420 --> 00:13:55,760
and now we reach we have to press G to

229
00:13:52,860 --> 00:14:00,560
we are in the context of this process

230
00:13:55,760 --> 00:14:04,380
but we can run G to detect

231
00:14:00,560 --> 00:14:07,700
the entry point of this process so I've

232
00:14:04,380 --> 00:14:12,360
got the entry point address here

233
00:14:07,700 --> 00:14:15,200
 it's all it's also possible to

234
00:14:12,360 --> 00:14:19,980
detect the entry address

235
00:14:15,200 --> 00:14:23,100
by using the LM command for example 

236
00:14:19,980 --> 00:14:27,300
if I don't know about the pro Society I

237
00:14:23,100 --> 00:14:30,360
can use the that process list and we

238
00:14:27,300 --> 00:14:35,220
can find its product side here

239
00:14:30,360 --> 00:14:39,540
so I used LM for user mode modules and I

240
00:14:35,220 --> 00:14:43,199
need it for a process ID 14e4

241
00:14:39,540 --> 00:14:46,320
in hexadecimal format so I I run it and

242
00:14:43,199 --> 00:14:50,040
as you can see this module starts from

243
00:14:46,320 --> 00:14:54,000
here it's entry point is here and this

244
00:14:50,040 --> 00:14:56,880
is the main module we also have ntdl and

245
00:14:54,000 --> 00:14:59,339
kernel 32 here but we are not interested

246
00:14:56,880 --> 00:15:00,620
in in these modules we're only

247
00:14:59,339 --> 00:15:03,600
interested in

248
00:15:00,620 --> 00:15:07,019
 the 

249
00:15:03,600 --> 00:15:13,399
main module hello world modules so

250
00:15:07,019 --> 00:15:13,399
 let's write and monitor

251
00:15:14,899 --> 00:15:22,880
for the execution

252
00:15:17,660 --> 00:15:26,639
that starts starts monitoring memory

253
00:15:22,880 --> 00:15:30,120
 from

254
00:15:26,639 --> 00:15:35,180
this address from the start of the page

255
00:15:30,120 --> 00:15:38,899
and ends on the last byte of the page

256
00:15:35,180 --> 00:15:46,260
for the process ID

257
00:15:38,899 --> 00:15:49,260
 14 E4 and in the S script I write

258
00:15:46,260 --> 00:15:49,260


259
00:15:55,199 --> 00:16:00,480
instruction

260
00:15:57,740 --> 00:16:03,320
executed at

261
00:16:00,480 --> 00:16:03,320


262
00:16:04,620 --> 00:16:09,839
we can use both rip and context no

263
00:16:08,760 --> 00:16:11,480
different

264
00:16:09,839 --> 00:16:16,639
here

265
00:16:11,480 --> 00:16:16,639
 but just to run it 

266
00:16:19,079 --> 00:16:26,699
 now I run it and as you can see we

267
00:16:23,519 --> 00:16:29,040
got the executions here each time any

268
00:16:26,699 --> 00:16:32,779
instructions got executed in this

269
00:16:29,040 --> 00:16:37,620
address range we are notified here

270
00:16:32,779 --> 00:16:40,380
 and the the log the log shows the

271
00:16:37,620 --> 00:16:43,800
actual address that executed these

272
00:16:40,380 --> 00:16:47,880
instructions no I want to intercept

273
00:16:43,800 --> 00:16:51,680
the context of the of this process so I

274
00:16:47,880 --> 00:16:57,240
I clear clear it completely clear it

275
00:16:51,680 --> 00:17:02,160
and this time on my 

276
00:16:57,240 --> 00:17:04,400
a script I write a pause a function to

277
00:17:02,160 --> 00:17:08,760
just pause the

278
00:17:04,400 --> 00:17:11,640
target debuggee so I run it again and as

279
00:17:08,760 --> 00:17:14,760
you can see we arrived in the middle of

280
00:17:11,640 --> 00:17:19,020
the process once this address range got

281
00:17:14,760 --> 00:17:21,780
executed we are in the middle of its

282
00:17:19,020 --> 00:17:25,500
execution and now we can change the

283
00:17:21,780 --> 00:17:28,439
context we have the registers we can 

284
00:17:25,500 --> 00:17:31,919
change the the flow of the program we

285
00:17:28,439 --> 00:17:34,919
can patch something in this process and

286
00:17:31,919 --> 00:17:37,980
we can do anything with this as we are

287
00:17:34,919 --> 00:17:42,020
in the context of the process and

288
00:17:37,980 --> 00:17:42,020
executing in the main module

289
00:17:42,660 --> 00:17:49,200
that's it I think it's enough we will

290
00:17:45,780 --> 00:17:53,280
see more examples about short

291
00:17:49,200 --> 00:17:55,760
circuiting events later in this

292
00:17:53,280 --> 00:17:55,760
series

