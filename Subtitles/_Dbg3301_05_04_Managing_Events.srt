1
00:00:00,060 --> 00:00:07,620
okay let's go to another part which is

2
00:00:03,600 --> 00:00:11,040
the managing events everything as I

3
00:00:07,620 --> 00:00:13,200
mentioned before in hyper DVD is event

4
00:00:11,040 --> 00:00:17,699
for example these Epp Hooks and these

5
00:00:13,200 --> 00:00:21,900
manager hooks are both events and we

6
00:00:17,699 --> 00:00:24,359
have a list of events if you want to see

7
00:00:21,900 --> 00:00:26,460
the list of active events or currently

8
00:00:24,359 --> 00:00:30,180
working events you can use the event

9
00:00:26,460 --> 00:00:32,820
command and for example if you want

10
00:00:30,180 --> 00:00:35,700
to remove an apt hook or manager or

11
00:00:32,820 --> 00:00:39,000
other events how we can remove it we can

12
00:00:35,700 --> 00:00:41,879
use this command event and each of them

13
00:00:39,000 --> 00:00:46,140
is assigned with a with an event ID or

14
00:00:41,879 --> 00:00:49,500
event ID you can see its event ID here

15
00:00:46,140 --> 00:00:53,340
for example these events is assigned

16
00:00:49,500 --> 00:00:56,539
with two four or five event IDs after

17
00:00:53,340 --> 00:01:01,500
that you can disable enable or clear

18
00:00:56,539 --> 00:01:04,860
each event by using Deo or see

19
00:01:01,500 --> 00:01:06,780
parameters for disabling events the

20
00:01:04,860 --> 00:01:09,540
thing is that everything is still

21
00:01:06,780 --> 00:01:13,020
applied and even the still triggering in

22
00:01:09,540 --> 00:01:16,380
the CPU level but the thing is that

23
00:01:13,020 --> 00:01:20,280
HyperDbg won't trigger events for it on

24
00:01:16,380 --> 00:01:22,500
its internal so the CPU is continues to

25
00:01:20,280 --> 00:01:24,840
generate the event but hypers you want

26
00:01:22,500 --> 00:01:27,000
performance actions like pausing the

27
00:01:24,840 --> 00:01:30,780
debugger or running their scripts or

28
00:01:27,000 --> 00:01:33,140
running the custom puts so if you don't

29
00:01:30,780 --> 00:01:38,159
want to use an advantageous recommended

30
00:01:33,140 --> 00:01:41,579
to completely remove it or clear the

31
00:01:38,159 --> 00:01:44,900
event but if you want to pass a d

32
00:01:41,579 --> 00:01:48,960
parameter aligned with the event ID

33
00:01:44,900 --> 00:01:52,020
you can eventually re-enable the event

34
00:01:48,960 --> 00:01:55,200
later by using the E parameters

35
00:01:52,020 --> 00:02:00,479
syntax is also simple you just put the

36
00:01:55,200 --> 00:02:03,119
email ID on the after putting d as the

37
00:02:00,479 --> 00:02:06,299
second command and the event ID and this

38
00:02:03,119 --> 00:02:10,020
way you can use it to perform just

39
00:02:06,299 --> 00:02:13,500
disable the target event another thing

40
00:02:10,020 --> 00:02:17,280
is that you can later re-enable the

41
00:02:13,500 --> 00:02:19,800
event by using e parameter to the event

42
00:02:17,280 --> 00:02:22,800
command and after that the command

43
00:02:19,800 --> 00:02:25,680
itself as you can see in this example

44
00:02:22,800 --> 00:02:28,500
the event is currently the event force

45
00:02:25,680 --> 00:02:32,660
currently in a disabled state and the

46
00:02:28,500 --> 00:02:35,940
event itself is here but after that we

47
00:02:32,660 --> 00:02:38,040
re-enable it and the debugger the hybrid

48
00:02:35,940 --> 00:02:40,440
which debugger tries to trigger event

49
00:02:38,040 --> 00:02:43,560
and perform the actions for this event

50
00:02:40,440 --> 00:02:46,680
again after enabling another thing is

51
00:02:43,560 --> 00:02:50,220
that you can clear an event what will

52
00:02:46,680 --> 00:02:52,379
completely remove the event and the

53
00:02:50,220 --> 00:02:56,280
processor won't generates that even

54
00:02:52,379 --> 00:02:59,160
anymore but the removed event cannot be

55
00:02:56,280 --> 00:03:02,099
restored and if you are the only way to

56
00:02:59,160 --> 00:03:05,700
restore them is that you should run the

57
00:03:02,099 --> 00:03:08,700
command again so the event is generated

58
00:03:05,700 --> 00:03:11,159
again but you cannot re-enable the

59
00:03:08,700 --> 00:03:13,260
remote command or clear command also if

60
00:03:11,159 --> 00:03:16,500
you want to for example manage them

61
00:03:13,260 --> 00:03:19,440
together you can use the all and pass

62
00:03:16,500 --> 00:03:21,360
all as the event ID it's also supported

63
00:03:19,440 --> 00:03:24,659
for example the following commands I

64
00:03:21,360 --> 00:03:28,560
want to disable all the events so I use

65
00:03:24,659 --> 00:03:31,140
event D or if I want to just clear all

66
00:03:28,560 --> 00:03:35,760
the events I will use events see all

67
00:03:31,140 --> 00:03:39,420
this way the events are clear disabled

68
00:03:35,760 --> 00:03:42,620
or even all of them are these actions

69
00:03:39,420 --> 00:03:42,620
apply to all of them

