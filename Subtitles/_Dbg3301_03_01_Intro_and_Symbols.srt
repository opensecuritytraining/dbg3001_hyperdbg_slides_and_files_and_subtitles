1
00:00:00,120 --> 00:00:06,540
hi everyone welcome to the next part of

2
00:00:03,120 --> 00:00:09,240
the tutorial reversing with HyperDbg

3
00:00:06,540 --> 00:00:11,460
in the previous tutorial we see basic

4
00:00:09,240 --> 00:00:15,420
hybrid usage concepts like events

5
00:00:11,460 --> 00:00:18,359
actions at last we connect to a VMI mode

6
00:00:15,420 --> 00:00:21,720
debugging of the hybrid Vision in this

7
00:00:18,359 --> 00:00:24,119
part we're gonna explore more details

8
00:00:21,720 --> 00:00:27,599
about this divider and we see more basic

9
00:00:24,119 --> 00:00:30,000
commands of how we can use this divider

10
00:00:27,599 --> 00:00:33,200
and we get started with using the

11
00:00:30,000 --> 00:00:36,480
debugger by debugging in debugger mode

12
00:00:33,200 --> 00:00:39,480
first we start exploring symbols and

13
00:00:36,480 --> 00:00:41,879
symbol servers we see some of the bay

14
00:00:39,480 --> 00:00:44,820
very very basic Windows internal

15
00:00:41,879 --> 00:00:47,700
concepts like process and third data

16
00:00:44,820 --> 00:00:51,899
structures and how we can find loaded

17
00:00:47,700 --> 00:00:55,800
modules we also try to convert the

18
00:00:51,899 --> 00:00:58,920
symbols the the and Microsoft symbols

19
00:00:55,800 --> 00:01:02,359
and regular program symbols the C data

20
00:00:58,920 --> 00:01:05,220
types black building enumerations making

21
00:01:02,359 --> 00:01:07,580
structures out of the symbols and

22
00:01:05,220 --> 00:01:11,220
dumping the headers

23
00:01:07,580 --> 00:01:14,159
and then we start kernel and user

24
00:01:11,220 --> 00:01:16,020
debugging like setup or user mod

25
00:01:14,159 --> 00:01:19,260
debugger and the kernel mode debugger

26
00:01:16,020 --> 00:01:23,939
then we start reading and modifying the

27
00:01:19,260 --> 00:01:27,360
virtual memory registers and we see

28
00:01:23,939 --> 00:01:30,960
how we can use the HyperDbg disassemble

29
00:01:27,360 --> 00:01:33,540
codes and examining a memory like a

30
00:01:30,960 --> 00:01:36,720
stack heaps and pools like things like

31
00:01:33,540 --> 00:01:38,159
Aztec vectors and mapping memory thread

32
00:01:36,720 --> 00:01:41,460
structures

33
00:01:38,159 --> 00:01:44,880
and then we go through the break points

34
00:01:41,460 --> 00:01:47,700
and we see the different stepping

35
00:01:44,880 --> 00:01:49,079
mechanisms in hypertension this one is

36
00:01:47,700 --> 00:01:52,380
really

37
00:01:49,079 --> 00:01:55,439
an important topic in HyperDbg is

38
00:01:52,380 --> 00:01:58,680
how HyperDbg is different in a stepping

39
00:01:55,439 --> 00:02:02,939
mechanisms I will explain it truly later

40
00:01:58,680 --> 00:02:06,960
and we have a hands-on for starting

41
00:02:02,939 --> 00:02:10,080
with Hyper twitch okay and this part is

42
00:02:06,960 --> 00:02:13,800
about symbols and symbols servers as you

43
00:02:10,080 --> 00:02:17,760
know every binary that are compiled

44
00:02:13,800 --> 00:02:18,680
with a Microsoft compiler or in also in

45
00:02:17,760 --> 00:02:21,300
other

46
00:02:18,680 --> 00:02:24,800
operating systems we have something like

47
00:02:21,300 --> 00:02:28,620
this platform Microsoft and

48
00:02:24,800 --> 00:02:33,680
most of the Windows binaries we have pdb

49
00:02:28,620 --> 00:02:37,080
files these PDF pdb files are

50
00:02:33,680 --> 00:02:40,640
these beautiful files contain their

51
00:02:37,080 --> 00:02:44,040
debugging information like different

52
00:02:40,640 --> 00:02:47,459
function names different parameters and

53
00:02:44,040 --> 00:02:49,340
things that help us in debugging without

54
00:02:47,459 --> 00:02:53,300
having the source code

55
00:02:49,340 --> 00:02:56,300
as you probably know Windows supports

56
00:02:53,300 --> 00:03:01,340
symbols and this is also through about

57
00:02:56,300 --> 00:03:04,739
it's also true about HyperDbg

58
00:03:01,340 --> 00:03:08,040
first of all we have to set a symbol

59
00:03:04,739 --> 00:03:11,879
path a simple past campaigns a local

60
00:03:08,040 --> 00:03:15,060
path which will be say which are all of

61
00:03:11,879 --> 00:03:18,540
our symbols are located there and we

62
00:03:15,060 --> 00:03:22,019
will provide a URL most of the times

63
00:03:18,540 --> 00:03:25,560
the Microsoft symbol server url url

64
00:03:22,019 --> 00:03:28,560
which if a symbol is not available then

65
00:03:25,560 --> 00:03:31,739
the HyperDbg goes and download that

66
00:03:28,560 --> 00:03:35,420
symbol from the macro server

67
00:03:31,739 --> 00:03:38,959
so the command for setting the symbol

68
00:03:35,420 --> 00:03:43,140
server is that same path

69
00:03:38,959 --> 00:03:47,159
srvs star and after that we're gonna

70
00:03:43,140 --> 00:03:51,659
provide the Local Pass and separated by

71
00:03:47,159 --> 00:03:54,599
a star then we set their remote path

72
00:03:51,659 --> 00:03:56,480
let's see it in action actually I bring

73
00:03:54,599 --> 00:03:59,879
a HyperDbg

74
00:03:56,480 --> 00:04:03,599
here as we compile it in the previous

75
00:03:59,879 --> 00:04:06,599
session so let me start running it in

76
00:04:03,599 --> 00:04:13,099
the Haas operating system

77
00:04:06,599 --> 00:04:13,099
 if I use the dot Sim path 

78
00:04:18,739 --> 00:04:24,600
command it said that the symbol server

79
00:04:21,540 --> 00:04:27,240
is not configured please use that help

80
00:04:24,600 --> 00:04:29,360
dot simpass one more information so I

81
00:04:27,240 --> 00:04:32,280
exactly copy the same

82
00:04:29,360 --> 00:04:34,560
command and

83
00:04:32,280 --> 00:04:36,900
and we have a command that's impasse

84
00:04:34,560 --> 00:04:38,840
this is in the help of the hybrid

85
00:04:36,900 --> 00:04:42,600
which you can use it if you have

86
00:04:38,840 --> 00:04:44,520
different symbol paths like I

87
00:04:42,600 --> 00:04:49,259
always 

88
00:04:44,520 --> 00:04:51,780
save my symbols in this path but if you

89
00:04:49,259 --> 00:04:55,440
choose another path you can just change

90
00:04:51,780 --> 00:04:58,320
this path to your specific path pass and

91
00:04:55,440 --> 00:05:00,960
click enter now after that it says that

92
00:04:58,320 --> 00:05:04,440
the single server pass is configured and

93
00:05:00,960 --> 00:05:06,840
now we should use this dot seam load

94
00:05:04,440 --> 00:05:09,300
command or dot Sim reload command or

95
00:05:06,840 --> 00:05:13,460
that Sim download command to load the

96
00:05:09,300 --> 00:05:16,500
PDF right so what are these 

97
00:05:13,460 --> 00:05:20,040
the meaning of these parameters to this

98
00:05:16,500 --> 00:05:23,040
command for this purpose we go to the

99
00:05:20,040 --> 00:05:23,040
documentation

100
00:05:23,180 --> 00:05:26,660
text that

101
00:05:34,139 --> 00:05:38,240
and we will find

102
00:05:41,460 --> 00:05:44,960
dot scene command

103
00:05:45,360 --> 00:05:54,240
 this is completely described here if

104
00:05:48,660 --> 00:05:56,580
you want to see the table of the 

105
00:05:54,240 --> 00:05:58,979
Table after steamboats we use this

106
00:05:56,580 --> 00:06:01,740
command if we want to load the pdb file

107
00:05:58,979 --> 00:06:05,100
or unload the PDF file if you want to

108
00:06:01,740 --> 00:06:06,979
download a PDF file or reload this is

109
00:06:05,100 --> 00:06:11,460
also different for example

110
00:06:06,979 --> 00:06:14,520
 load command tries to load and parse

111
00:06:11,460 --> 00:06:16,919
pdb files based on the previous paid 

112
00:06:14,520 --> 00:06:21,960
builds simple table

113
00:06:16,919 --> 00:06:25,800
I will explain it later but for now 

114
00:06:21,960 --> 00:06:28,380
if we didn't provide a symbol table

115
00:06:25,800 --> 00:06:31,380
 we use the Dutch reload which first

116
00:06:28,380 --> 00:06:33,720
builds the symbol table and then loads

117
00:06:31,380 --> 00:06:36,840
the local symbol pass

118
00:06:33,720 --> 00:06:38,340
that's from load command won't download

119
00:06:36,840 --> 00:06:40,319


120
00:06:38,340 --> 00:06:43,800
the symbols if they are not available

121
00:06:40,319 --> 00:06:47,300
but if you use this the Sim that Sim

122
00:06:43,800 --> 00:06:50,580
download command in it tries to first

123
00:06:47,300 --> 00:06:53,479
search the local password the symbol and

124
00:06:50,580 --> 00:06:53,479
if it's not available

125
00:06:55,800 --> 00:07:00,900
the website that we have other options

126
00:06:58,319 --> 00:07:03,300
which are not really important for us in

127
00:07:00,900 --> 00:07:06,139
this stage but you can see the examples

128
00:07:03,300 --> 00:07:09,979
where we used

129
00:07:06,139 --> 00:07:13,800
this that seem reload command

130
00:07:09,979 --> 00:07:16,979
no I don't have any other debuggies so I

131
00:07:13,800 --> 00:07:20,479
just want to use the symbols without

132
00:07:16,979 --> 00:07:20,479
connecting to the debugging

133
00:07:20,880 --> 00:07:27,860
and also here I describe

134
00:07:24,319 --> 00:07:32,340
 the different 

135
00:07:27,860 --> 00:07:35,759
commands and purpose of each command

136
00:07:32,340 --> 00:07:40,759
for example if you want to just add your

137
00:07:35,759 --> 00:07:45,000
custom symbol you can use that Sim at

138
00:07:40,759 --> 00:07:47,400
base and UK you should provide a base

139
00:07:45,000 --> 00:07:49,800
that starts that the module starts

140
00:07:47,400 --> 00:07:53,940
loading from that address and after that

141
00:07:49,800 --> 00:07:54,720
it passed on a symbol pass to the PDF

142
00:07:53,940 --> 00:07:58,139
file

143
00:07:54,720 --> 00:08:00,360
you know and also all of them are clear

144
00:07:58,139 --> 00:08:02,400
when we use it in a real debugging

145
00:08:00,360 --> 00:08:06,960
scenario

146
00:08:02,400 --> 00:08:10,380
the first thing that we can find 

147
00:08:06,960 --> 00:08:13,680
and the search is actually a wild card

148
00:08:10,380 --> 00:08:17,639
 kind of a string that you can provide

149
00:08:13,680 --> 00:08:21,240
that it will show you every extrins that

150
00:08:17,639 --> 00:08:24,979
start with this pattern like I want to

151
00:08:21,240 --> 00:08:30,300
see or the search for all the empty

152
00:08:24,979 --> 00:08:34,080
modules functions or functions that

153
00:08:30,300 --> 00:08:35,539
are starting with x allocate as you can

154
00:08:34,080 --> 00:08:38,820
see

155
00:08:35,539 --> 00:08:43,200
ntx allocate will be tag and antioxide

156
00:08:38,820 --> 00:08:46,500
allocate cash a cash aware push lock or

157
00:08:43,200 --> 00:08:48,720
other functions are starting with this

158
00:08:46,500 --> 00:08:51,060
pattern so we can have it and search

159
00:08:48,720 --> 00:08:54,560
through the symbols and find their

160
00:08:51,060 --> 00:08:54,560
corresponding addresses

