1
00:00:00,060 --> 00:00:06,740
there are also other features in hyper

2
00:00:03,419 --> 00:00:09,320
dvg which is used for defeating

3
00:00:06,740 --> 00:00:13,160
anti-debagging methods

4
00:00:09,320 --> 00:00:15,660
transparent mode of the HyperDbg is an

5
00:00:13,160 --> 00:00:19,199
anti-debagging or an anti-anthony

6
00:00:15,660 --> 00:00:22,500
hypervisor solution for HyperDbg even

7
00:00:19,199 --> 00:00:24,119
it won't guarantee a 100 percent of

8
00:00:22,500 --> 00:00:25,740
transparency but it makes it

9
00:00:24,119 --> 00:00:27,720
substantially harder for the

10
00:00:25,740 --> 00:00:31,019
anti-debagging methods to detect high

11
00:00:27,720 --> 00:00:33,660
produced debugger this feature is

12
00:00:31,019 --> 00:00:36,300
actively under development some new

13
00:00:33,660 --> 00:00:40,320
feature will be added to it or but this

14
00:00:36,300 --> 00:00:43,980
is like there are still other methods to

15
00:00:40,320 --> 00:00:46,320
take HyperDbg but it will be enhanced

16
00:00:43,980 --> 00:00:49,020
during the time if you want to enable

17
00:00:46,320 --> 00:00:52,079
this mode of execution you should use

18
00:00:49,020 --> 00:00:55,379
measure and height command bang measure

19
00:00:52,079 --> 00:00:58,440
and bang height commands the transparent

20
00:00:55,379 --> 00:01:00,899
mode can be used in both VMI mode and

21
00:00:58,440 --> 00:01:03,000
debugger mode and because if you want to

22
00:01:00,899 --> 00:01:06,900
enable it it's just simply want to

23
00:01:03,000 --> 00:01:09,180
bypass some of the Delta timing attacks

24
00:01:06,900 --> 00:01:13,260
again hypervisor we could use this bang

25
00:01:09,180 --> 00:01:16,740
measure command and it uses statistical

26
00:01:13,260 --> 00:01:18,900
methods to measure and provide details

27
00:01:16,740 --> 00:01:22,259
of transparent and mode for the high

28
00:01:18,900 --> 00:01:26,460
producing to defeat this anti-hypervisor

29
00:01:22,259 --> 00:01:28,979
methods after you should run the measure

30
00:01:26,460 --> 00:01:32,520
command before running load or before

31
00:01:28,979 --> 00:01:34,979
running debug command so hybrid Refugee

32
00:01:32,520 --> 00:01:38,400
is able to measure the behavior of the

33
00:01:34,979 --> 00:01:41,340
processor in case of when the hypervisor

34
00:01:38,400 --> 00:01:44,759
is not loaded and but if you just forget

35
00:01:41,340 --> 00:01:46,979
it hybrid which also has some hard to

36
00:01:44,759 --> 00:01:49,399
the default results which you can which

37
00:01:46,979 --> 00:01:53,040
can be used but this is still

38
00:01:49,399 --> 00:01:56,220
recommended to use to let HyperDbg to

39
00:01:53,040 --> 00:01:57,479
measure the statistics based on your

40
00:01:56,220 --> 00:02:00,240
current system

41
00:01:57,479 --> 00:02:02,640
after that you could use a height

42
00:02:00,240 --> 00:02:06,299
command bang height command and you

43
00:02:02,640 --> 00:02:09,360
could specify process ID by using PID

44
00:02:06,299 --> 00:02:12,840
argument or if you can use name and add

45
00:02:09,360 --> 00:02:16,560
the process name as its argument tool so

46
00:02:12,840 --> 00:02:19,560
each time this special process wants to

47
00:02:16,560 --> 00:02:22,200
do some I3 debugging methods then hyper

48
00:02:19,560 --> 00:02:24,900
individual will detect it and try to

49
00:02:22,200 --> 00:02:27,540
prevent it from performing some of the

50
00:02:24,900 --> 00:02:30,200
anti-dividing methods or prevent it from

51
00:02:27,540 --> 00:02:33,599
understanding that there is a debugger

52
00:02:30,200 --> 00:02:37,819
behind scenes so this is the way that

53
00:02:33,599 --> 00:02:37,819
you can use this measure and height Max

