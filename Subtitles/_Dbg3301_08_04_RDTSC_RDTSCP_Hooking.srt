1
00:00:00,000 --> 00:00:08,179
the same capability also exists for

2
00:00:03,720 --> 00:00:11,940
timing commands now if we want to hook

3
00:00:08,179 --> 00:00:14,820
rdtsc and rdtscp instruction we could we

4
00:00:11,940 --> 00:00:17,760
could use the exact same command and TSC

5
00:00:14,820 --> 00:00:20,580
batch hooking these instructions make

6
00:00:17,760 --> 00:00:24,000
the system a little bit unstable you

7
00:00:20,580 --> 00:00:27,000
should avoid to use them as a just if

8
00:00:24,000 --> 00:00:30,300
you know what you want to do you can use

9
00:00:27,000 --> 00:00:33,779
it but in case this command mostly make

10
00:00:30,300 --> 00:00:33,779
the system unstable

