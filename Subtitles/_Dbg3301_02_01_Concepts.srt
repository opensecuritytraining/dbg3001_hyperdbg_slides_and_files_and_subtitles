1
00:00:00,120 --> 00:00:06,540
hi everyone welcome to the next part of

2
00:00:03,120 --> 00:00:10,380
the tutorial reversing with HyperDbg

3
00:00:06,540 --> 00:00:12,660
in the previous part we see how what

4
00:00:10,380 --> 00:00:14,519
what exactly this debugger is and we

5
00:00:12,660 --> 00:00:17,400
compare it with other debuggers like

6
00:00:14,519 --> 00:00:19,740
mini widget in this part we're gonna see

7
00:00:17,400 --> 00:00:22,260
the basic concepts about this debugger

8
00:00:19,740 --> 00:00:24,840
and the concepts that you need to know

9
00:00:22,260 --> 00:00:28,439
before I start using this debugger after

10
00:00:24,840 --> 00:00:31,399
that we start attaching to the HyperDbg

11
00:00:28,439 --> 00:00:34,820
to debug a system okay

12
00:00:31,399 --> 00:00:38,340
in this session we're gonna learn about

13
00:00:34,820 --> 00:00:41,000
basic concepts that are in the basic

14
00:00:38,340 --> 00:00:44,760
hybrid video concepts like events

15
00:00:41,000 --> 00:00:47,579
actions different actions that are a

16
00:00:44,760 --> 00:00:49,860
script break or custom code we're gonna

17
00:00:47,579 --> 00:00:52,379
talk about conditions the different

18
00:00:49,860 --> 00:00:55,460
command formats in hybridivity and

19
00:00:52,379 --> 00:00:58,980
events format and we also see different

20
00:00:55,460 --> 00:01:01,559
subsystems of this debugger and we're

21
00:00:58,980 --> 00:01:04,880
gonna learn about operation modes in

22
00:01:01,559 --> 00:01:07,740
HyperDbg like VMI mode

23
00:01:04,880 --> 00:01:10,860
mode and transparent mode

24
00:01:07,740 --> 00:01:12,900
at last we start building hyper

25
00:01:10,860 --> 00:01:15,180
Division and we attached to

26
00:01:12,900 --> 00:01:19,619
hyperactivity in a local debugging area

27
00:01:15,180 --> 00:01:23,759
or in TCP or serial debugging

28
00:01:19,619 --> 00:01:27,840
okay let's see these concepts everything

29
00:01:23,759 --> 00:01:30,900
in HyperDbg is an event like every

30
00:01:27,840 --> 00:01:33,240
incident that happens in your system is

31
00:01:30,900 --> 00:01:37,079
AVID for example breakpoint is an event

32
00:01:33,240 --> 00:01:40,939
EPT hooks are events since the syscall

33
00:01:37,079 --> 00:01:42,720
hooks are also events and most of the

34
00:01:40,939 --> 00:01:45,960
commands that just start with

35
00:01:42,720 --> 00:01:48,840
exclamation mark or bang or almost all

36
00:01:45,960 --> 00:01:54,619
of them are events and probably

37
00:01:48,840 --> 00:01:58,680
future features are also based on events

38
00:01:54,619 --> 00:02:02,040
events might consist of zero to

39
00:01:58,680 --> 00:02:04,380
unlimited number of actions and these

40
00:02:02,040 --> 00:02:06,000
events are either conditional or

41
00:02:04,380 --> 00:02:10,380
unconditional

42
00:02:06,000 --> 00:02:13,500
I I will explain each other through

43
00:02:10,380 --> 00:02:17,340
classic debuggers like WinDbg and

44
00:02:13,500 --> 00:02:18,959
other debuggers house everything for

45
00:02:17,340 --> 00:02:22,080
example whenever a breakpoint happens

46
00:02:18,959 --> 00:02:25,080
just the entire system is halted and

47
00:02:22,080 --> 00:02:28,080
waiting for for a command this is

48
00:02:25,080 --> 00:02:31,260
also true about HyperDbg hyper

49
00:02:28,080 --> 00:02:34,680
division has a has an option or has an

50
00:02:31,260 --> 00:02:37,020
 has an action that is called break

51
00:02:34,680 --> 00:02:40,260
and it holds everything

52
00:02:37,020 --> 00:02:42,900
the second option is a s script this is

53
00:02:40,260 --> 00:02:45,959
a powerful option I will explain about

54
00:02:42,900 --> 00:02:48,620
it and the third option is also running

55
00:02:45,959 --> 00:02:53,879
custom assembly codes

56
00:02:48,620 --> 00:02:56,879
so we have three basic actions break a

57
00:02:53,879 --> 00:02:59,160
script and custom groups so let's see

58
00:02:56,879 --> 00:03:02,580
each of them in details

59
00:02:59,160 --> 00:03:04,920
and as I told you before break is 

60
00:03:02,580 --> 00:03:07,200
exactly like classic debuggers and

61
00:03:04,920 --> 00:03:09,599
everything is paused and Hyper directly

62
00:03:07,200 --> 00:03:12,480
guarantees that nothing no no

63
00:03:09,599 --> 00:03:15,120
instruction will get executed into the

64
00:03:12,480 --> 00:03:17,519
guest or into the debuggee without the

65
00:03:15,120 --> 00:03:19,860
debugger's permission

66
00:03:17,519 --> 00:03:23,159
we have the same thing in IoT by girls

67
00:03:19,860 --> 00:03:26,819
but another powerful feature or another

68
00:03:23,159 --> 00:03:30,120
powerful action of this debugger is a

69
00:03:26,819 --> 00:03:33,480
script s group helps you to view

70
00:03:30,120 --> 00:03:35,940
parameters create logs from new

71
00:03:33,480 --> 00:03:37,980
parameters and the system estate the

72
00:03:35,940 --> 00:03:41,159
overall System state like different

73
00:03:37,980 --> 00:03:44,700
registers memory and things of other 

74
00:03:41,159 --> 00:03:47,159
features it's custom language that are

75
00:03:44,700 --> 00:03:49,319
designed for this debugger and it's

76
00:03:47,159 --> 00:03:53,640
exactly like other programming languages

77
00:03:49,319 --> 00:03:56,220
like it it has if statements four

78
00:03:53,640 --> 00:03:58,319
statements or loop statements or other

79
00:03:56,220 --> 00:04:01,440
things like this

80
00:03:58,319 --> 00:04:04,260
and the thing about this device this

81
00:04:01,440 --> 00:04:07,500
script is that it comes it's

82
00:04:04,260 --> 00:04:11,040
competitive compatible with a remix root

83
00:04:07,500 --> 00:04:14,280
mode it's like everything is executed

84
00:04:11,040 --> 00:04:16,560
in the VM extrude mode in the debuggle

85
00:04:14,280 --> 00:04:19,019
so you have a higher privilege and

86
00:04:16,560 --> 00:04:21,139
nothing is communicated between the

87
00:04:19,019 --> 00:04:24,139
debugger and the debugger we have a

88
00:04:21,139 --> 00:04:27,780
complete we have a complete session

89
00:04:24,139 --> 00:04:31,139
and we will explain scripts in

90
00:04:27,780 --> 00:04:34,680
details later in this series

91
00:04:31,139 --> 00:04:37,380
another action for HyperDbg is custom

92
00:04:34,680 --> 00:04:39,000
codes custom codes is a also a

93
00:04:37,380 --> 00:04:41,520
powerful feature that gives you the

94
00:04:39,000 --> 00:04:44,940
ability to run your custom assembly

95
00:04:41,520 --> 00:04:47,940
codes whenever a special event is

96
00:04:44,940 --> 00:04:50,160
triggered like you define some assembly

97
00:04:47,940 --> 00:04:51,740
codes and after that whenever the event

98
00:04:50,160 --> 00:04:55,259
is triggered

99
00:04:51,740 --> 00:04:59,280
 your codes your assembly posts

100
00:04:55,259 --> 00:05:01,020
will will execute in for example

101
00:04:59,280 --> 00:05:03,139
VMX root mode

102
00:05:01,020 --> 00:05:08,220
which is the highest available

103
00:05:03,139 --> 00:05:12,060
permission in this debugger we also have

104
00:05:08,220 --> 00:05:14,280
conditions and conditions are exactly

105
00:05:12,060 --> 00:05:20,160
like custom codes they are assemblies

106
00:05:14,280 --> 00:05:24,860
and if at last if you put zero on 

107
00:05:20,160 --> 00:05:28,560
RX Regis there and then the def the

108
00:05:24,860 --> 00:05:32,400
debugger assumed that the event should

109
00:05:28,560 --> 00:05:35,639
be ignored and the event or other

110
00:05:32,400 --> 00:05:40,919
actions could not be executed so it's a

111
00:05:35,639 --> 00:05:43,860
level B before the actions if

112
00:05:40,919 --> 00:05:46,080
the conditions are not met then the

113
00:05:43,860 --> 00:05:48,120
actions will never get a chance to get

114
00:05:46,080 --> 00:05:51,120
executed into the system

115
00:05:48,120 --> 00:05:55,139
so but but if you return anything

116
00:05:51,120 --> 00:05:58,979
other than zero in RX register then

117
00:05:55,139 --> 00:06:03,380
 the actions are actions are performed

118
00:05:58,979 --> 00:06:05,880
as I told you we might have zero or

119
00:06:03,380 --> 00:06:09,120
unlimited number of actions like it's

120
00:06:05,880 --> 00:06:14,039
like an event can have two s grips or

121
00:06:09,120 --> 00:06:16,080
one script one custom code and 

122
00:06:14,039 --> 00:06:19,800
eventually breaks the system I will

123
00:06:16,080 --> 00:06:23,639
explain it in details later also if you

124
00:06:19,800 --> 00:06:28,080
are not really convenient with using a

125
00:06:23,639 --> 00:06:31,199
custom code assemblies you can also use

126
00:06:28,080 --> 00:06:35,039
the high level script engine conditions

127
00:06:31,199 --> 00:06:37,819
like if else and else if to for example

128
00:06:35,039 --> 00:06:41,280
ignore an event or ignore

129
00:06:37,819 --> 00:06:43,800
a special action for an event or

130
00:06:41,280 --> 00:06:47,400
something like this but in terms of

131
00:06:43,800 --> 00:06:51,360
performance definitely the custom codes

132
00:06:47,400 --> 00:06:55,080
is a better than running an escape

133
00:06:51,360 --> 00:06:58,259
in HyperDbg we have three exactly like

134
00:06:55,080 --> 00:07:00,720
individually we borrowed the command

135
00:06:58,259 --> 00:07:02,819
format the commands format from

136
00:07:00,720 --> 00:07:07,440
individually because it makes it easier

137
00:07:02,819 --> 00:07:10,680
for users to adapt with this debugger

138
00:07:07,440 --> 00:07:13,080
 hacker dvg has three types of

139
00:07:10,680 --> 00:07:16,560
commands like windy regime

140
00:07:13,080 --> 00:07:19,919
and these commands are regular commands

141
00:07:16,560 --> 00:07:23,300
meta commands and extension commands a

142
00:07:19,919 --> 00:07:26,099
regular commands

143
00:07:23,300 --> 00:07:27,979
something that they these commands won't

144
00:07:26,099 --> 00:07:32,340
start with anything

145
00:07:27,979 --> 00:07:34,979
and it's like tests and they will apply

146
00:07:32,340 --> 00:07:37,259
in the debugging station and most of

147
00:07:34,979 --> 00:07:39,660
them this is not true about all of the

148
00:07:37,259 --> 00:07:42,720
commands bingividly also it's not also

149
00:07:39,660 --> 00:07:46,800
true about of individual in all the

150
00:07:42,720 --> 00:07:49,680
aspects but these mostly these regular

151
00:07:46,800 --> 00:07:50,880
commands apply to the debugging session

152
00:07:49,680 --> 00:07:54,360


153
00:07:50,880 --> 00:07:56,759
some of them are controlling and getting

154
00:07:54,360 --> 00:07:59,160
information from the debuggie or the

155
00:07:56,759 --> 00:08:02,460
debugging target

156
00:07:59,160 --> 00:08:05,220
there's also a meta command metal

157
00:08:02,460 --> 00:08:08,220
commands or prefixed with a DOT

158
00:08:05,220 --> 00:08:11,039
with these commands apply to the

159
00:08:08,220 --> 00:08:13,919
debugger itself it's not applying into

160
00:08:11,039 --> 00:08:16,740
the debug you but it's the applied it

161
00:08:13,919 --> 00:08:20,220
applies to the debugger

162
00:08:16,740 --> 00:08:24,060
yeah it's a simple controlling the

163
00:08:20,220 --> 00:08:27,000
debugger itself another important 

164
00:08:24,060 --> 00:08:31,760
commands are extension commands almost

165
00:08:27,000 --> 00:08:35,479
all of the events not all of them but

166
00:08:31,760 --> 00:08:40,260
a good portion of them are a start with

167
00:08:35,479 --> 00:08:43,760
 exclamation mark and most of the

168
00:08:40,260 --> 00:08:46,200
events are from these types of command

169
00:08:43,760 --> 00:08:48,839
events all of the events in

170
00:08:46,200 --> 00:08:53,519
hyperactivity follow the same rule or

171
00:08:48,839 --> 00:08:57,180
the same Syntax for the way that you

172
00:08:53,519 --> 00:09:00,600
can write it as I told you events or

173
00:08:57,180 --> 00:09:03,839
extension commands and events have

174
00:09:00,600 --> 00:09:07,019
some parameters that are 

175
00:09:03,839 --> 00:09:10,080
the same for all of them these

176
00:09:07,019 --> 00:09:13,399
parameters are like PID or the process

177
00:09:10,080 --> 00:09:13,399
ID that

178
00:09:13,640 --> 00:09:20,880
specifies a process ID that is the event

179
00:09:17,940 --> 00:09:22,800
should only be triggered in the case of

180
00:09:20,880 --> 00:09:26,700
that process ID

181
00:09:22,800 --> 00:09:29,700
we also can specify cover or the core ID

182
00:09:26,700 --> 00:09:32,300
or which core is responsible when

183
00:09:29,700 --> 00:09:35,640
whenever the event happens on which core

184
00:09:32,300 --> 00:09:39,360
the event should be trigger it's also

185
00:09:35,640 --> 00:09:41,820
some it applies some conditions

186
00:09:39,360 --> 00:09:44,880
there are also codes which is exactly

187
00:09:41,820 --> 00:09:48,420
the custom code custom assembly codes

188
00:09:44,880 --> 00:09:51,000
action that I talked about it before we

189
00:09:48,420 --> 00:09:54,360
will see some examples from this

190
00:09:51,000 --> 00:09:57,140
we also have a conditions conditions are

191
00:09:54,360 --> 00:09:59,220
also assembly codes all of them are in

192
00:09:57,140 --> 00:10:02,880
heximal format

193
00:09:59,220 --> 00:10:06,000
and these conditions 

194
00:10:02,880 --> 00:10:09,000
are about conditional or

195
00:10:06,000 --> 00:10:12,959
unconditional events there are also the

196
00:10:09,000 --> 00:10:15,480
tag script which is which specifies the

197
00:10:12,959 --> 00:10:19,800
script engine codes that should be

198
00:10:15,480 --> 00:10:23,100
executed in the target debugging

199
00:10:19,800 --> 00:10:25,800
and we also have IMM or immediate

200
00:10:23,100 --> 00:10:29,580
messaging mechanism mechanism I will

201
00:10:25,800 --> 00:10:33,480
explain it later but some other event

202
00:10:29,580 --> 00:10:36,360
specific parameters might also be added

203
00:10:33,480 --> 00:10:38,880
to this list in the future okay

204
00:10:36,360 --> 00:10:42,420
now let's see some examples

205
00:10:38,880 --> 00:10:44,579
as you can see in this example which

206
00:10:42,420 --> 00:10:48,000
is a command which is a system assist

207
00:10:44,579 --> 00:10:50,519
call or system call command it starts

208
00:10:48,000 --> 00:10:53,160
with an exclamation mark which shows

209
00:10:50,519 --> 00:10:56,760
that it's an extension command after

210
00:10:53,160 --> 00:10:57,980
that we can see the command itself it's

211
00:10:56,760 --> 00:11:04,220
the scope

212
00:10:57,980 --> 00:11:07,880
then each event might have a specific 

213
00:11:04,220 --> 00:11:12,300
parameter related to the to that event

214
00:11:07,880 --> 00:11:14,940
here's the documentation of hybrid

215
00:11:12,300 --> 00:11:17,519
which debugger as you can see you can go

216
00:11:14,940 --> 00:11:19,740
through the different extension commands

217
00:11:17,519 --> 00:11:22,140
we also have debugging commands meta

218
00:11:19,740 --> 00:11:24,899
commands and 

219
00:11:22,140 --> 00:11:27,180
most importantly 

220
00:11:24,899 --> 00:11:30,440
extension commands each of them are

221
00:11:27,180 --> 00:11:34,880
specific here is specified here

222
00:11:30,440 --> 00:11:37,440
and for example for the syscall let me

223
00:11:34,880 --> 00:11:41,820
yeah here it is

224
00:11:37,440 --> 00:11:44,760
it explains about the basic syntax that

225
00:11:41,820 --> 00:11:46,620
 this command for example for the

226
00:11:44,760 --> 00:11:52,519
syscall the system call numbers

227
00:11:46,620 --> 00:11:57,720
specified so if we see it in a slides 

228
00:11:52,519 --> 00:12:01,079
0x55 or 55 is the syscall number it also

229
00:11:57,720 --> 00:12:03,839
explains about the parameters and what

230
00:12:01,079 --> 00:12:07,380
are these parameters

231
00:12:03,839 --> 00:12:10,140
and most of them are the same for the

232
00:12:07,380 --> 00:12:13,320
events for example as as I told during

233
00:12:10,140 --> 00:12:14,820
the slides we are PID core IMM or

234
00:12:13,320 --> 00:12:17,760
another thing that that are not

235
00:12:14,820 --> 00:12:20,640
specified in the slides like buffer

236
00:12:17,760 --> 00:12:26,279
and a script condition or call okay

237
00:12:20,640 --> 00:12:28,579
let's return to the slides and

238
00:12:26,279 --> 00:12:33,000
see it again

239
00:12:28,579 --> 00:12:35,579
0x55 is the parameter to the commands I

240
00:12:33,000 --> 00:12:39,540
will also specify the PID which means

241
00:12:35,579 --> 00:12:41,399
that only if everything in

242
00:12:39,540 --> 00:12:46,200
hypertension is in hex format for

243
00:12:41,399 --> 00:12:50,279
example like if you just write 55 then

244
00:12:46,200 --> 00:12:54,959
it's interpreted as X so nothing nothing

245
00:12:50,279 --> 00:12:58,440
is in other formats like decimal in this

246
00:12:54,959 --> 00:13:03,019
example I specified that if the process

247
00:12:58,440 --> 00:13:08,220
that execute the syscall is

248
00:13:03,019 --> 00:13:12,480
b40 or hexadecimal form of b40

249
00:13:08,220 --> 00:13:17,279
each process ID is before zero then if

250
00:13:12,480 --> 00:13:20,639
the 55 syscall is executed then an event

251
00:13:17,279 --> 00:13:23,880
should be triggered and this event has

252
00:13:20,639 --> 00:13:28,320
an action which is with which its action

253
00:13:23,880 --> 00:13:32,279
is a script and in this action the

254
00:13:28,320 --> 00:13:36,360
user tries to print I'm called inside

255
00:13:32,279 --> 00:13:39,060
a script message just printed as you

256
00:13:36,360 --> 00:13:44,220
can create clearly see that this script

257
00:13:39,060 --> 00:13:49,220
is the action itself I can also

258
00:13:44,220 --> 00:13:54,360
specify other actions for this event too

259
00:13:49,220 --> 00:13:59,720
another event and first of all let me

260
00:13:54,360 --> 00:13:59,720
show you its documentation it's EPT hook

261
00:14:03,320 --> 00:14:09,959
I will explain about these Hooks and how

262
00:14:06,839 --> 00:14:12,300
how they work how they implement it and

263
00:14:09,959 --> 00:14:14,940
how we can use it later so don't worry

264
00:14:12,300 --> 00:14:17,000
if we don't don't have any idea about

265
00:14:14,940 --> 00:14:20,519
this yet

266
00:14:17,000 --> 00:14:23,760
and I will explain it through later but

267
00:14:20,519 --> 00:14:25,920
for now that we want to just see that

268
00:14:23,760 --> 00:14:28,880
the first parameter specifies the

269
00:14:25,920 --> 00:14:33,600
address the address where we want to put

270
00:14:28,880 --> 00:14:36,200
 put the hook or the equity book

271
00:14:33,600 --> 00:14:40,680
so it's a virtual address

272
00:14:36,200 --> 00:14:43,260
exactly like WinDbg we specify the

273
00:14:40,680 --> 00:14:45,480
command itself it's an extension command

274
00:14:43,260 --> 00:14:48,660
so it just starts with an exclamation

275
00:14:45,480 --> 00:14:50,880
mark after that I specify a parameter

276
00:14:48,660 --> 00:14:54,300
which is the address the virtual address

277
00:14:50,880 --> 00:14:57,060
of where I want to hook which is anti-ex

278
00:14:54,300 --> 00:14:59,060
allocate pool with tag it's the name of

279
00:14:57,060 --> 00:15:02,480
a 

280
00:14:59,060 --> 00:15:06,480
function in a Windows kernel

281
00:15:02,480 --> 00:15:09,839
 NT bang or anti-exclamation mark

282
00:15:06,480 --> 00:15:12,779
means that it comes from an intimage

283
00:15:09,839 --> 00:15:15,540
Rule and the function name is X

284
00:15:12,779 --> 00:15:17,459
allocated you definitely are familiar

285
00:15:15,540 --> 00:15:19,680
with this part for those who are not

286
00:15:17,459 --> 00:15:22,320
familiar probably might be not familiar

287
00:15:19,680 --> 00:15:25,440
I will explain

288
00:15:22,320 --> 00:15:29,699
another thing is core I specify that

289
00:15:25,440 --> 00:15:33,600
only on the second core or the if

290
00:15:29,699 --> 00:15:37,380
this EPT hook happens on the second

291
00:15:33,600 --> 00:15:39,959
chord then we should start checking 

292
00:15:37,380 --> 00:15:43,800
for the conditions

293
00:15:39,959 --> 00:15:48,120
and the condition itself is assembly

294
00:15:43,800 --> 00:15:50,699
code disassembly code just specify 

295
00:15:48,120 --> 00:15:57,899
if I want to disassemble this code if

296
00:15:50,699 --> 00:16:01,380
it's like X or IX RX which puts zero 

297
00:15:57,899 --> 00:16:03,779
or clear every bits or it clears

298
00:16:01,380 --> 00:16:06,779
every bit of there are extrudges there

299
00:16:03,779 --> 00:16:09,800
and then it returns make sure to have a

300
00:16:06,779 --> 00:16:13,500
return on your assembly record because

301
00:16:09,800 --> 00:16:18,000
there might be undefined behavior if you

302
00:16:13,500 --> 00:16:21,899
don't specify a red as you can see it

303
00:16:18,000 --> 00:16:24,300
always returns zero so the code here

304
00:16:21,899 --> 00:16:27,060
will never get never get a chance to get

305
00:16:24,300 --> 00:16:31,019
executed because we don't check anything

306
00:16:27,060 --> 00:16:35,279
we just clear the RX it's just an

307
00:16:31,019 --> 00:16:38,579
example it might it might not have any

308
00:16:35,279 --> 00:16:41,339
meaning but it's just an example after

309
00:16:38,579 --> 00:16:44,360
that we specify

310
00:16:41,339 --> 00:16:48,540
the code which indicates a custom code

311
00:16:44,360 --> 00:16:52,980
and in the custom code I execute to nope

312
00:16:48,540 --> 00:16:57,300
operations and operations and finally I

313
00:16:52,980 --> 00:17:01,320
return so I put the 

314
00:16:57,300 --> 00:17:05,480
the hexadecimal code for this 

315
00:17:01,320 --> 00:17:05,480
this assembly code here

