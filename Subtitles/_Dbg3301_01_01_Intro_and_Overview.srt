1
00:00:00,359 --> 00:00:06,299
Hello and welcome to the first part of

2
00:00:03,600 --> 00:00:09,240
the tutorial reversing with HyperDbg

3
00:00:06,299 --> 00:00:12,599
HyperDbg is the hypervisor based

4
00:00:09,240 --> 00:00:15,059
debugger which is designed for reverse

5
00:00:12,599 --> 00:00:18,600
engineering for analyzing and for

6
00:00:15,059 --> 00:00:21,480
fuzzing and here's probably the first

7
00:00:18,600 --> 00:00:24,180
tutorial about the HyperDbg and it's

8
00:00:21,480 --> 00:00:26,220
the comprehensive guide to HyperDbg

9
00:00:24,060 --> 00:00:29,340
debugger so in this tutorial we're

10
00:00:26,220 --> 00:00:32,520
gonna see how we can use this debugger

11
00:00:29,340 --> 00:00:35,880
and we will see some techniques that we

12
00:00:32,520 --> 00:00:39,600
will use it to enhance our debugging and

13
00:00:35,880 --> 00:00:43,079
reverse engineering so let's see first

14
00:00:39,600 --> 00:00:45,540
of all I'm Sina just a person

15
00:00:43,079 --> 00:00:48,239
interested in programming Windows

16
00:00:45,540 --> 00:00:49,379
internals micro micro architecture and

17
00:00:48,239 --> 00:00:52,800
different things

18
00:00:49,379 --> 00:00:56,820
here's a link to my blog and my Twitter

19
00:00:52,800 --> 00:01:00,239
account and my email I'm also a

20
00:00:56,820 --> 00:01:02,640
developer of HyperDbg debugger and you

21
00:01:00,239 --> 00:01:04,860
can always reach me and ask me questions

22
00:01:02,640 --> 00:01:08,640
if you have any questions about this

23
00:01:04,860 --> 00:01:10,860
debugger this is the first part of the

24
00:01:08,640 --> 00:01:13,860
tutorial and we're gonna see a course

25
00:01:10,860 --> 00:01:17,299
outline of what we're gonna do during

26
00:01:13,860 --> 00:01:21,720
this tutorial 

27
00:01:17,299 --> 00:01:23,820
this part or the first station we

28
00:01:21,720 --> 00:01:27,540
have an introduction an overview of the

29
00:01:23,820 --> 00:01:31,500
debugger we will see the outline of

30
00:01:27,540 --> 00:01:34,500
different different sessions of

31
00:01:31,500 --> 00:01:36,960
this tutorial and we will see some some

32
00:01:34,500 --> 00:01:39,180
of the frequently asked questions then

33
00:01:36,960 --> 00:01:42,119
we will compare HyperDbg with other

34
00:01:39,180 --> 00:01:45,540
debuggers and of course we'll compare it

35
00:01:42,119 --> 00:01:48,180
with WinDbg versus some of its unique

36
00:01:45,540 --> 00:01:50,640
features and will introduce hypervisor

37
00:01:48,180 --> 00:01:53,939
from scratch and and at last we have

38
00:01:50,640 --> 00:01:57,180
summary so that's it that's the overview

39
00:01:53,939 --> 00:01:59,759
of decision and there's a Murphy's

40
00:01:57,180 --> 00:02:03,060
Law let's say anything that can possibly

41
00:01:59,759 --> 00:02:07,259
go wrong will eventually go wrong so

42
00:02:03,060 --> 00:02:11,760
let's see here's the first session of

43
00:02:07,259 --> 00:02:13,200
HyperDbg and first we have some some we

44
00:02:11,760 --> 00:02:16,200
have the answer to some of the

45
00:02:13,200 --> 00:02:19,200
frequently asked questions

46
00:02:16,200 --> 00:02:21,660
the first question is what is HyperDbg

47
00:02:19,200 --> 00:02:23,879
so you probably have some ideas about

48
00:02:21,660 --> 00:02:26,540
this debugger but HyperDbg is an open

49
00:02:23,879 --> 00:02:30,000
source hypervisor assistant debugger

50
00:02:26,540 --> 00:02:33,239
which is used for debugging both user

51
00:02:30,000 --> 00:02:35,640
mode and kernel mode applications and who

52
00:02:33,239 --> 00:02:39,180
who uses HyperDbg? of course

53
00:02:35,640 --> 00:02:41,640
programmers security researchers malware

54
00:02:39,180 --> 00:02:47,340
analyzers and fuzzer programmers use

55
00:02:41,640 --> 00:02:51,239
HyperDbg and why do we need HyperDbg

56
00:02:47,340 --> 00:02:55,019
HyperDbg gives us unique abilities to

57
00:02:51,239 --> 00:02:58,319
use modern processor features and these

58
00:02:55,019 --> 00:03:01,980
processor features are combined in a way

59
00:02:58,319 --> 00:03:05,400
that assists us to visiting the visiting

60
00:03:01,980 --> 00:03:09,000
different targets like we will we can it

61
00:03:05,400 --> 00:03:11,360
will assist us to use so we can use

62
00:03:09,000 --> 00:03:14,580
it in our reverse engineering journey

63
00:03:11,360 --> 00:03:16,200
and another question is what makes

64
00:03:14,580 --> 00:03:19,440
HyperDbg different from classic

65
00:03:16,200 --> 00:03:22,379
debuggers and the thing is that HyperDbg

66
00:03:19,440 --> 00:03:26,220
has a complexly unique architecture

67
00:03:22,379 --> 00:03:29,580
and there are some principles in

68
00:03:26,220 --> 00:03:33,739
designing HyperDbg one of them is we want

69
00:03:29,580 --> 00:03:36,780
to have an OS independent debugger that

70
00:03:33,739 --> 00:03:39,840
wants to use some of the modern

71
00:03:36,780 --> 00:03:42,720
processor features to generate and to

72
00:03:39,840 --> 00:03:46,920
make some new techniques for the reverse

73
00:03:42,720 --> 00:03:49,080
engineering and the thing is this

74
00:03:46,920 --> 00:03:51,180
feature features are not available in

75
00:03:49,080 --> 00:03:53,459
any other debuggers

76
00:03:51,180 --> 00:03:56,299
so HyperDbg is mainly designed for

77
00:03:53,459 --> 00:03:59,879
the reverse engineering

78
00:03:56,299 --> 00:04:02,400
and the differences

79
00:03:59,879 --> 00:04:06,659
between HyperDbg and WinDbg

80
00:04:02,400 --> 00:04:10,680
because of WinDbg which is probably 

81
00:04:06,659 --> 00:04:13,080
one of the things that is may might

82
00:04:10,680 --> 00:04:14,599
be similar to the HyperDbg in some

83
00:04:13,080 --> 00:04:18,060
aspects but

84
00:04:14,599 --> 00:04:23,460
the differences between HyperDbg and

85
00:04:18,060 --> 00:04:25,259
WinDbg is that HyperDbg has a

86
00:04:23,460 --> 00:04:27,540
completely different architecture of

87
00:04:25,259 --> 00:04:31,259
WinDbg mainly operates on a kernel

88
00:04:27,540 --> 00:04:35,160
mode or in ring zero via HyperDbg

89
00:04:31,259 --> 00:04:38,340
running on ring -1 or hypervisor and

90
00:04:35,160 --> 00:04:41,400
HyperDbg provides some of the unique

91
00:04:38,340 --> 00:04:44,940
features that are not available in in OS

92
00:04:41,400 --> 00:04:47,639
level or in kernel mode and besides that

93
00:04:44,940 --> 00:04:51,780
HyperDbg is of course not a simple

94
00:04:47,639 --> 00:04:55,259
 debugger it it comes with a lot of

95
00:04:51,780 --> 00:04:58,259
methods by using Intel VT-x and modern

96
00:04:55,259 --> 00:05:00,120
processor facilities to ease the reverse

97
00:04:58,259 --> 00:05:03,300
engineering analyzing and fuzzing

98
00:05:00,120 --> 00:05:07,380
another question is is it only for a

99
00:05:03,300 --> 00:05:10,860
special processor or can I run it on AMD

100
00:05:07,380 --> 00:05:12,540
or ARM processors the answer to this

101
00:05:10,860 --> 00:05:15,720
question is that the current version of

102
00:05:12,540 --> 00:05:19,560
HyperDbg only supports E64 processors

103
00:05:15,720 --> 00:05:22,620
of Intel at the in the current stage

104
00:05:19,560 --> 00:05:26,180
you cannot run a HyperDbg on an

105
00:05:22,620 --> 00:05:29,580
AMD processor or an arm processor but 

106
00:05:26,180 --> 00:05:32,160
future versions will probably support

107
00:05:29,580 --> 00:05:35,400
other processors as well

108
00:05:32,160 --> 00:05:39,240
and another thing is what generation of

109
00:05:35,400 --> 00:05:41,479
Intel processor supports HyperDbg the

110
00:05:39,240 --> 00:05:45,600
answer to this question

111
00:05:41,479 --> 00:05:48,660
 is that your processor should at

112
00:05:45,600 --> 00:05:52,919
least support Intel EPT which is

113
00:05:48,660 --> 00:05:54,380
introduced on post Nehalem micro

114
00:05:52,919 --> 00:05:57,180
architecture

115
00:05:54,380 --> 00:06:00,539
so 

116
00:05:57,180 --> 00:06:03,600
the sum of functionalities only work on

117
00:06:00,539 --> 00:06:06,080
in on the fourth generally generation or

118
00:06:03,600 --> 00:06:09,979
later generation of Intel processors

119
00:06:06,080 --> 00:06:13,080
and the processor and previous processor

120
00:06:09,979 --> 00:06:15,900
I mean older than the fourth generation

121
00:06:13,080 --> 00:06:17,820
might have some undefined behaviors with

122
00:06:15,900 --> 00:06:22,039
some of the functionalities

123
00:06:17,820 --> 00:06:26,699
as but generally it's recommended to use

124
00:06:22,039 --> 00:06:31,860
an sixth generation or later process or

125
00:06:26,699 --> 00:06:36,199
newer processor for running HyperDbg

126
00:06:31,860 --> 00:06:39,240
and can I use it on Linux or macOS X

127
00:06:36,199 --> 00:06:42,360
well no the current version of hybrid

128
00:06:39,240 --> 00:06:44,360
which is only limited to Windows 10 and

129
00:06:42,360 --> 00:06:49,080
Windows 11.

130
00:06:44,360 --> 00:06:52,199
 but it's one of our top priorities

131
00:06:49,080 --> 00:06:54,180
that we want to support it on Linux

132
00:06:52,199 --> 00:06:56,720
but it's currently only usable on

133
00:06:54,180 --> 00:06:56,720
Windows

134
00:06:56,900 --> 00:07:03,419
 it's all it also supports Windows 11

135
00:07:00,120 --> 00:07:05,280
as well I just mentioned Windows 10 here

136
00:07:03,419 --> 00:07:08,100
but it supports Windows or even as

137
00:07:05,280 --> 00:07:10,979
little and another question is should I

138
00:07:08,100 --> 00:07:13,319
have a separate machine to use HyperDbg

139
00:07:10,979 --> 00:07:16,740
and the answer to this question is of

140
00:07:13,319 --> 00:07:19,620
course not you don't have to have a

141
00:07:16,740 --> 00:07:22,919
you don't have to make some other 

142
00:07:19,620 --> 00:07:24,840
separate machines you can use your own

143
00:07:22,919 --> 00:07:27,599
machine

144
00:07:24,840 --> 00:07:29,099
 but the problem of not having a

145
00:07:27,599 --> 00:07:32,520
separate machine is that you can only

146
00:07:29,099 --> 00:07:34,160
operate on VMI mode I will explain about

147
00:07:32,520 --> 00:07:39,000
the VMI mode

148
00:07:34,160 --> 00:07:41,520
 but in VMI mode you can debug your

149
00:07:39,000 --> 00:07:44,520
own system like local debugging of the

150
00:07:41,520 --> 00:07:47,280
WinDbg and just like WinDbg you

151
00:07:44,520 --> 00:07:49,139
cannot halt your system or you cannot

152
00:07:47,280 --> 00:07:52,680
break or step through the instructions

153
00:07:49,139 --> 00:07:56,340
in your own system but if you use some

154
00:07:52,680 --> 00:07:58,340
 virtualization softwares like

155
00:07:56,340 --> 00:08:02,099
VMware Workstation

156
00:07:58,340 --> 00:08:05,699
and you can also use these features you

157
00:08:02,099 --> 00:08:07,500
can debug how you can debug in the

158
00:08:05,699 --> 00:08:09,599
debugger mode I will explain about the

159
00:08:07,500 --> 00:08:12,080
debugger modulator and all of the

160
00:08:09,599 --> 00:08:14,400
features of HyperDbg are also available

161
00:08:12,080 --> 00:08:17,460
 whenever you want to debug for

162
00:08:14,400 --> 00:08:20,039
example a VMware worker station machine

163
00:08:17,460 --> 00:08:21,780
you can step through the instruction you

164
00:08:20,039 --> 00:08:24,599
can pause the target operating system

165
00:08:21,780 --> 00:08:27,360
all of them are available there

166
00:08:24,599 --> 00:08:29,460
another question is can I use it on a

167
00:08:27,360 --> 00:08:33,479
nested virtualization environment like

168
00:08:29,460 --> 00:08:35,580
VMware virtualbox or hyper-v and the

169
00:08:33,479 --> 00:08:40,979
current version of the hybrid original

170
00:08:35,580 --> 00:08:43,680
casted on VMware Workstation it it I

171
00:08:40,979 --> 00:08:46,440
think VMware Player doesn't support next

172
00:08:43,680 --> 00:08:49,920
virtualization I'm not sure but if

173
00:08:46,440 --> 00:08:52,140
it's not support if we won't support

174
00:08:49,920 --> 00:08:54,060
or not supporting nested

175
00:08:52,140 --> 00:08:57,540
virtualization that hyperdoodle cannot

176
00:08:54,060 --> 00:08:59,760
run it run on it but the future version

177
00:08:57,540 --> 00:09:01,820
probably in the future version will

178
00:08:59,760 --> 00:09:03,899
support all of the virtualization

179
00:09:01,820 --> 00:09:05,279
platforms for example this support

180
00:09:03,899 --> 00:09:09,000
hyper-v

181
00:09:05,279 --> 00:09:12,360
I'm not sure about other virtualization

182
00:09:09,000 --> 00:09:14,040
prop platforms that it might work you

183
00:09:12,360 --> 00:09:16,680
have to test it

184
00:09:14,040 --> 00:09:19,380
but hybrid which is well tested and

185
00:09:16,680 --> 00:09:22,320
VMware Workstation and in a physical

186
00:09:19,380 --> 00:09:24,600
machine without any nested

187
00:09:22,320 --> 00:09:27,060
virtualization environment

188
00:09:24,600 --> 00:09:29,519
 how can I start reading about HyperDbg

189
00:09:27,060 --> 00:09:32,399
internals how does it work and can I

190
00:09:29,519 --> 00:09:34,680
contribute to the HyperDbg hybrid

191
00:09:32,399 --> 00:09:36,420
result is an open source project of

192
00:09:34,680 --> 00:09:39,380
course like all the other projects you

193
00:09:36,420 --> 00:09:42,899
can go and contribute on GitHub

194
00:09:39,380 --> 00:09:45,240
 and the best source for this for

195
00:09:42,899 --> 00:09:47,640
learning internals of HyperDbg is

196
00:09:45,240 --> 00:09:50,640
hypervisor from a scratch tutorial you

197
00:09:47,640 --> 00:09:52,620
can search about this tutorial and for

198
00:09:50,640 --> 00:09:55,740
the contribution you can also follow the

199
00:09:52,620 --> 00:09:58,980
contribution guides in the GitHub page

200
00:09:55,740 --> 00:10:01,700
now let's compare HyperDbg with other

201
00:09:58,980 --> 00:10:01,700
debuggers

202
00:10:02,180 --> 00:10:08,279
 in in the modern projected mode plus

203
00:10:05,700 --> 00:10:10,740
paging enabled systems there are seven

204
00:10:08,279 --> 00:10:14,100
protection links in Enterprise solves

205
00:10:10,740 --> 00:10:16,580
the first ring is Rank 3 which is

206
00:10:14,100 --> 00:10:20,540
designed for user mode application

207
00:10:16,580 --> 00:10:24,600
Ring 2 and ring 1 which is not used are

208
00:10:20,540 --> 00:10:30,660
previously designed for device drivers

209
00:10:24,600 --> 00:10:34,620
and the zeroth ring is the kernel kernel

210
00:10:30,660 --> 00:10:38,940
mode which operating system will run in

211
00:10:34,620 --> 00:10:42,660
this ring of execution ring minus one is

212
00:10:38,940 --> 00:10:45,480
 for hypervisors ring minus two is for

213
00:10:42,660 --> 00:10:49,140
system management more or SMM and ring

214
00:10:45,480 --> 00:10:54,240
minus trees for Intel management engine

215
00:10:49,140 --> 00:10:59,100
 HyperDbg run runs on hypervisor

216
00:10:54,240 --> 00:11:02,700
level or ring -1 while Microsoft WinDbg

217
00:10:59,100 --> 00:11:06,779
or GDB Gano debugger or lldb all of them

218
00:11:02,700 --> 00:11:11,700
run on a ring zero like a kernel mode

219
00:11:06,779 --> 00:11:15,540
module and x64 dbg EDB or part of the

220
00:11:11,700 --> 00:11:18,959
GDB odbg immunity debugger and WinDbg

221
00:11:15,540 --> 00:11:21,899
have some modules to run on user modes

222
00:11:18,959 --> 00:11:24,500
so hybrid BG is more privileged in terms

223
00:11:21,899 --> 00:11:24,500
of terminology

224
00:11:24,720 --> 00:11:30,899
so let's see some of these debuggers

225
00:11:28,279 --> 00:11:33,060
These are the kernel debugger family

226
00:11:30,899 --> 00:11:37,079
members these are these are the

227
00:11:33,060 --> 00:11:39,000
debuggers that are able to debug a

228
00:11:37,079 --> 00:11:42,300
kernel of the operating system first

229
00:11:39,000 --> 00:11:45,240
they have WinDbg which has over 30

230
00:11:42,300 --> 00:11:48,779
years of development and Windows is made

231
00:11:45,240 --> 00:11:51,060
by using WinDbg. WinDbg is not

232
00:11:48,779 --> 00:11:52,940
open source but its source code is

233
00:11:51,060 --> 00:11:55,440
leaked several times

234
00:11:52,940 --> 00:11:58,500
behind the source code of the Windows

235
00:11:55,440 --> 00:12:03,720
then you have lldb which is mostly used

236
00:11:58,500 --> 00:12:05,160
for OS X it's the os60 diagram made by

237
00:12:03,720 --> 00:12:08,100
researchers

238
00:12:05,160 --> 00:12:11,760
and it's also open source

239
00:12:08,100 --> 00:12:15,420
and we have a GDB which is the main

240
00:12:11,760 --> 00:12:18,600
kernel debugger for Linux and it's also

241
00:12:15,420 --> 00:12:21,120
an open source debugger

242
00:12:18,600 --> 00:12:25,260
and HyperDbg is the new family member

243
00:12:21,120 --> 00:12:27,600
to this native debuggers so HyperDbg is

244
00:12:25,260 --> 00:12:30,060
more privileged in terms of rings in

245
00:12:27,600 --> 00:12:32,940
Intel process source it has some unique

246
00:12:30,060 --> 00:12:36,360
features which is mainly designed for

247
00:12:32,940 --> 00:12:38,700
the reverse engineering and it has an

248
00:12:36,360 --> 00:12:41,880
efficient and complicated design which

249
00:12:38,700 --> 00:12:44,940
is hidden by its nature and it combines

250
00:12:41,880 --> 00:12:47,040
the academic Innovation with practical

251
00:12:44,940 --> 00:12:48,180
implementation and is of course open

252
00:12:47,040 --> 00:12:51,959
source

253
00:12:48,180 --> 00:12:54,180
and another another note here is that

254
00:12:51,959 --> 00:12:57,600
you can debug WinDbg or any other

255
00:12:54,180 --> 00:13:00,180
kernel debuggers with HyperDbg

256
00:12:57,600 --> 00:13:04,220
so let's see the differences between

257
00:13:00,180 --> 00:13:07,800
HyperDbg and WinDbg yeah

258
00:13:04,220 --> 00:13:10,139
the main difference is that hyper that

259
00:13:07,800 --> 00:13:12,899
WinDbg is designed for development

260
00:13:10,139 --> 00:13:15,060
purposes while HyperDbg is mainly

261
00:13:12,899 --> 00:13:18,839
designed with a reverse engineering

262
00:13:15,060 --> 00:13:22,740
mindset Microsoft originally made this

263
00:13:18,839 --> 00:13:27,060
debugger to to ease the driver

264
00:13:22,740 --> 00:13:29,160
development and Os development and high

265
00:13:27,060 --> 00:13:31,079
producing on the other hand is the

266
00:13:29,160 --> 00:13:32,720
mainly designed to be used for the

267
00:13:31,079 --> 00:13:36,240
reverse engineering

268
00:13:32,720 --> 00:13:39,139
so in the in scenarios when you don't

269
00:13:36,240 --> 00:13:42,720
have any idea about the target debugging

270
00:13:39,139 --> 00:13:45,480
you can it's better to use HyperDbg but

271
00:13:42,720 --> 00:13:47,639
of course if you want to develop some

272
00:13:45,480 --> 00:13:51,240
drivers then windy which is a better

273
00:13:47,639 --> 00:13:53,880
option when you have source code of

274
00:13:51,240 --> 00:13:56,339
drivers better to use WinDbg but if you

275
00:13:53,880 --> 00:13:59,220
want to understand the mechanism in in

276
00:13:56,339 --> 00:14:01,800
an application when when you don't have

277
00:13:59,220 --> 00:14:04,220
any access to the source code or you

278
00:14:01,800 --> 00:14:08,060
want to analyze a malware

279
00:14:04,220 --> 00:14:10,800
or analyze the closed source application

280
00:14:08,060 --> 00:14:13,380
or or if there's symbols of an

281
00:14:10,800 --> 00:14:15,899
application is partially available then

282
00:14:13,380 --> 00:14:20,240
HyperDbg gives you more features to

283
00:14:15,899 --> 00:14:24,060
explore explore your device

284
00:14:20,240 --> 00:14:26,779
 HyperDbg is not a classic debugger

285
00:14:24,060 --> 00:14:31,019
the intention of hypers

286
00:14:26,779 --> 00:14:36,060
we don't really not gonna reinvent

287
00:14:31,019 --> 00:14:38,339
the Wheel by designing HyperDbg 

288
00:14:36,060 --> 00:14:40,980
the thing is that the modern binary

289
00:14:38,339 --> 00:14:43,800
files or modern files use sophisticated

290
00:14:40,980 --> 00:14:46,260
methods and techniques to obfuscute

291
00:14:43,800 --> 00:14:49,519
their internals for example most of the

292
00:14:46,260 --> 00:14:51,720
malvers use different anti-debagging

293
00:14:49,519 --> 00:14:54,240
anti-hypervisor techniques to avoid

294
00:14:51,720 --> 00:14:57,000
showing for example in the in terms

295
00:14:54,240 --> 00:15:00,839
of malware they avoid showing their

296
00:14:57,000 --> 00:15:05,880
malicious behavior when a debugger is

297
00:15:00,839 --> 00:15:08,459
running so  on the other hand

298
00:15:05,880 --> 00:15:11,579
the classic debuggers are not suitable

299
00:15:08,459 --> 00:15:13,199
to analyze the internal mechanisms that

300
00:15:11,579 --> 00:15:15,720
are buried into the complicated

301
00:15:13,199 --> 00:15:18,720
operating system module in these cases

302
00:15:15,720 --> 00:15:22,980
it's better to use the HyperDbg debugger

303
00:15:18,720 --> 00:15:27,060
being hyper debugger gives high

304
00:15:22,980 --> 00:15:29,880
producing new magical features and this

305
00:15:27,060 --> 00:15:32,279
magical features exists in hyper high

306
00:15:29,880 --> 00:15:35,180
producer we will see all of them most of

307
00:15:32,279 --> 00:15:38,420
them in in the coming parts

308
00:15:35,180 --> 00:15:41,279
and these features were drastically

309
00:15:38,420 --> 00:15:42,600
enhance your reverse engineering

310
00:15:41,279 --> 00:15:47,579
Journey

311
00:15:42,600 --> 00:15:50,519
 and HyperDbg is more transparent by

312
00:15:47,579 --> 00:15:53,579
its nature for example generally

313
00:15:50,519 --> 00:15:56,100
being only in hypervisor level makes

314
00:15:53,579 --> 00:15:59,160
high producing more transparent than

315
00:15:56,100 --> 00:16:02,519
having dvg and another thing is HyperDbg

316
00:15:59,160 --> 00:16:04,680
didn't use any debugging

317
00:16:02,519 --> 00:16:06,899
related API

318
00:16:04,680 --> 00:16:10,740
so even the operating system itself

319
00:16:06,899 --> 00:16:14,160
doesn't have any ideas in debug by the

320
00:16:10,740 --> 00:16:17,399
HyperDbg and it's also not the only

321
00:16:14,160 --> 00:16:20,339
thing high producer also hides tries to

322
00:16:17,399 --> 00:16:23,160
hide itself from some of the timing

323
00:16:20,339 --> 00:16:27,079
mutual architectural timing attacks that

324
00:16:23,160 --> 00:16:29,820
reveals the presence of hypervisor and

325
00:16:27,079 --> 00:16:32,160
transparency is always a priority for

326
00:16:29,820 --> 00:16:34,620
the hyper divisions under active

327
00:16:32,160 --> 00:16:36,839
development another difference between

328
00:16:34,620 --> 00:16:38,759
hyper Division and wind images that

329
00:16:36,839 --> 00:16:40,560
HyperDbg is the coming to driven

330
00:16:38,759 --> 00:16:42,839
debugger which is open source and

331
00:16:40,560 --> 00:16:45,060
everyone could contribute to the to this

332
00:16:42,839 --> 00:16:47,300
project while of individual is not open

333
00:16:45,060 --> 00:16:50,399
source

334
00:16:47,300 --> 00:16:53,399
works on multiple architecture like arm

335
00:16:50,399 --> 00:16:56,279
process source AMD process source while

336
00:16:53,399 --> 00:17:00,420
HyperDbg only works on an Intel

337
00:16:56,279 --> 00:17:03,480
process source or in x64-bit

338
00:17:00,420 --> 00:17:05,400
processors but also you can also debug

339
00:17:03,480 --> 00:17:11,220
x80 

340
00:17:05,400 --> 00:17:13,799
6 or 32-bit programs by using HyperDbg

341
00:17:11,220 --> 00:17:17,280
and HyperDbg is tremendously faster

342
00:17:13,799 --> 00:17:19,860
than wind images because it's it comes

343
00:17:17,280 --> 00:17:21,839
with a VMS route more compatible script

344
00:17:19,860 --> 00:17:24,360
engine and everything is performed in

345
00:17:21,839 --> 00:17:27,720
the kernel so it has a different design

346
00:17:24,360 --> 00:17:29,640
and nothing is passed to the debugger

347
00:17:27,720 --> 00:17:32,820
everything is done in the kernel and

348
00:17:29,640 --> 00:17:35,880
this is thousands of times faster than

349
00:17:32,820 --> 00:17:38,039
 higher than vinivigi there's also an

350
00:17:35,880 --> 00:17:43,980
academic paper about the hyper energy

351
00:17:38,039 --> 00:17:47,940
you can see how how we compared a hyper

352
00:17:43,980 --> 00:17:50,520
dbg with WinDbg and how HyperDbg

353
00:17:47,940 --> 00:17:52,799
is faster than WinDbg and why it's

354
00:17:50,520 --> 00:17:56,640
faster

355
00:17:52,799 --> 00:17:59,700
so if in conclusion if if I want to

356
00:17:56,640 --> 00:18:03,360
present the pros and cons minivity is

357
00:17:59,700 --> 00:18:05,520
best for source code debugging and

358
00:18:03,360 --> 00:18:09,240
supports multiple architecture as well

359
00:18:05,520 --> 00:18:12,179
as many programming scripting 

360
00:18:09,240 --> 00:18:14,640
languages for many programming

361
00:18:12,179 --> 00:18:17,580
languages for a scripted for a script

362
00:18:14,640 --> 00:18:20,940
engine like JavaScript and the cons is

363
00:18:17,580 --> 00:18:22,799
not open source and it's also a slowing

364
00:18:20,940 --> 00:18:26,400
debugging function with high rate of

365
00:18:22,799 --> 00:18:28,500
execution for the HyperDbg it has a

366
00:18:26,400 --> 00:18:30,720
very fast descriptive engine tens of

367
00:18:28,500 --> 00:18:32,460
innovative features that they are

368
00:18:30,720 --> 00:18:35,340
designed for the reversal engineering

369
00:18:32,460 --> 00:18:38,700
and it's also more transparent than vdg

370
00:18:35,340 --> 00:18:41,160
and for the cons it doesn't support many

371
00:18:38,700 --> 00:18:43,740
architectures it only supports Intel

372
00:18:41,160 --> 00:18:45,660
processors in the current version and

373
00:18:43,740 --> 00:18:48,679
some of the commands are not compatible

374
00:18:45,660 --> 00:18:48,679
with patchgard

375
00:18:48,900 --> 00:18:54,120
so let's see some of these unique

376
00:18:51,299 --> 00:18:56,760
features in the first version of HyperDbg

377
00:18:54,120 --> 00:18:58,980
which supports classic hidden

378
00:18:56,760 --> 00:19:01,799
hooks that I produce has we will talk

379
00:18:58,980 --> 00:19:04,740
about all of these features in in the

380
00:19:01,799 --> 00:19:07,140
coming sessions of this tutorial so

381
00:19:04,740 --> 00:19:11,039
don't worry about it but for now HyperDbg

382
00:19:07,140 --> 00:19:13,559
supports classical EPT hooks I

383
00:19:11,039 --> 00:19:16,679
like hidden breakpoints as well as

384
00:19:13,559 --> 00:19:20,100
inline EFT hooks or detours the style

385
00:19:16,679 --> 00:19:23,220
Hooks and it also has some ministering

386
00:19:20,100 --> 00:19:25,080
for memory it tries to emulate the

387
00:19:23,220 --> 00:19:28,580
hardware debug register without any

388
00:19:25,080 --> 00:19:32,940
limitation in the content size

389
00:19:28,580 --> 00:19:37,020
it also has some syscall and sister hooks

390
00:19:32,940 --> 00:19:41,160
you can disable you can hook any

391
00:19:37,020 --> 00:19:43,260
execution of any system call in the

392
00:19:41,160 --> 00:19:46,679
operating system and when it returns to

393
00:19:43,260 --> 00:19:49,679
the user mode HyperDbg has cpuid

394
00:19:46,679 --> 00:19:54,380
Hooks and monitors as well as rdmsr and

395
00:19:49,679 --> 00:19:57,840
WR MSR hooks besides that rdtsc

396
00:19:54,380 --> 00:20:00,860
rdtscp and rdpmc hooks are also

397
00:19:57,840 --> 00:20:04,980
available in this divider

398
00:20:00,860 --> 00:20:07,200
 it hooks any interactions with the

399
00:20:04,980 --> 00:20:12,000
hardware debug registers and monitors

400
00:20:07,200 --> 00:20:15,299
them it also has a unlimited number of i

401
00:20:12,000 --> 00:20:17,460
o port and unlimited number of old IO

402
00:20:15,299 --> 00:20:20,000
port old instruction booking and

403
00:20:17,460 --> 00:20:24,240
monitoring so you can

404
00:20:20,000 --> 00:20:28,740
debug PMIO devices as well as MMIO

405
00:20:24,240 --> 00:20:32,100
devices and then it has a feature to who

406
00:20:28,740 --> 00:20:37,160
can monitor exceptions or external

407
00:20:32,100 --> 00:20:40,559
interrupts it can monitor the IDT

408
00:20:37,160 --> 00:20:44,360
and you you can also use hybrid DVD to

409
00:20:40,559 --> 00:20:46,220
run automated scripts it has a strong

410
00:20:44,360 --> 00:20:48,799
transparent mode

411
00:20:46,220 --> 00:20:51,120
which is an einst debugging and

412
00:20:48,799 --> 00:20:54,240
anti-hypervisor resistance it's under

413
00:20:51,120 --> 00:20:57,299
development it won't guarantee 100

414
00:20:54,240 --> 00:21:00,660
percent of transparency but it's quite

415
00:20:57,299 --> 00:21:03,720
good and it also runs custom assembly

416
00:21:00,660 --> 00:21:06,480
codes in both VMS root and VMX non-root

417
00:21:03,720 --> 00:21:09,480
mode in the kernel and the user mode

418
00:21:06,480 --> 00:21:13,320
as the last processor specific and trade

419
00:21:09,480 --> 00:21:16,260
specific commands be the powerful kernel

420
00:21:13,320 --> 00:21:19,799
side scripting engine it also supports

421
00:21:16,260 --> 00:21:24,240
symbol for a symbol interpretations

422
00:21:19,799 --> 00:21:28,260
or a symbol parsing it supports

423
00:21:24,240 --> 00:21:31,559
the parsing of PDF files it has an event

424
00:21:28,260 --> 00:21:34,860
forwarding mechanism and as well as

425
00:21:31,559 --> 00:21:37,559
transparent breakpoint handler some

426
00:21:34,860 --> 00:21:40,740
other concept like short circuiting

427
00:21:37,559 --> 00:21:43,380
events will we will we will talk

428
00:21:40,740 --> 00:21:46,559
about these features later in this

429
00:21:43,380 --> 00:21:48,659
tutorial and how we can track the

430
00:21:46,559 --> 00:21:52,580
execution by using hyper Division and

431
00:21:48,659 --> 00:21:56,039
that also has some some of the very some

432
00:21:52,580 --> 00:21:59,460
custom Scripts

433
00:21:56,039 --> 00:22:01,799
so the best way to learn HyperDbg

434
00:21:59,460 --> 00:22:04,679
internals and cut and start contributing

435
00:22:01,799 --> 00:22:08,460
on this project is start reading the

436
00:22:04,679 --> 00:22:12,299
hypervisor from scratch this is this

437
00:22:08,460 --> 00:22:14,880
tutorial is mainly designed to teach a

438
00:22:12,299 --> 00:22:17,340
hypervisor concept so you can learn it

439
00:22:14,880 --> 00:22:19,460
and after that high produce is mainly an

440
00:22:17,340 --> 00:22:21,960
extension to the hypervisor from a

441
00:22:19,460 --> 00:22:25,500
scratch the hypervisor from scratch

442
00:22:21,960 --> 00:22:29,460
source code is enhanced the HyperDbg is

443
00:22:25,500 --> 00:22:31,140
made based on this source code and this

444
00:22:29,460 --> 00:22:33,299
is these are other parts of this

445
00:22:31,140 --> 00:22:36,020
tutorial so make sure to read it if you

446
00:22:33,299 --> 00:22:41,520
want to contribute on this project

447
00:22:36,020 --> 00:22:45,480
 and the last thing is if you have any

448
00:22:41,520 --> 00:22:48,960
questions you can ask your questions in

449
00:22:45,480 --> 00:22:52,500
the GitHub discussions and I will see

450
00:22:48,960 --> 00:22:55,460
you in the next section so thanks a lot

451
00:22:52,500 --> 00:22:55,460
for watching it

