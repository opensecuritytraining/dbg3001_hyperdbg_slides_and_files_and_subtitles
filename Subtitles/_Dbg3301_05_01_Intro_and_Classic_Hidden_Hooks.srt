1
00:00:00,000 --> 00:00:06,720
hi everyone welcome to the next part of

2
00:00:03,300 --> 00:00:08,940
the tutorial reversing with HyperDbg in

3
00:00:06,720 --> 00:00:12,059
the previous part we see the basic

4
00:00:08,940 --> 00:00:14,280
principles about the script engine and

5
00:00:12,059 --> 00:00:16,800
we see how it works and wrote some

6
00:00:14,280 --> 00:00:19,080
simple scripts in this part we're gonna

7
00:00:16,800 --> 00:00:22,680
combine our knowledge from the script

8
00:00:19,080 --> 00:00:26,460
Engine with EPT hooks and see how we can

9
00:00:22,680 --> 00:00:28,920
use EPT hooks in our debug so here is a

10
00:00:26,460 --> 00:00:32,220
brief outline about what we're gonna

11
00:00:28,920 --> 00:00:35,340
talk in this part actually we're going

12
00:00:32,220 --> 00:00:38,100
to talk about use cases and examples of

13
00:00:35,340 --> 00:00:41,879
classic hidden hooks detours starting

14
00:00:38,100 --> 00:00:44,520
books and different memory monitor or

15
00:00:41,879 --> 00:00:47,219
emulating a hardware debug registers

16
00:00:44,520 --> 00:00:50,100
also we're gonna see how we can manage

17
00:00:47,219 --> 00:00:52,620
different events in a produce G like

18
00:00:50,100 --> 00:00:56,640
image list or disabling enabling

19
00:00:52,620 --> 00:00:59,879
clearing or managing events and we will

20
00:00:56,640 --> 00:01:03,239
talk about flashing buffers at last we

21
00:00:59,879 --> 00:01:05,339
have have a pads on which in which we

22
00:01:03,239 --> 00:01:08,460
come back or script engine knowledge

23
00:01:05,339 --> 00:01:11,580
with the EPT debugging techniques so

24
00:01:08,460 --> 00:01:14,640
let's talk about classic hidden books

25
00:01:11,580 --> 00:01:19,080
classic hidden books or you might know

26
00:01:14,640 --> 00:01:23,040
it as a hidden breakpoints are just like

27
00:01:19,080 --> 00:01:25,320
regular breakpoints box and implemented

28
00:01:23,040 --> 00:01:28,640
with some unique features actually it's

29
00:01:25,320 --> 00:01:32,460
different than a regular very coin box

30
00:01:28,640 --> 00:01:36,060
it uses the same concept by using these

31
00:01:32,460 --> 00:01:38,759
hooks you're able to hook the hook

32
00:01:36,060 --> 00:01:41,159
codes anywhere in the memory from the

33
00:01:38,759 --> 00:01:43,680
kernel mode to user mode the thing is

34
00:01:41,159 --> 00:01:46,500
that even you can manage to put a hook

35
00:01:43,680 --> 00:01:49,619
on the middle of a function or it's up

36
00:01:46,500 --> 00:01:53,340
to you wherever you want you can put put

37
00:01:49,619 --> 00:01:56,520
a hook Piper Luigi in this case is

38
00:01:53,340 --> 00:01:59,939
optimized to handle these events which

39
00:01:56,520 --> 00:02:04,020
are book events substantially faster

40
00:01:59,939 --> 00:02:06,960
than than simple kernel interrupt

41
00:02:04,020 --> 00:02:09,060
handlers I I will tell you why I mean I

42
00:02:06,960 --> 00:02:11,400
I want to talk about the kernel

43
00:02:09,060 --> 00:02:14,280
interrupt handlers layer the unique

44
00:02:11,400 --> 00:02:17,700
thing about HyperDbg or these hidden

45
00:02:14,280 --> 00:02:21,239
hooks is that it won't notify the user

46
00:02:17,700 --> 00:02:24,000
mode code or the even the OS kernel that

47
00:02:21,239 --> 00:02:27,300
the breakpoint is triggered or as it

48
00:02:24,000 --> 00:02:30,239
handled in the hypervisor level so the

49
00:02:27,300 --> 00:02:33,239
OS or the application doesn't have any

50
00:02:30,239 --> 00:02:35,099
idea whether it's currently a break when

51
00:02:33,239 --> 00:02:37,260
it's triggered and something is

52
00:02:35,099 --> 00:02:39,300
currently debugging the code if hidden

53
00:02:37,260 --> 00:02:42,000
break points are not visible

54
00:02:39,300 --> 00:02:43,680
today whether the kernel mode code wants

55
00:02:42,000 --> 00:02:46,200
to create for example a hash algorithm

56
00:02:43,680 --> 00:02:48,840
from the memory then it won't be

57
00:02:46,200 --> 00:02:51,300
notified that the memory is modified

58
00:02:48,840 --> 00:02:53,879
even though the modified the memory is

59
00:02:51,300 --> 00:02:57,660
modified but it's hidden in the EPT

60
00:02:53,879 --> 00:03:00,360
level so there's nothing that hashes

61
00:02:57,660 --> 00:03:03,780
the same as before as as it's like

62
00:03:00,360 --> 00:03:06,720
nothing is changed and if the

63
00:03:03,780 --> 00:03:09,599
application uses a move instruction to

64
00:03:06,720 --> 00:03:12,480
read that address of the memory it seems

65
00:03:09,599 --> 00:03:16,319
that nothing is changed there just like

66
00:03:12,480 --> 00:03:19,680
normal routines and the HyperDbg

67
00:03:16,319 --> 00:03:22,379
exports or brings these EPT hooks as

68
00:03:19,680 --> 00:03:25,920
events and as you can as you probably

69
00:03:22,379 --> 00:03:28,800
know whenever anything is like formed in

70
00:03:25,920 --> 00:03:31,260
an event style or in an image style

71
00:03:28,800 --> 00:03:34,739
event in the HyperDbg then you can

72
00:03:31,260 --> 00:03:37,319
write customer scripts or create logs or

73
00:03:34,739 --> 00:03:40,799
you can halt the system run custom codes

74
00:03:37,319 --> 00:03:43,860
or whatever you want that is true for

75
00:03:40,799 --> 00:03:47,640
all the events so let's see some of the

76
00:03:43,860 --> 00:03:51,840
use cases when we want to use the APT

77
00:03:47,640 --> 00:03:54,959
hooks so you might think how an EPT hook

78
00:03:51,840 --> 00:03:57,659
might help us well the first thing is

79
00:03:54,959 --> 00:04:00,659
that it's not visible for example if you

80
00:03:57,659 --> 00:04:03,959
are debugging a malware that is that

81
00:04:00,659 --> 00:04:06,180
extensively monitor its memory then to

82
00:04:03,959 --> 00:04:08,340
check whether and if something is

83
00:04:06,180 --> 00:04:10,799
changed or not or if there's a

84
00:04:08,340 --> 00:04:13,080
breakpoint or not then by using hybrid

85
00:04:10,799 --> 00:04:15,299
imagery the malware won't I won't be

86
00:04:13,080 --> 00:04:18,180
notified that it that its memory is

87
00:04:15,299 --> 00:04:22,019
changed the other thing is that if we're

88
00:04:18,180 --> 00:04:25,320
gonna create logs from the parameters of

89
00:04:22,019 --> 00:04:27,720
a function or we can whether we want to

90
00:04:25,320 --> 00:04:30,300
see whether a special part of the

91
00:04:27,720 --> 00:04:33,360
function is ever called or not we can

92
00:04:30,300 --> 00:04:35,820
use apt hooks as it's so fast faster

93
00:04:33,360 --> 00:04:38,340
than wind origin in this case is another

94
00:04:35,820 --> 00:04:40,259
thing is that we can easily by using a

95
00:04:38,340 --> 00:04:43,320
simple script we can and change the

96
00:04:40,259 --> 00:04:45,780
programs normal execution flow we can

97
00:04:43,320 --> 00:04:48,720
just write a simple s script and

98
00:04:45,780 --> 00:04:52,020
completely change the program in a new

99
00:04:48,720 --> 00:04:54,660
direction or another thing is that we

100
00:04:52,020 --> 00:04:57,960
can ignore the execution of a function

101
00:04:54,660 --> 00:05:00,540
we can simply just bypass everything and

102
00:04:57,960 --> 00:05:02,720
the function is never executed we can

103
00:05:00,540 --> 00:05:05,820
just ignore it and return

104
00:05:02,720 --> 00:05:09,000
the application won't be notified that

105
00:05:05,820 --> 00:05:13,199
the target function is ignored so here

106
00:05:09,000 --> 00:05:15,840
are some cases when we can when we can

107
00:05:13,199 --> 00:05:20,160
use the APT hooks you can apply Equity

108
00:05:15,840 --> 00:05:23,340
hooks by using this command apt hook

109
00:05:20,160 --> 00:05:26,880
which is an extension command and as you

110
00:05:23,340 --> 00:05:29,100
can as you probably know EPT holes are

111
00:05:26,880 --> 00:05:32,580
those hooks that are implemented in the

112
00:05:29,100 --> 00:05:35,100
EPT or extended page table of Intel

113
00:05:32,580 --> 00:05:37,860
processor which is a if it is the

114
00:05:35,100 --> 00:05:40,440
hypervisor mechanism used for handling

115
00:05:37,860 --> 00:05:43,860
page tables in the high hypervisor level

116
00:05:40,440 --> 00:05:47,039
so we use them to bring these features

117
00:05:43,860 --> 00:05:50,340
to hybrid divider let's see some of

118
00:05:47,039 --> 00:05:54,000
the examples in the first example we

119
00:05:50,340 --> 00:05:57,720
just put a simple EPT hook on a ex

120
00:05:54,000 --> 00:06:00,060
allocate pulley tag function that is

121
00:05:57,720 --> 00:06:02,639
responsible for allocating pool memories

122
00:06:00,060 --> 00:06:06,060
on the kernel of the Windows this works

123
00:06:02,639 --> 00:06:09,240
simply like a breakpoint but the thing

124
00:06:06,060 --> 00:06:11,400
is that it's hidden nothing can

125
00:06:09,240 --> 00:06:15,060
understand that there is a hook on the

126
00:06:11,400 --> 00:06:18,240
target memory address we can also put a

127
00:06:15,060 --> 00:06:22,860
direct address or a static address to

128
00:06:18,240 --> 00:06:26,639
the EPT hook everything is that we can

129
00:06:22,860 --> 00:06:30,660
have an S script that is running 

130
00:06:26,639 --> 00:06:33,000
whenever the EPT hook is executed for

131
00:06:30,660 --> 00:06:36,180
example in this case I wanna show the

132
00:06:33,000 --> 00:06:40,639
parameters as you can as you probably

133
00:06:36,180 --> 00:06:43,080
know in each 64

134
00:06:40,639 --> 00:06:46,620
functions in Windows everything is

135
00:06:43,080 --> 00:06:49,319
tasked in a fast video fascal coming

136
00:06:46,620 --> 00:06:52,740
convention so you can expect that

137
00:06:49,319 --> 00:06:57,960
whenever you run this command then the

138
00:06:52,740 --> 00:07:02,100
parameters are passed in the rcx rdx r8

139
00:06:57,960 --> 00:07:04,680
and r9 and here we put a simple printf

140
00:07:02,100 --> 00:07:07,319
to print these parameters another thing

141
00:07:04,680 --> 00:07:10,440
is that we can easily manipulate the

142
00:07:07,319 --> 00:07:13,800
functions execution flow for example

143
00:07:10,440 --> 00:07:16,880
in this case we set the zero flag and

144
00:07:13,800 --> 00:07:19,860
change the direction of a jump

145
00:07:16,880 --> 00:07:23,039
which is the jump is located in this

146
00:07:19,860 --> 00:07:28,919
address we have an example on the Hanson

147
00:07:23,039 --> 00:07:31,020
section later in this part it also we

148
00:07:28,919 --> 00:07:33,060
are also able to completely ignore the

149
00:07:31,020 --> 00:07:35,639
execution of a function for example if

150
00:07:33,060 --> 00:07:40,800
we want to set an EPV hook here in this

151
00:07:35,639 --> 00:07:43,560
address we can just put rx2 to 1 which

152
00:07:40,800 --> 00:07:46,139
means return true and then change the

153
00:07:43,560 --> 00:07:49,319
art Bridges there to to the address that

154
00:07:46,139 --> 00:07:52,319
that is currently pushed in the stack

155
00:07:49,319 --> 00:07:55,319
like if you want to return something

156
00:07:52,319 --> 00:07:58,380
else because everything that needs to be

157
00:07:55,319 --> 00:08:00,900
returned by function by default or task

158
00:07:58,380 --> 00:08:05,400
the r is registered so you can change

159
00:08:00,900 --> 00:08:08,580
the Rix register to your desired 

160
00:08:05,400 --> 00:08:12,000
return value and then use this command

161
00:08:08,580 --> 00:08:14,160
to simply change the rip address or the

162
00:08:12,000 --> 00:08:17,340
instruction pointer address and ignore

163
00:08:14,160 --> 00:08:21,060
the execution of a special function so

164
00:08:17,340 --> 00:08:24,180
let's see some demos I will use the

165
00:08:21,060 --> 00:08:26,879
exact same script here let me copy it

166
00:08:24,180 --> 00:08:29,460
first of all I I create a Windows

167
00:08:26,879 --> 00:08:32,940
divider to bypass the driver signature

168
00:08:29,460 --> 00:08:37,140
enforcement so I'm gonna

169
00:08:32,940 --> 00:08:41,039
start the my VM again meanwhile I wanna

170
00:08:37,140 --> 00:08:45,120
open the HyperDbg from its source code

171
00:08:41,039 --> 00:08:49,140
I use HyperDbg so I go to pin please

172
00:08:45,120 --> 00:08:52,700
open the high producer CLI here

173
00:08:49,140 --> 00:08:55,260
so I'm gonna put it here and I'm gonna

174
00:08:52,700 --> 00:08:59,360
put 

175
00:08:55,260 --> 00:08:59,360
in the end here yeah

176
00:08:59,580 --> 00:09:05,220
my name is not working yeah it's not

177
00:09:03,540 --> 00:09:09,060
works

178
00:09:05,220 --> 00:09:13,399
as you can see I run and I want to

179
00:09:09,060 --> 00:09:13,399
listen to the hybrid images pipe

180
00:09:14,279 --> 00:09:19,620
I mean by

181
00:09:16,980 --> 00:09:22,760
connect the

182
00:09:19,620 --> 00:09:22,760
debuggee part

183
00:09:30,600 --> 00:09:38,100
so yeah it's currently synchronizing

184
00:09:34,200 --> 00:09:40,260
the symbols here now the the thing is

185
00:09:38,100 --> 00:09:42,420
that the debugger is currently around so

186
00:09:40,260 --> 00:09:45,500
I pause it by pressing the Ctrl C

187
00:09:42,420 --> 00:09:49,320
everything is passed here

188
00:09:45,500 --> 00:09:54,260
and I'm gonna execute the exact same

189
00:09:49,320 --> 00:09:54,260
script I don't know why this

190
00:09:54,620 --> 00:10:00,959
happens here and this puts the simple

191
00:09:57,839 --> 00:10:03,959
EPT hook breakpoint on ntx allocate with

192
00:10:00,959 --> 00:10:06,380
tech and run and create a block from its

193
00:10:03,959 --> 00:10:06,380
parameters

194
00:10:06,600 --> 00:10:09,320
foreign

195
00:10:13,680 --> 00:10:20,640
so here we go here are the parameters if

196
00:10:16,740 --> 00:10:22,980
I just want to show you guys I just

197
00:10:20,640 --> 00:10:25,620
press Ctrl C again these are the

198
00:10:22,980 --> 00:10:28,980
parameters to this function and you can

199
00:10:25,620 --> 00:10:31,440
see that experimeters are passed this is

200
00:10:28,980 --> 00:10:33,480
a special function sure you can use if

201
00:10:31,440 --> 00:10:36,360
else a statement to check whether a

202
00:10:33,480 --> 00:10:40,320
special thing happens or also you can

203
00:10:36,360 --> 00:10:41,940
put a instructor put an EPT hook to the

204
00:10:40,320 --> 00:10:45,180
last instruction or the return

205
00:10:41,940 --> 00:10:47,220
instruction that this function to see

206
00:10:45,180 --> 00:10:49,579
what are the addresses that are

207
00:10:47,220 --> 00:10:53,160
allocated by the it's allocated

208
00:10:49,579 --> 00:10:56,100
this is a you can have Innovative

209
00:10:53,160 --> 00:10:59,060
methods to create your debug view or to

210
00:10:56,100 --> 00:10:59,060
debug or debug

