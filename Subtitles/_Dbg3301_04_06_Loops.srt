1
00:00:00,179 --> 00:00:06,779
same thing is also true about the loops

2
00:00:03,959 --> 00:00:09,360
or there are different different looks

3
00:00:06,779 --> 00:00:13,019
in the escape changing HyperDbg just

4
00:00:09,360 --> 00:00:16,139
like C language hypertug supports four

5
00:00:13,019 --> 00:00:19,619
byte and dual loops for example if you

6
00:00:16,139 --> 00:00:23,279
have one to have a very simple Cube here

7
00:00:19,619 --> 00:00:25,500
we can use this syntax which is never

8
00:00:23,279 --> 00:00:28,380
see but just keep in mind that

9
00:00:25,500 --> 00:00:33,360
everything is interpreted as hex so it

10
00:00:28,380 --> 00:00:35,940
means that in decimal it's 16 just it's

11
00:00:33,360 --> 00:00:40,079
not just 10 in decimals it should be

12
00:00:35,940 --> 00:00:42,960
converted to a hexadecimal so also you

13
00:00:40,079 --> 00:00:47,040
can use the nested loops and use the

14
00:00:42,960 --> 00:00:49,680
loops together in a nested way it's

15
00:00:47,040 --> 00:00:51,120
pretty okay and supported also keep in

16
00:00:49,680 --> 00:00:52,700
mind that you don't need to define

17
00:00:51,120 --> 00:00:57,059
anything like

18
00:00:52,700 --> 00:01:00,840
defining the variable I or J here it's

19
00:00:57,059 --> 00:01:04,080
pretty okay as it doesn't have have any

20
00:01:00,840 --> 00:01:07,680
types then when you find here it means

21
00:01:04,080 --> 00:01:11,520
that the variable is assigned to it's

22
00:01:07,680 --> 00:01:14,340
interpreted as a 64-bit value the same

23
00:01:11,520 --> 00:01:17,720
is also true about the while and do

24
00:01:14,340 --> 00:01:21,840
while it's pretty simple if you want to

25
00:01:17,720 --> 00:01:24,360
put just variable here and assign an

26
00:01:21,840 --> 00:01:28,020
expression to the variable and use the

27
00:01:24,360 --> 00:01:30,360
while statement and in the in the body

28
00:01:28,020 --> 00:01:33,720
of the while statement you can decrease

29
00:01:30,360 --> 00:01:35,579
or increase or change the condition of

30
00:01:33,720 --> 00:01:38,939
the while statement the same is also

31
00:01:35,579 --> 00:01:42,860
true about the do wire statement and the

32
00:01:38,939 --> 00:01:47,159
difference is that pretty simple you can

33
00:01:42,860 --> 00:01:50,340
do a body which will be executed at

34
00:01:47,159 --> 00:01:52,439
least once but it's not true about the Y

35
00:01:50,340 --> 00:01:55,619
if the condition of the Y is not true

36
00:01:52,439 --> 00:01:58,439
then you never get a chance to execute

37
00:01:55,619 --> 00:02:01,820
the body of the Y but it's not true

38
00:01:58,439 --> 00:02:06,200
about the Dual it's pretty simple

39
00:02:01,820 --> 00:02:06,200
explanation about the C program language

