1
00:00:00,000 --> 00:00:06,359
another possible scenario that you can

2
00:00:02,879 --> 00:00:09,000
use it in HyperDbg is monitoring move

3
00:00:06,359 --> 00:00:11,760
to debug registers debug registers are

4
00:00:09,000 --> 00:00:14,820
used by the debuggers but in case if you

5
00:00:11,760 --> 00:00:17,279
want to just notify any changes in

6
00:00:14,820 --> 00:00:20,180
hardware debug registers you can use the

7
00:00:17,279 --> 00:00:24,060
Dr events in hyperteology

8
00:00:20,180 --> 00:00:28,019
the for example I don't know whether

9
00:00:24,060 --> 00:00:30,420
it's the PatchGuard used to like modify

10
00:00:28,019 --> 00:00:32,640
some of the debug register or access to

11
00:00:30,420 --> 00:00:35,399
some of their debuggages there I don't

12
00:00:32,640 --> 00:00:38,219
know it still checks the debug register

13
00:00:35,399 --> 00:00:40,620
or not but in some cases like this we

14
00:00:38,219 --> 00:00:43,980
could use the VR command or for example

15
00:00:40,620 --> 00:00:47,940
if we want to bypass some of for example

16
00:00:43,980 --> 00:00:50,899
some debugging methods or anti using or

17
00:00:47,940 --> 00:00:53,640
anything virtualization methods just

18
00:00:50,899 --> 00:00:57,840
more more or less antibagging it was

19
00:00:53,640 --> 00:01:01,079
tries to just don't let you to set some

20
00:00:57,840 --> 00:01:04,619
of the debuggages there you could easily

21
00:01:01,079 --> 00:01:06,780
bypass it here here by ignoring and the

22
00:01:04,619 --> 00:01:08,880
changes to the debuggers is there or

23
00:01:06,780 --> 00:01:11,520
performing some changes to the devices

24
00:01:08,880 --> 00:01:14,000
their Dr command is used for this

25
00:01:11,520 --> 00:01:14,000
purpose

