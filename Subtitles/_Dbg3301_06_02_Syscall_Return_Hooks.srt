1
00:00:00,020 --> 00:00:06,060
no let's see we have some examples about

2
00:00:03,419 --> 00:00:08,160
it don't worry if you just don't know

3
00:00:06,060 --> 00:00:10,980
anything about these commands yet we

4
00:00:08,160 --> 00:00:14,340
have some examples about it but for now

5
00:00:10,980 --> 00:00:17,340
let's see how we can intercept and

6
00:00:14,340 --> 00:00:21,180
hook the secret instructions the sister

7
00:00:17,340 --> 00:00:23,460
instruction is used to return from the

8
00:00:21,180 --> 00:00:27,420
kernel mode to the user mode it's the

9
00:00:23,460 --> 00:00:29,480
it's just another side I mean whenever

10
00:00:27,420 --> 00:00:32,579
you use a syscall instruction

11
00:00:29,480 --> 00:00:34,980
you will go from the user mode to the

12
00:00:32,579 --> 00:00:39,660
kernel mode and when you want to return

13
00:00:34,980 --> 00:00:43,460
from the from a system call then the

14
00:00:39,660 --> 00:00:49,200
Windows runs a sister instruction

15
00:00:43,460 --> 00:00:53,100
if we we are able to hook the syscalls

16
00:00:49,200 --> 00:00:57,059
then we we will know what what was the

17
00:00:53,100 --> 00:01:00,660
result of the syscall and whether it's

18
00:00:57,059 --> 00:01:04,379
it was successful or not or what the

19
00:01:00,660 --> 00:01:05,939
state of the system when tries to run

20
00:01:04,379 --> 00:01:09,360
that system code

21
00:01:05,939 --> 00:01:12,960
 the hooking system calls are done by

22
00:01:09,360 --> 00:01:16,439
C thread command again this is an event

23
00:01:12,960 --> 00:01:21,240
and it's implemented the same way as

24
00:01:16,439 --> 00:01:23,540
syscall command by disabling the syscall

25
00:01:21,240 --> 00:01:29,700
enabled bits in

26
00:01:23,540 --> 00:01:32,220
eser MSR this command is not

27
00:01:29,700 --> 00:01:35,360
compatible with PatchGuard just like

28
00:01:32,220 --> 00:01:39,060
syscall command and 

29
00:01:35,360 --> 00:01:42,000
whenever you want to use just syscall

30
00:01:39,060 --> 00:01:44,100
or just sister one of them both of them

31
00:01:42,000 --> 00:01:46,860
are start triggering events because

32
00:01:44,100 --> 00:01:49,740
there's no way to just make one of them

33
00:01:46,860 --> 00:01:52,619
to trigger the events so both of them

34
00:01:49,740 --> 00:01:56,340
start to trigger triggering events

35
00:01:52,619 --> 00:01:59,220
most of the times sister hooking is

36
00:01:56,340 --> 00:02:02,159
useful whenever it's come combined with

37
00:01:59,220 --> 00:02:06,060
the syscall cooking we have some examples

38
00:02:02,159 --> 00:02:09,179
here and here's a an example where we

39
00:02:06,060 --> 00:02:13,620
combine these two two commands in a

40
00:02:09,179 --> 00:02:15,780
meaningful very in this example 

41
00:02:13,620 --> 00:02:17,400
we want to intercept the results of the

42
00:02:15,780 --> 00:02:19,680
system call with the system call

43
00:02:17,400 --> 00:02:24,900
number zero is five five

44
00:02:19,680 --> 00:02:28,140
 and we have a special thread we

45
00:02:24,900 --> 00:02:32,099
want to just intercept the this special

46
00:02:28,140 --> 00:02:35,340
sister for this special thread which

47
00:02:32,099 --> 00:02:39,900
is zero X one C four zero

48
00:02:35,340 --> 00:02:44,340
first of all we create a variable we

49
00:02:39,900 --> 00:02:47,120
create a global variable here we use

50
00:02:44,340 --> 00:02:49,920
dots at the start of

51
00:02:47,120 --> 00:02:53,700
this variable so it means that it's a

52
00:02:49,920 --> 00:02:58,019
global variable and we initialize it

53
00:02:53,700 --> 00:03:01,140
with zero this is an indicator that we

54
00:02:58,019 --> 00:03:04,440
see this system call

55
00:03:01,140 --> 00:03:08,099
for example let's just explain it and

56
00:03:04,440 --> 00:03:11,780
after that I will explain this first

57
00:03:08,099 --> 00:03:15,000
of all there are some reasons why I just

58
00:03:11,780 --> 00:03:17,300
wrote a C thread first and the second

59
00:03:15,000 --> 00:03:21,420
thing is syscall

60
00:03:17,300 --> 00:03:24,659
for the second one we will check whether

61
00:03:21,420 --> 00:03:27,420
the trade ID or the sudo-register For

62
00:03:24,659 --> 00:03:30,959
Thread ID is equal to this specific

63
00:03:27,420 --> 00:03:33,599
value which is or target thread ID

64
00:03:30,959 --> 00:03:35,700
and then we check whether the system a

65
00:03:33,599 --> 00:03:40,739
system call number which is located on

66
00:03:35,700 --> 00:03:44,640
RX register equal to 0x5 Pi or not

67
00:03:40,739 --> 00:03:49,200
if we see this special system call in

68
00:03:44,640 --> 00:03:51,599
this thread then we set the thread

69
00:03:49,200 --> 00:03:57,120
intercept thread to one which is an

70
00:03:51,599 --> 00:03:59,819
indicator that the that the

71
00:03:57,120 --> 00:04:04,080
application executes this system call

72
00:03:59,819 --> 00:04:07,319
and now the next thing that we expect

73
00:04:04,080 --> 00:04:12,780
to see from this thread is a special

74
00:04:07,319 --> 00:04:15,060
thread is a C threat so in this example

75
00:04:12,780 --> 00:04:17,940
we just print the system call number

76
00:04:15,060 --> 00:04:22,460
aligned with its parameters and after

77
00:04:17,940 --> 00:04:25,680
that we come to the sister instruction

78
00:04:22,460 --> 00:04:29,400
or Institute event first we check

79
00:04:25,680 --> 00:04:31,860
whether we are in the same thread or not

80
00:04:29,400 --> 00:04:35,460
if we are in the same thread we check

81
00:04:31,860 --> 00:04:38,160
whether the thread already executed the

82
00:04:35,460 --> 00:04:42,419
syscall instruction or not and if it

83
00:04:38,160 --> 00:04:45,000
executes then we just try to onset this

84
00:04:42,419 --> 00:04:46,520
global variable to make it available for

85
00:04:45,000 --> 00:04:51,300
the next

86
00:04:46,520 --> 00:04:53,220
runs and and the change the result

87
00:04:51,300 --> 00:04:56,660
of the system because you can you

88
00:04:53,220 --> 00:04:59,400
definitely know that we can here we can

89
00:04:56,660 --> 00:05:03,120
manipulate the result of the system

90
00:04:59,400 --> 00:05:07,460
called by simply just changing the Rex

91
00:05:03,120 --> 00:05:07,460
registers or this kind of

