1
00:00:00,060 --> 00:00:06,120
okay before just start using hybrid

2
00:00:03,600 --> 00:00:10,200
division debugger mode first they have

3
00:00:06,120 --> 00:00:13,019
to have some very basic knowledge

4
00:00:10,200 --> 00:00:16,740
about the Windows internals

5
00:00:13,019 --> 00:00:19,080
and you you probably know about it

6
00:00:16,740 --> 00:00:23,939
but for those who don't know I just want

7
00:00:19,080 --> 00:00:27,300
to re-explain it like in Windows

8
00:00:23,939 --> 00:00:29,640
most of these details are derived from

9
00:00:27,300 --> 00:00:31,439
the Windows internals books you can read

10
00:00:29,640 --> 00:00:35,340
it if you are interested

11
00:00:31,439 --> 00:00:37,500
actually in Windows every process is

12
00:00:35,340 --> 00:00:40,079
represented by a structure which is

13
00:00:37,500 --> 00:00:44,760
called EPROCESS which is a part of

14
00:00:40,079 --> 00:00:47,520
antimage rule or anti-os car module and

15
00:00:44,760 --> 00:00:50,160
also it contains everything that's

16
00:00:47,520 --> 00:00:52,640
related to each process like every

17
00:00:50,160 --> 00:00:55,079
details that the process needs to

18
00:00:52,640 --> 00:00:58,079
execute to be executed in the Windows

19
00:00:55,079 --> 00:01:00,059
like process ID like the process name or

20
00:00:58,079 --> 00:01:03,019
whatever information that you can

21
00:01:00,059 --> 00:01:06,119
imagine and the same is true about

22
00:01:03,019 --> 00:01:09,119
threads the

23
00:01:06,119 --> 00:01:13,560
each process can have several threads

24
00:01:09,119 --> 00:01:16,140
and the structure for for each thread

25
00:01:13,560 --> 00:01:20,340
like each trade ID and other details are

26
00:01:16,140 --> 00:01:21,840
stored in underline ethernet 

27
00:01:20,340 --> 00:01:26,820
structure

28
00:01:21,840 --> 00:01:29,180
actually the EPROCESS structure is

29
00:01:26,820 --> 00:01:32,340
is a

30
00:01:29,180 --> 00:01:35,479
stored in the Windows kernel address

31
00:01:32,340 --> 00:01:35,479
this and it's not available

32
00:01:57,119 --> 00:02:02,520
it's also another structure which is

33
00:02:00,119 --> 00:02:05,759
called PEB which just stands for process

34
00:02:02,520 --> 00:02:08,340
environment block and it's also in the

35
00:02:05,759 --> 00:02:10,920
process addresses based on contains some

36
00:02:08,340 --> 00:02:12,959
information which is available for the

37
00:02:10,920 --> 00:02:16,980
username code

38
00:02:12,959 --> 00:02:19,980
there's a command in HyperDbg which

39
00:02:16,980 --> 00:02:23,480
is called the dot process which shows a

40
00:02:19,980 --> 00:02:26,040
list of processes and their anti

41
00:02:23,480 --> 00:02:29,819
e-processor structures and we can also

42
00:02:26,040 --> 00:02:32,580
switch to a view or the memory layout of

43
00:02:29,819 --> 00:02:35,720
a specific process by using this

44
00:02:32,580 --> 00:02:38,879
command this is like also explains about

45
00:02:35,720 --> 00:02:43,200
ethread and 

46
00:02:38,879 --> 00:02:46,280
the same is also true about it's also

47
00:02:43,200 --> 00:02:52,040
available on the kernel addresses

48
00:02:46,280 --> 00:02:54,959
 we can there's another 

49
00:02:52,040 --> 00:02:56,580
information which is available in the

50
00:02:54,959 --> 00:02:59,580
user mode which is called trade

51
00:02:56,580 --> 00:03:01,260
environment black or TBB and it exists

52
00:02:59,580 --> 00:03:04,379
in the process addresses space that

53
00:03:01,260 --> 00:03:09,720
explains about the threat in the sub

54
00:03:04,379 --> 00:03:13,260
system or the csrs as also maintains

55
00:03:09,720 --> 00:03:18,680
a power structure for each thread 

56
00:03:13,260 --> 00:03:21,420
 it has a trace of the each

57
00:03:18,680 --> 00:03:24,480
structure that is created for the thread

58
00:03:21,420 --> 00:03:29,519
and it can handle it there is also

59
00:03:24,480 --> 00:03:33,599
another subsystem for the Windows for

60
00:03:29,519 --> 00:03:38,099
Threads or it's called Uh user or GDI

61
00:03:33,599 --> 00:03:43,260
functions that are handled in win 32k

62
00:03:38,099 --> 00:03:45,840
and the structure is w32 thread in hyper

63
00:03:43,260 --> 00:03:49,640
dvg we have a command which is called

64
00:03:45,840 --> 00:03:52,260
dot thread that shows the current 

65
00:03:49,640 --> 00:03:55,379
executing thread or the thread that we

66
00:03:52,260 --> 00:03:57,180
are currently executing and also able to

67
00:03:55,379 --> 00:04:00,060
create a list of all the available

68
00:03:57,180 --> 00:04:03,659
threads in a specific process

69
00:04:00,060 --> 00:04:08,519
and I think that is whenever you are you

70
00:04:03,659 --> 00:04:12,299
use Windows a portion of the dlls or

71
00:04:08,519 --> 00:04:15,480
modules binary modules are loaded in the

72
00:04:12,299 --> 00:04:18,660
kernel mode and some of them are loaded

73
00:04:15,480 --> 00:04:22,139
in the user mode per each process we

74
00:04:18,660 --> 00:04:25,800
will see each of them and I will see how

75
00:04:22,139 --> 00:04:28,139
how hyper-d can show both the camera

76
00:04:25,800 --> 00:04:31,380
mode and user mode modules along with

77
00:04:28,139 --> 00:04:35,300
their addresses their stats a start size

78
00:04:31,380 --> 00:04:35,300
and has those functions

