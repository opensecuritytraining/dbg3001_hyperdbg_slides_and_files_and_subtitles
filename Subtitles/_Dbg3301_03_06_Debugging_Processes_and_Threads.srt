1
00:00:00,000 --> 00:00:07,020
now let's see how we can debug processes

2
00:00:04,620 --> 00:00:10,019
and threads which is a really important

3
00:00:07,020 --> 00:00:12,719
Concept in in kernel mode and Boston

4
00:00:10,019 --> 00:00:15,540
Carnival and user mode by using the dot

5
00:00:12,719 --> 00:00:18,420
process command you know the current

6
00:00:15,540 --> 00:00:21,840
process that currently is executing into

7
00:00:18,420 --> 00:00:25,800
into your system or it shows the current

8
00:00:21,840 --> 00:00:29,099
process that it's CR3 registers

9
00:00:25,800 --> 00:00:32,880
currently mapped and the process layout

10
00:00:29,099 --> 00:00:35,340
this process layout is valid into our

11
00:00:32,880 --> 00:00:38,520
current debugging context there's also

12
00:00:35,340 --> 00:00:41,280
another command called dot thread this

13
00:00:38,520 --> 00:00:43,920
command is used to show the current

14
00:00:41,280 --> 00:00:46,680
threat information the current trade

15
00:00:43,920 --> 00:00:50,340
that is running to the system it also

16
00:00:46,680 --> 00:00:53,700
shows the e-processor structure for the

17
00:00:50,340 --> 00:00:57,239
current process and the e-thread

18
00:00:53,700 --> 00:01:00,840
structure for the country along with it

19
00:00:57,239 --> 00:01:05,880
along with 16 bytes process if you

20
00:01:00,840 --> 00:01:09,000
want to see the list of processes you

21
00:01:05,880 --> 00:01:12,119
can use that process list using this

22
00:01:09,000 --> 00:01:14,700
command needs your symbols files to be

23
00:01:12,119 --> 00:01:17,220
configured because HyperDbg otherwise

24
00:01:14,700 --> 00:01:21,119
don't know how to search for their

25
00:01:17,220 --> 00:01:24,420
process names and process details so

26
00:01:21,119 --> 00:01:26,640
before using the using this command you

27
00:01:24,420 --> 00:01:27,540
should make sure that your symbol server

28
00:01:26,640 --> 00:01:33,020
is set

29
00:01:27,540 --> 00:01:36,299
and it shows both the kernel CR3

30
00:01:33,020 --> 00:01:41,159
register aligned with image name process

31
00:01:36,299 --> 00:01:44,820
ID and the structure of the EPROCESS as

32
00:01:41,159 --> 00:01:47,820
you can see here if you want to just see

33
00:01:44,820 --> 00:01:51,299
the threads for a special process then

34
00:01:47,820 --> 00:01:54,960
we can use dot spread list and after

35
00:01:51,299 --> 00:01:59,220
that we could specify its EPROCESS

36
00:01:54,960 --> 00:02:04,020
address for example in this example I I

37
00:01:59,220 --> 00:02:05,719
just want to see the thread list threads

38
00:02:04,020 --> 00:02:10,340
list for

39
00:02:05,719 --> 00:02:15,180
process like Microsoft Edge process

40
00:02:10,340 --> 00:02:19,920
which its EPROCESS is located here so I

41
00:02:15,180 --> 00:02:22,560
use this after the process parameter to

42
00:02:19,920 --> 00:02:24,080
the dot thread command we will see some

43
00:02:22,560 --> 00:02:28,560
examples about

44
00:02:24,080 --> 00:02:32,760
these commands later in this part and

45
00:02:28,560 --> 00:02:37,080
the result will be at least threads with

46
00:02:32,760 --> 00:02:42,360
their e-trad structure along with the

47
00:02:37,080 --> 00:02:45,360
process ID dot thread ID so so e08 is

48
00:02:42,360 --> 00:02:50,700
the process ID for this Microsoft Edge

49
00:02:45,360 --> 00:02:53,640
process and this 1918 is also the thread

50
00:02:50,700 --> 00:02:57,540
ID for this specific track

51
00:02:53,640 --> 00:03:00,900
 now with those other ways that hyper

52
00:02:57,540 --> 00:03:04,739
activity can switch to another process

53
00:03:00,900 --> 00:03:07,379
or another Trade A Memory layout and it

54
00:03:04,739 --> 00:03:09,420
will it will change the context of the

55
00:03:07,379 --> 00:03:12,840
debugger for example if you want to

56
00:03:09,420 --> 00:03:15,060
switch to a new process and we want to

57
00:03:12,840 --> 00:03:16,340
see its registers it's a Memory

58
00:03:15,060 --> 00:03:21,980
addresses

59
00:03:16,340 --> 00:03:25,560
you should provide its process ID or

60
00:03:21,980 --> 00:03:29,519
an EPROCESS structure both of them are

61
00:03:25,560 --> 00:03:32,900
possible so in this case I use dot

62
00:03:29,519 --> 00:03:36,900
process plus PID and specified the

63
00:03:32,900 --> 00:03:40,560
process ID in hexadecimal format and

64
00:03:36,900 --> 00:03:43,620
pass it to the okay HyperDbg after that

65
00:03:40,560 --> 00:03:47,159
we should continue the debugger so next

66
00:03:43,620 --> 00:03:49,500
time the debugger sees this process then

67
00:03:47,159 --> 00:03:53,400
it will break and give the control to

68
00:03:49,500 --> 00:03:56,760
the debugger the debug the debuggee

69
00:03:53,400 --> 00:03:59,159
will be harder they gain

70
00:03:56,760 --> 00:04:02,519
and also if you want to just switch to a

71
00:03:59,159 --> 00:04:06,840
specific to a specific thread then you

72
00:04:02,519 --> 00:04:10,140
should specify its e-trad address and

73
00:04:06,840 --> 00:04:12,860
use that thread command and provide the

74
00:04:10,140 --> 00:04:18,000
address to the e-thread of the target

75
00:04:12,860 --> 00:04:22,019
there are also other commands one 

76
00:04:18,000 --> 00:04:25,560
for grabbing the access to the processes

77
00:04:22,019 --> 00:04:28,860
the like you could use the both touch

78
00:04:25,560 --> 00:04:32,040
process command or Dutch process two

79
00:04:28,860 --> 00:04:36,180
commands we will use some examples for

80
00:04:32,040 --> 00:04:39,720
this commands the first command uses the

81
00:04:36,180 --> 00:04:42,900
clock interrupt to intercept the process

82
00:04:39,720 --> 00:04:46,800
and the second command uses the changes

83
00:04:42,900 --> 00:04:49,620
of CR 3 register to intercept the

84
00:04:46,800 --> 00:04:53,120
process changes so generally speaking

85
00:04:49,620 --> 00:04:57,840
whenever the time sliced for a special

86
00:04:53,120 --> 00:05:00,419
process is finished then a clock

87
00:04:57,840 --> 00:05:02,580
interrupt will be the the operating

88
00:05:00,419 --> 00:05:05,940
system configures the processor to throw

89
00:05:02,580 --> 00:05:08,880
some clock interrupts and get the

90
00:05:05,940 --> 00:05:10,919
execution back and it will give the

91
00:05:08,880 --> 00:05:13,500
execution back to operating systems

92
00:05:10,919 --> 00:05:17,040
operating system can perform context

93
00:05:13,500 --> 00:05:20,220
switches can perform other tasks but 

94
00:05:17,040 --> 00:05:22,860
this is where HyperDbg know to get

95
00:05:20,220 --> 00:05:25,860
notified about the process change and

96
00:05:22,860 --> 00:05:28,740
will intercept and halt the system and

97
00:05:25,860 --> 00:05:31,320
give the control of the debuggee to the

98
00:05:28,740 --> 00:05:35,820
debugger so this is how that process

99
00:05:31,320 --> 00:05:38,419
command work but another scenario if if

100
00:05:35,820 --> 00:05:41,520
you want to use that process to then

101
00:05:38,419 --> 00:05:44,520
whenever the Windows wants to change the

102
00:05:41,520 --> 00:05:48,720
memory layout to a new memory layout or

103
00:05:44,520 --> 00:05:52,020
to a new process memory layout then you

104
00:05:48,720 --> 00:05:54,419
change the CR series is there and Hyper

105
00:05:52,020 --> 00:05:59,100
DVD tries to intercept all of the

106
00:05:54,419 --> 00:06:02,100
changes to this CR3 register and if 

107
00:05:59,100 --> 00:06:04,680
this changed a hybrid intercept these

108
00:06:02,100 --> 00:06:07,139
changes then it it will interpret it as

109
00:06:04,680 --> 00:06:12,180
the Windows is trying to change the

110
00:06:07,139 --> 00:06:15,240
process so the the basic principles of

111
00:06:12,180 --> 00:06:17,039
implementation are not important if you

112
00:06:15,240 --> 00:06:20,419
just want to know when to use that

113
00:06:17,039 --> 00:06:24,120
process or when to use that process to

114
00:06:20,419 --> 00:06:27,960
in most of the cases that process is a

115
00:06:24,120 --> 00:06:32,880
better option because we could use

116
00:06:27,960 --> 00:06:36,000
that process to sometimes intercept

117
00:06:32,880 --> 00:06:38,479
the execution in the user mode while the

118
00:06:36,000 --> 00:06:41,699
second one is not possible

119
00:06:38,479 --> 00:06:47,340
changing CR3 is always done in the

120
00:06:41,699 --> 00:06:51,060
kernel so we cannot intercept the 

121
00:06:47,340 --> 00:06:53,400
execution in user mode with a Dutch

122
00:06:51,060 --> 00:06:56,400
process tool but it's possible by using

123
00:06:53,400 --> 00:06:58,620
Dash process command so if you want to

124
00:06:56,400 --> 00:07:00,840
just interest accepted in user mode you

125
00:06:58,620 --> 00:07:04,680
should use that process and if you want

126
00:07:00,840 --> 00:07:07,199
to see and modify the context by context

127
00:07:04,680 --> 00:07:09,960
I mean the registers the memory layout

128
00:07:07,199 --> 00:07:12,660
the state of the system of the thread

129
00:07:09,960 --> 00:07:15,660
the in the user mode or directly in the

130
00:07:12,660 --> 00:07:18,539
kernel mode again you should use that

131
00:07:15,660 --> 00:07:21,180
process and this is also possible to

132
00:07:18,539 --> 00:07:22,919
step through the instruction because we

133
00:07:21,180 --> 00:07:26,940
will be notified in the middle of

134
00:07:22,919 --> 00:07:30,180
execution of the process so we have a

135
00:07:26,940 --> 00:07:33,060
chance to step through the instructions

136
00:07:30,180 --> 00:07:37,080
directly in the context of the target

137
00:07:33,060 --> 00:07:39,960
process for that process to if for

138
00:07:37,080 --> 00:07:42,300
example the process is waiting for a

139
00:07:39,960 --> 00:07:45,240
context switch the process is waiting

140
00:07:42,300 --> 00:07:48,240
for an object so the process is simply

141
00:07:45,240 --> 00:07:51,120
the operating system don't assign any

142
00:07:48,240 --> 00:07:54,539
timing slice to the to that specific

143
00:07:51,120 --> 00:07:58,139
process and then the first command

144
00:07:54,539 --> 00:08:00,720
probably won't work but most of the time

145
00:07:58,139 --> 00:08:03,720
the second command works so in most of

146
00:08:00,720 --> 00:08:06,000
the cases if you you couldn't catch the

147
00:08:03,720 --> 00:08:08,160
execution by using that process command

148
00:08:06,000 --> 00:08:11,580
then another option is using Dash

149
00:08:08,160 --> 00:08:15,000
process 2 command and another scenario

150
00:08:11,580 --> 00:08:18,419
is where you want to get notified before

151
00:08:15,000 --> 00:08:22,639
the process finds a chance to get

152
00:08:18,419 --> 00:08:26,879
executed for example the first command

153
00:08:22,639 --> 00:08:30,240
or the dot process command gets the

154
00:08:26,879 --> 00:08:33,000
execution after one time slice so the

155
00:08:30,240 --> 00:08:36,300
process will find a chance to execute

156
00:08:33,000 --> 00:08:39,120
one time for one time slice and after

157
00:08:36,300 --> 00:08:41,219
that yeah it might the the process

158
00:08:39,120 --> 00:08:45,720
might be intercepted by clock and drop

159
00:08:41,219 --> 00:08:49,980
but that process 2 guarantees that when

160
00:08:45,720 --> 00:08:51,019
you like execute this that process 2

161
00:08:49,980 --> 00:08:55,380
command

162
00:08:51,019 --> 00:08:57,720
then before the program get gets the

163
00:08:55,380 --> 00:09:00,720
chance to get executed then you will

164
00:08:57,720 --> 00:09:03,060
intercept it so this is a little bit

165
00:09:00,720 --> 00:09:05,519
tricky sometimes you need to use that

166
00:09:03,060 --> 00:09:09,959
processor sometimes you need to use that

167
00:09:05,519 --> 00:09:12,959
process to also the same thing happens

168
00:09:09,959 --> 00:09:15,360
to that thread there is also there is

169
00:09:12,959 --> 00:09:19,140
one that thread command and one

170
00:09:15,360 --> 00:09:23,279
doctor two command doctorate 

171
00:09:19,140 --> 00:09:25,339
command works the same as touch

172
00:09:23,279 --> 00:09:27,740
process command it uses the clock

173
00:09:25,339 --> 00:09:32,160
interrupt in the same way

174
00:09:27,740 --> 00:09:35,160
but that thread to command uses

175
00:09:32,160 --> 00:09:35,160
actually

176
00:09:35,600 --> 00:09:45,660
intercept accesses to this GS

177
00:09:40,820 --> 00:09:48,120
188 address like if if Windows wants

178
00:09:45,660 --> 00:09:51,540
to change some threads then the Windows

179
00:09:48,120 --> 00:09:54,360
will update this location so HyperDbg

180
00:09:51,540 --> 00:09:57,180
tries to intercepts any rights and this

181
00:09:54,360 --> 00:09:58,860
address and if there's any right on this

182
00:09:57,180 --> 00:10:01,279
address then the high producing

183
00:09:58,860 --> 00:10:03,779
interpret it as a

184
00:10:01,279 --> 00:10:06,839
threat change and it will check whether

185
00:10:03,779 --> 00:10:10,680
or a specific thread is present or not

186
00:10:06,839 --> 00:10:13,320
so there are some scenarios where you

187
00:10:10,680 --> 00:10:17,040
can use that thread for example if

188
00:10:13,320 --> 00:10:18,959
you want to debug only one thread and

189
00:10:17,040 --> 00:10:21,120
you have multiple choice and you only

190
00:10:18,959 --> 00:10:22,860
want to debug one of them then you

191
00:10:21,120 --> 00:10:25,920
should use dot thread instead of that

192
00:10:22,860 --> 00:10:27,959
thread 2 and if you're going to halt the

193
00:10:25,920 --> 00:10:30,360
debug even the thread is for example in

194
00:10:27,959 --> 00:10:32,459
user mode in kernel mode in the middle

195
00:10:30,360 --> 00:10:35,820
of its execution then you should use

196
00:10:32,459 --> 00:10:38,519
that thread this is not possible by

197
00:10:35,820 --> 00:10:42,360
using doctorate to of course the actual

198
00:10:38,519 --> 00:10:45,300
context is visible by context again I

199
00:10:42,360 --> 00:10:48,480
mean registers and memory the actual

200
00:10:45,300 --> 00:10:49,880
context is are visible by using that

201
00:10:48,480 --> 00:10:53,760
trade command

202
00:10:49,880 --> 00:10:56,640
while that thread to command the

203
00:10:53,760 --> 00:10:59,820
actual context is not visible you have

204
00:10:56,640 --> 00:11:02,339
to accept some instructions to again

205
00:10:59,820 --> 00:11:04,860
perform the context switching and reach

206
00:11:02,339 --> 00:11:08,220
again to the actual target

207
00:11:04,860 --> 00:11:10,140
and if you want to step through the

208
00:11:08,220 --> 00:11:12,300
instructions directly again you should

209
00:11:10,140 --> 00:11:14,579
use that thread command and if the

210
00:11:12,300 --> 00:11:17,279
thread is halted and it did like happens

211
00:11:14,579 --> 00:11:19,019
 in in when you want to just debug

212
00:11:17,279 --> 00:11:22,019
some dead likes in your target

213
00:11:19,019 --> 00:11:24,180
application and again you should use

214
00:11:22,019 --> 00:11:28,260
that trade command that thread 2 is not

215
00:11:24,180 --> 00:11:31,140
usable in general in only and only if

216
00:11:28,260 --> 00:11:33,120
you couldn't get a chance to get the

217
00:11:31,140 --> 00:11:37,079
third execution by using the first

218
00:11:33,120 --> 00:11:40,019
method and you can try the second one so

219
00:11:37,079 --> 00:11:45,200
it's highly recommended to use the dot

220
00:11:40,019 --> 00:11:45,200
shred command instead of extra tube

