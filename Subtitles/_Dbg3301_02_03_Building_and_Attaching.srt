1
00:00:00,179 --> 00:00:07,740
okay let's start by start with 

2
00:00:04,980 --> 00:00:10,620
building hybrid engineer from its source

3
00:00:07,740 --> 00:00:14,820
code for building HyperDbg we need to

4
00:00:10,620 --> 00:00:18,000
install a VisualStudio each version

5
00:00:14,820 --> 00:00:20,240
of the visuals that use that supports WD

6
00:00:18,000 --> 00:00:23,460
card WDK

7
00:00:20,240 --> 00:00:25,980
like visuals the community which is the

8
00:00:23,460 --> 00:00:29,760
preferred version or if you have an

9
00:00:25,980 --> 00:00:32,180
Enterprise license of Visual Studio

10
00:00:29,760 --> 00:00:37,140
or professional

11
00:00:32,180 --> 00:00:41,520
it's best it works it best works with

12
00:00:37,140 --> 00:00:43,460
newer version of visuals that you as

13
00:00:41,520 --> 00:00:48,120
you can see that you can

14
00:00:43,460 --> 00:00:51,600
download HyperDbg download the visual

15
00:00:48,120 --> 00:00:54,360
studio from the Microsoft website visual

16
00:00:51,600 --> 00:00:57,300
studio.com another thing that is needed

17
00:00:54,360 --> 00:01:01,500
for building hyper Community is

18
00:00:57,300 --> 00:01:04,199
Windows SDK is preferred to download

19
00:01:01,500 --> 00:01:06,299
latest

20
00:01:04,199 --> 00:01:09,240
I mean does SDK

21
00:01:06,299 --> 00:01:13,340
well I know and also download then

22
00:01:09,240 --> 00:01:17,060
Windows driver kit or WDK

23
00:01:13,340 --> 00:01:18,860
for running hybrid which is currently

24
00:01:17,060 --> 00:01:23,040
tested

25
00:01:18,860 --> 00:01:28,340
with the newest available VisualStudio

26
00:01:23,040 --> 00:01:34,080
version we will Studio 2022

27
00:01:28,340 --> 00:01:37,259
and with Windows 11 SDK it works best

28
00:01:34,080 --> 00:01:41,340
for now but in the future versions also

29
00:01:37,259 --> 00:01:45,320
the newer SDK will will be supported and

30
00:01:41,340 --> 00:01:50,340
the newer WD case and you just need to

31
00:01:45,320 --> 00:01:54,180
recursively use the git clone command

32
00:01:50,340 --> 00:01:55,439
and clone everything make sure to just

33
00:01:54,180 --> 00:01:58,020
not

34
00:01:55,439 --> 00:02:01,460
and just download the source code

35
00:01:58,020 --> 00:02:05,280
because it have it has some other some

36
00:02:01,460 --> 00:02:09,479
external repositories which you

37
00:02:05,280 --> 00:02:11,879
should use dash dash recursive command

38
00:02:09,479 --> 00:02:14,459
for the git

39
00:02:11,879 --> 00:02:17,340
and after that you can start building

40
00:02:14,459 --> 00:02:21,300
the hybrid Imaging

41
00:02:17,340 --> 00:02:26,120
so I will go and as you can see

42
00:02:21,300 --> 00:02:26,120
let me close everything but this

43
00:02:30,900 --> 00:02:36,920
okay

44
00:02:33,739 --> 00:02:40,860
 no 

45
00:02:36,920 --> 00:02:43,260
after building it we will try to attach

46
00:02:40,860 --> 00:02:46,760
to the hyper WG

47
00:02:43,260 --> 00:02:50,879
and see different operations

48
00:02:46,760 --> 00:02:52,800
as you see the previous slides we

49
00:02:50,879 --> 00:02:56,519
want to create a local dividing

50
00:02:52,800 --> 00:02:59,940
environment or we want to also we can

51
00:02:56,519 --> 00:03:03,060
also make a TCP connection VMI mode or

52
00:02:59,940 --> 00:03:04,220
we can make a Serial debugging but for

53
00:03:03,060 --> 00:03:08,040
now

54
00:03:04,220 --> 00:03:10,019
it's also possible to use it in my

55
00:03:08,040 --> 00:03:14,420
current machine

56
00:03:10,019 --> 00:03:14,420
I will go to the hybrid which is

57
00:03:17,099 --> 00:03:19,940
why

58
00:03:20,220 --> 00:03:26,760
you can also download the

59
00:03:22,819 --> 00:03:30,200
hybrid widget itself the binary files of

60
00:03:26,760 --> 00:03:33,959
the hybrid which is built by GitHub

61
00:03:30,200 --> 00:03:37,019
actions in the repository of the hyper

62
00:03:33,959 --> 00:03:39,720
division but it's preferred to build it

63
00:03:37,019 --> 00:03:41,940
by your own so if you can if you want to

64
00:03:39,720 --> 00:03:44,280
somehow change HyperDbg your

65
00:03:41,940 --> 00:03:46,379
functionalities in the source code you

66
00:03:44,280 --> 00:03:48,780
can also change it directly in the

67
00:03:46,379 --> 00:03:49,500
source code and compile your customized

68
00:03:48,780 --> 00:03:54,120
version

69
00:03:49,500 --> 00:03:59,159
I will run it as administrator and I'm

70
00:03:54,120 --> 00:04:03,739
using this version of HyperDbg

71
00:03:59,159 --> 00:04:08,000
divider for connecting if you

72
00:04:03,739 --> 00:04:10,500
there might be many error many

73
00:04:08,000 --> 00:04:13,019
considerations before running the hybrid

74
00:04:10,500 --> 00:04:16,519
dbg it's like most of is considered

75
00:04:13,019 --> 00:04:21,299
considerations are explained to

76
00:04:16,519 --> 00:04:24,620
attaching to a local machine here we can

77
00:04:21,299 --> 00:04:27,120
just type that connect local but there

78
00:04:24,620 --> 00:04:29,160
might be errors

79
00:04:27,120 --> 00:04:31,560
and

80
00:04:29,160 --> 00:04:36,540
if you want to know how to solve these

81
00:04:31,560 --> 00:04:39,720
errors please  take a look at these

82
00:04:36,540 --> 00:04:41,759
documentation page for example HyperDbg

83
00:04:39,720 --> 00:04:45,060
the current version is not compatible

84
00:04:41,759 --> 00:04:47,340
with VBS or virtualization based

85
00:04:45,060 --> 00:04:49,020
security so you have to disable it

86
00:04:47,340 --> 00:04:50,880
before running hybrid usually because

87
00:04:49,020 --> 00:04:54,740
the hybrid version itself is a

88
00:04:50,880 --> 00:04:58,800
hypervised there and the

89
00:04:54,740 --> 00:05:01,139
VBS is also a hypervisor so this as the

90
00:04:58,800 --> 00:05:03,360
nested virtualization is not supported

91
00:05:01,139 --> 00:05:05,340
in Intel process source it's supported

92
00:05:03,360 --> 00:05:08,220
by vendors but not the Intel processor

93
00:05:05,340 --> 00:05:12,120
itself so we are not compatible

94
00:05:08,220 --> 00:05:15,180
currently with the VBS so you may you

95
00:05:12,120 --> 00:05:17,340
should disable it another thing is that

96
00:05:15,180 --> 00:05:21,000
you should disable drivers signature

97
00:05:17,340 --> 00:05:24,120
enforcement because if because the

98
00:05:21,000 --> 00:05:25,639
hybrid regions driver is not signed find

99
00:05:24,120 --> 00:05:30,180
Microsoft

100
00:05:25,639 --> 00:05:32,820
it's definitely not Microsoft won't

101
00:05:30,180 --> 00:05:37,139
sign a driver that is able to modify its

102
00:05:32,820 --> 00:05:38,880
kernel so you should disable this driver

103
00:05:37,139 --> 00:05:40,919
extinguisher enforcement or if you have

104
00:05:38,880 --> 00:05:44,060
a Microsoft signature you can't sign it

105
00:05:40,919 --> 00:05:48,300
for yourself so you are not forced to

106
00:05:44,060 --> 00:05:50,759
disable the DSE feature after doing this

107
00:05:48,300 --> 00:05:52,740
I also explained how you can check

108
00:05:50,759 --> 00:05:56,280
whether your computer supports hybrid

109
00:05:52,740 --> 00:05:59,160
with you or not and how you can disable

110
00:05:56,280 --> 00:06:00,660
the driver's signature enforcement 

111
00:05:59,160 --> 00:06:05,039
and

112
00:06:00,660 --> 00:06:08,460
other thing related how I can how you

113
00:06:05,039 --> 00:06:11,520
can temporarily disable the DSE and how

114
00:06:08,460 --> 00:06:14,520
you can disable VBS or hbci

115
00:06:11,520 --> 00:06:17,699
different commands are used for for it

116
00:06:14,520 --> 00:06:21,960
and for example currently in in this

117
00:06:17,699 --> 00:06:23,880
machine I have I have an enabled 

118
00:06:21,960 --> 00:06:26,039
driver signature enforcement so

119
00:06:23,880 --> 00:06:29,539
definitely we're gonna see some errors

120
00:06:26,039 --> 00:06:35,240
when we when we are using HyperDbg but

121
00:06:29,539 --> 00:06:35,240
but just let me show you the errors

122
00:06:38,000 --> 00:06:45,960
as you can see they said it's not able

123
00:06:42,060 --> 00:06:47,639
to install hyperusians because either

124
00:06:45,960 --> 00:06:50,039
the driver's signature enforcement in

125
00:06:47,639 --> 00:06:52,520
air is enabled or hvcr prevents the

126
00:06:50,039 --> 00:06:57,199
driver from loading

127
00:06:52,520 --> 00:07:01,759
I also asks you to follow the

128
00:06:57,199 --> 00:07:01,759
instructions provided here

