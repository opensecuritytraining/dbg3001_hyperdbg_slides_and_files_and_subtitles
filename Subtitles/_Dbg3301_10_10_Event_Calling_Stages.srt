1
00:00:00,000 --> 00:00:07,919
another mechanism in HyperDbg is even

2
00:00:04,080 --> 00:00:09,840
calling stages or event stages

3
00:00:07,919 --> 00:00:12,059


4
00:00:09,840 --> 00:00:15,599
 let's learn more about these

5
00:00:12,059 --> 00:00:18,600
mechanism some of the events in hyper

6
00:00:15,599 --> 00:00:20,600
dvg contain an additional parameter

7
00:00:18,600 --> 00:00:23,100
known as stage

8
00:00:20,600 --> 00:00:26,460
they're in HyperDbg there are

9
00:00:23,100 --> 00:00:30,720
three different levels or stages of

10
00:00:26,460 --> 00:00:36,500
execution which we can use from pre post

11
00:00:30,720 --> 00:00:40,020
and all this is the type of stages

12
00:00:36,500 --> 00:00:44,160
this parameter or this stage parameter

13
00:00:40,020 --> 00:00:48,420
is specified at which stage the event

14
00:00:44,160 --> 00:00:53,219
should be triggered for example MSR read

15
00:00:48,420 --> 00:00:57,360
event in the in the 

16
00:00:53,219 --> 00:00:58,879
previous stage event of the MSR read

17
00:00:57,360 --> 00:01:03,059
event

18
00:00:58,879 --> 00:01:05,060
the event is triggered before emulating

19
00:01:03,059 --> 00:01:09,000
their rdmsr

20
00:01:05,060 --> 00:01:12,240
instructions and after rdmsr instruction

21
00:01:09,000 --> 00:01:14,340
is executed and the result of this

22
00:01:12,240 --> 00:01:18,600
instruction is available in their

23
00:01:14,340 --> 00:01:21,840
registers this is the post stage of

24
00:01:18,600 --> 00:01:24,780
calling or triggering the the event this

25
00:01:21,840 --> 00:01:27,900
is the it's also the same for most of

26
00:01:24,780 --> 00:01:32,420
other events for example cpuid MSR

27
00:01:27,900 --> 00:01:36,360
right or TSC PMC for example for cpuid

28
00:01:32,420 --> 00:01:39,119
 before cpuid instructions

29
00:01:36,360 --> 00:01:42,060
actually executed in the target you have

30
00:01:39,119 --> 00:01:45,060
the the pre-stage of the event will be

31
00:01:42,060 --> 00:01:48,259
called and once we're done with running

32
00:01:45,060 --> 00:01:51,659
a cpuid instruction and adjust the

33
00:01:48,259 --> 00:01:54,240
registers for the CPU resistors are that

34
00:01:51,659 --> 00:01:57,659
are modified by the CPI ID instruction

35
00:01:54,240 --> 00:01:59,340
then after that the poster stage will be

36
00:01:57,659 --> 00:02:05,360
called and we can't we could easily

37
00:01:59,340 --> 00:02:10,679
manipulate the results in the post event

38
00:02:05,360 --> 00:02:14,160
 whenever we specified all type

39
00:02:10,679 --> 00:02:17,099
for event stages it indicates that the

40
00:02:14,160 --> 00:02:20,220
target event should be executed both in

41
00:02:17,099 --> 00:02:23,940
the pre event stage and post event

42
00:02:20,220 --> 00:02:26,640
stage and you can imagine that by

43
00:02:23,940 --> 00:02:29,580
default events all of the events are

44
00:02:26,640 --> 00:02:32,060
considered to be triggered in the

45
00:02:29,580 --> 00:02:35,940
previous state so

46
00:02:32,060 --> 00:02:39,420
we have in the pre stage we have the

47
00:02:35,940 --> 00:02:41,879
ability to ignore the event we we can

48
00:02:39,420 --> 00:02:44,459
use the previously mentioned mechanism

49
00:02:41,879 --> 00:02:48,420
for short circuiting or for event

50
00:02:44,459 --> 00:02:51,260
ignorance and you can imagine it's

51
00:02:48,420 --> 00:02:55,800
clear that if we 

52
00:02:51,260 --> 00:03:00,420
ignore the events or if we short circuit

53
00:02:55,800 --> 00:03:03,300
the event then the post calling stage

54
00:03:00,420 --> 00:03:04,700
won't be called and the post will be

55
00:03:03,300 --> 00:03:08,099
ignored

56
00:03:04,700 --> 00:03:10,440
for some events like EPT hook or EPT

57
00:03:08,099 --> 00:03:13,319
hook shoe this mechanism doesn't make

58
00:03:10,440 --> 00:03:17,180
sense and it's not supported because it

59
00:03:13,319 --> 00:03:20,280
doesn't make sense to just create a

60
00:03:17,180 --> 00:03:24,620
calling stage for a hook which is like

61
00:03:20,280 --> 00:03:24,620
which is simply like a breakpoint

62
00:03:24,739 --> 00:03:31,739
 another useful scenario in which we

63
00:03:28,680 --> 00:03:34,879
can use this event calling stages

64
00:03:31,739 --> 00:03:39,900
mechanism is for the mitral command

65
00:03:34,879 --> 00:03:42,299
 the different stages of of

66
00:03:39,900 --> 00:03:45,720
triggering the monitor command shows the

67
00:03:42,299 --> 00:03:49,560
content of memory before and after the

68
00:03:45,720 --> 00:03:52,860
actual modification and Studio

69
00:03:49,560 --> 00:03:57,180
register dollar stage double sign

70
00:03:52,860 --> 00:04:02,700
stages added to the HyperDbg that shows

71
00:03:57,180 --> 00:04:04,860
the calling a stage of the of the

72
00:04:02,700 --> 00:04:07,860
event for example if this should

73
00:04:04,860 --> 00:04:10,439
register is equal to zero it shows that

74
00:04:07,860 --> 00:04:14,400
the target calling stage is pretty

75
00:04:10,439 --> 00:04:19,079
wildly fresh it shows one it means

76
00:04:14,400 --> 00:04:21,800
that the target event is executed in a

77
00:04:19,079 --> 00:04:26,340
post call stage

78
00:04:21,800 --> 00:04:27,560
 for example in this example I create

79
00:04:26,340 --> 00:04:31,699
a

80
00:04:27,560 --> 00:04:38,100
write interception or a right apt hook

81
00:04:31,699 --> 00:04:42,419
on this address and if the stage

82
00:04:38,100 --> 00:04:46,199
of the memory is equals to one

83
00:04:42,419 --> 00:04:52,320
then it means the

84
00:04:46,199 --> 00:04:56,160
target it means that the target the

85
00:04:52,320 --> 00:04:59,340
target event is called in the post 

86
00:04:56,160 --> 00:05:02,940
stage so we could see the current

87
00:04:59,340 --> 00:05:06,720
memory we could use the DQ 

88
00:05:02,940 --> 00:05:08,759
function here and the context we know

89
00:05:06,720 --> 00:05:12,780
from the previous part that the context

90
00:05:08,759 --> 00:05:17,000
shows the address which is the actual

91
00:05:12,780 --> 00:05:21,500
address which is modified and 

92
00:05:17,000 --> 00:05:25,860
printf their modified memory content

93
00:05:21,500 --> 00:05:27,780
and also if it's not equal to one and

94
00:05:25,860 --> 00:05:30,780
then it means that it's equal probably

95
00:05:27,780 --> 00:05:33,660
equal to zero so we could use we could

96
00:05:30,780 --> 00:05:37,199
see the previous memory and we could 

97
00:05:33,660 --> 00:05:40,380
show the content of memory before it's

98
00:05:37,199 --> 00:05:44,100
modified and after it's modified let's

99
00:05:40,380 --> 00:05:48,780
see some examples here

100
00:05:44,100 --> 00:05:52,259
 I for this for this

101
00:05:48,780 --> 00:05:54,380
example I used the

102
00:05:52,259 --> 00:05:54,380
the

103
00:05:55,560 --> 00:06:01,100
right ignore which I showed you in

104
00:05:59,160 --> 00:06:05,639
the previous section

105
00:06:01,100 --> 00:06:08,180
 as I already explained it it's a

106
00:06:05,639 --> 00:06:10,800
simple 

107
00:06:08,180 --> 00:06:12,900
modification it's a simple

108
00:06:10,800 --> 00:06:15,840
modification it's a simple modification

109
00:06:12,900 --> 00:06:16,860
of the memory or it's just increments

110
00:06:15,840 --> 00:06:22,440
the

111
00:06:16,860 --> 00:06:24,300
 value here and after each two

112
00:06:22,440 --> 00:06:27,020
seconds it increments the value let's

113
00:06:24,300 --> 00:06:27,020
write again

114
00:06:27,120 --> 00:06:35,639
and yes we have the address of value

115
00:06:30,840 --> 00:06:39,660
and the process ID let's return back

116
00:06:35,639 --> 00:06:39,660
to or 

117
00:06:40,080 --> 00:06:50,960
a script I write money manager 

118
00:06:46,020 --> 00:06:54,479
event and it's each we should 

119
00:06:50,960 --> 00:06:58,020
monitor this address and this address

120
00:06:54,479 --> 00:07:00,979
plus four and for this specific

121
00:06:58,020 --> 00:07:00,979
process ID

122
00:07:01,759 --> 00:07:12,360
 but this time I specify a stage

123
00:07:08,759 --> 00:07:15,360
and we could use all stages here we

124
00:07:12,360 --> 00:07:18,360
could also use Prius stages here

125
00:07:15,360 --> 00:07:20,759
pre-stage is the default stage so if we

126
00:07:18,360 --> 00:07:25,560
don't write anything it means there is

127
00:07:20,759 --> 00:07:30,979
pre-stage but we can also explicit to

128
00:07:25,560 --> 00:07:34,020
the right right the pre or if we want to

129
00:07:30,979 --> 00:07:35,300
use it as a poster stage we can use it

130
00:07:34,020 --> 00:07:39,060
here

131
00:07:35,300 --> 00:07:42,060
 so in this state I want to show the

132
00:07:39,060 --> 00:07:48,020
content of memory before and after it's

133
00:07:42,060 --> 00:07:51,240
 modified so I simply write all

134
00:07:48,020 --> 00:07:55,699
and if

135
00:07:51,240 --> 00:07:55,699
pseudo-regist Stage Studio register

136
00:07:56,340 --> 00:08:03,560
equals to one It means that it's called

137
00:07:59,960 --> 00:08:03,560
 from

138
00:08:05,660 --> 00:08:12,080
 call after

139
00:08:09,180 --> 00:08:12,080
memory

140
00:08:15,240 --> 00:08:18,240
foreign

141
00:08:21,020 --> 00:08:26,120
ification

142
00:08:23,060 --> 00:08:30,720
 so I just 

143
00:08:26,120 --> 00:08:33,860
created a small variables or I call

144
00:08:30,720 --> 00:08:33,860
it final value

145
00:08:34,080 --> 00:08:43,020
 final value is equal to a memory

146
00:08:39,020 --> 00:08:45,560
at this address we use dollar context

147
00:08:43,020 --> 00:08:45,560
here

148
00:08:45,600 --> 00:08:52,440
again here I want to show you the

149
00:08:50,100 --> 00:08:55,100
content of memory before it's being

150
00:08:52,440 --> 00:08:58,140
modified so

151
00:08:55,100 --> 00:09:03,180
I use 

152
00:08:58,140 --> 00:09:07,800
previous value which is again equals to

153
00:09:03,180 --> 00:09:12,260
DQ the same context

154
00:09:07,800 --> 00:09:12,260
and a simple princess

155
00:09:28,860 --> 00:09:34,339
or current value or the final value

156
00:09:44,580 --> 00:09:49,160
previous value is equal to

157
00:09:50,820 --> 00:10:00,320
and it's also worse to set the comment

158
00:09:54,660 --> 00:10:00,320
to avoid confusion it's called before

159
00:10:01,220 --> 00:10:09,240
so I think it's good let's run it into

160
00:10:05,459 --> 00:10:13,800
the target debuggee as you as you can see

161
00:10:09,240 --> 00:10:18,260
it's it keeps incrementing so I just

162
00:10:13,800 --> 00:10:18,260
put the image here

163
00:10:22,880 --> 00:10:30,260
 it said that 

164
00:10:26,279 --> 00:10:30,260
something is wrong and if

165
00:10:34,140 --> 00:10:41,240
oh before get to write a script

166
00:10:38,640 --> 00:10:41,240
foreign

167
00:10:51,079 --> 00:10:57,680
format so let's let's change it to 

168
00:10:58,860 --> 00:11:04,920
 decimal format because the target

169
00:11:01,800 --> 00:11:08,339
application is in decimal format and

170
00:11:04,920 --> 00:11:10,680
also after we show the final value we

171
00:11:08,339 --> 00:11:14,540
put the

172
00:11:10,680 --> 00:11:14,540
new line vector

173
00:11:17,640 --> 00:11:26,940
as you can see we could monitor the 

174
00:11:23,300 --> 00:11:30,120
previous value and the final value or

175
00:11:26,940 --> 00:11:33,019
the final modified value here

176
00:11:30,120 --> 00:11:33,019


177
00:11:33,480 --> 00:11:40,500
this guest goes to and sleep mode the

178
00:11:36,839 --> 00:11:46,279
monitor is turned off but it shows

179
00:11:40,500 --> 00:11:46,279
the previous value and the final value

180
00:11:46,339 --> 00:11:56,040
 no we could also perform some some

181
00:11:50,540 --> 00:11:59,820
tasks or some conditions for the 

182
00:11:56,040 --> 00:12:02,519
target values for example see a

183
00:11:59,820 --> 00:12:05,480
special value 

184
00:12:02,519 --> 00:12:05,480
foreign

185
00:12:07,040 --> 00:12:14,959
let's say we modified 

186
00:12:11,220 --> 00:12:14,959
here I copied here

187
00:12:15,300 --> 00:12:21,420
and

188
00:12:16,880 --> 00:12:25,740
if this process is modified if the 

189
00:12:21,420 --> 00:12:28,339
for example the target 

190
00:12:25,740 --> 00:12:34,079
final value

191
00:12:28,339 --> 00:12:34,079
is dividable by and

192
00:12:34,200 --> 00:12:41,579
and it's not equal to zero

193
00:12:39,959 --> 00:12:46,800
then

194
00:12:41,579 --> 00:12:49,279
we simply resets this value reset it to

195
00:12:46,800 --> 00:12:49,279
zero

196
00:12:56,279 --> 00:12:58,399


197
00:12:58,560 --> 00:13:03,620
we could use EB or EQ

198
00:13:04,320 --> 00:13:15,480
and set stack context to 

199
00:13:12,480 --> 00:13:15,480
zero

200
00:13:15,959 --> 00:13:20,300
and show a message here

201
00:13:21,959 --> 00:13:31,100
that indicates that we reset the account

202
00:13:26,519 --> 00:13:31,100
now let's test the diesel script

203
00:13:31,500 --> 00:13:34,160


204
00:13:46,279 --> 00:13:53,459
as we can see it's 

205
00:13:50,519 --> 00:13:59,579
it's not dividable by 10 because before

206
00:13:53,459 --> 00:14:03,260
forgot that it's based on 10 or 

207
00:13:59,579 --> 00:14:07,920
in in decimal format is based on 16 so

208
00:14:03,260 --> 00:14:10,320
you just need to change it to

209
00:14:07,920 --> 00:14:15,240


210
00:14:10,320 --> 00:14:17,639
 zero n or decimal format of 10

211
00:14:15,240 --> 00:14:21,200
because we want it to be divided by

212
00:14:17,639 --> 00:14:25,860
actual 10 not in the hexadecimal format

213
00:14:21,200 --> 00:14:29,120
so let's return to the

214
00:14:25,860 --> 00:14:29,120
yes they gain

215
00:14:35,459 --> 00:14:43,139
as you can see once it reaches to a 10

216
00:14:39,600 --> 00:14:47,279
 whenever the final value is 10 it

217
00:14:43,139 --> 00:14:53,220
keeps resetting this variable and we

218
00:14:47,279 --> 00:14:57,620
could see its effects on both the

219
00:14:53,220 --> 00:14:57,620
debugger and in the target debugging

