1
00:00:00,000 --> 00:00:05,339
the very important thing for a

2
00:00:02,340 --> 00:00:06,980
debugging memory is a stack heaps and

3
00:00:05,339 --> 00:00:10,200
pools

4
00:00:06,980 --> 00:00:12,080
see some of the commands related to

5
00:00:10,200 --> 00:00:15,480
this 

6
00:00:12,080 --> 00:00:16,980
examining the system step for example

7
00:00:15,480 --> 00:00:21,840
K command

8
00:00:16,980 --> 00:00:25,740
 just specifies back it works

9
00:00:21,840 --> 00:00:27,660
exactly like individually and it

10
00:00:25,740 --> 00:00:30,480
shows the stack vectors and the

11
00:00:27,660 --> 00:00:33,180
functions that are called before

12
00:00:30,480 --> 00:00:36,320
reaching to the current point I will

13
00:00:33,180 --> 00:00:40,079
show you an example later when we

14
00:00:36,320 --> 00:00:44,160
demonstrate the breakpoints but

15
00:00:40,079 --> 00:00:49,820
 this command 

16
00:00:44,160 --> 00:00:49,820
has has other variants like KQ

17
00:00:49,879 --> 00:00:58,199
also shows different stack back trace

18
00:00:54,960 --> 00:01:00,300
 in different way along with the

19
00:00:58,199 --> 00:01:04,040
parameters that are currently on the

20
00:01:00,300 --> 00:01:07,740
stack and will show whether there's a

21
00:01:04,040 --> 00:01:10,920
known function based on the symbols on

22
00:01:07,740 --> 00:01:15,540
the stack or not you can easily trace

23
00:01:10,920 --> 00:01:19,680
the the weight or the path that your

24
00:01:15,540 --> 00:01:22,140
function is called until reaching to the

25
00:01:19,680 --> 00:01:26,100
point that we are currently

26
00:01:22,140 --> 00:01:28,860
let's see an example of how we can use

27
00:01:26,100 --> 00:01:32,640
the color stack command to view the

28
00:01:28,860 --> 00:01:36,439
color stack here I put a breakpoint on

29
00:01:32,640 --> 00:01:36,439
its allocate pool with tag

30
00:01:39,119 --> 00:01:43,259
and after that you can see that the

31
00:01:41,400 --> 00:01:47,180
breakpoint 

32
00:01:43,259 --> 00:01:50,240
it so we are in

33
00:01:47,180 --> 00:01:51,799
this process VM tools

34
00:01:50,240 --> 00:01:55,979
sd.exe

35
00:01:51,799 --> 00:01:59,780
I press K command and you can see that

36
00:01:55,979 --> 00:02:02,700
 these 

37
00:01:59,780 --> 00:02:04,619
functions are called these functions are

38
00:02:02,700 --> 00:02:07,500
in the color stack available in the

39
00:02:04,619 --> 00:02:09,119
color stack in these locations from the

40
00:02:07,500 --> 00:02:13,140
start of the stack

41
00:02:09,119 --> 00:02:15,959
 and here we can see the color stack

42
00:02:13,140 --> 00:02:19,860
but we can also use it in other forms

43
00:02:15,959 --> 00:02:24,000
for example if we if we use it like

44
00:02:19,860 --> 00:02:29,459
KD then and it interprets the stack as a

45
00:02:24,000 --> 00:02:33,660
32-bit program or if we use KQ it's the

46
00:02:29,459 --> 00:02:37,319
same as this command it just shows

47
00:02:33,660 --> 00:02:40,200
 all of the content of the stack and

48
00:02:37,319 --> 00:02:43,739
it tries to convert all of the addresses

49
00:02:40,200 --> 00:02:46,140
that are located in the stack for

50
00:02:43,739 --> 00:02:47,599
example this this is a valid address

51
00:02:46,140 --> 00:02:51,120
here

52
00:02:47,599 --> 00:02:53,640
 and this is a target that is called

53
00:02:51,120 --> 00:02:56,340
from this function

54
00:02:53,640 --> 00:03:00,420
or this function is also known from

55
00:02:56,340 --> 00:03:02,459
symbols this way these are addresses for

56
00:03:00,420 --> 00:03:05,879
example this is a probably a memory

57
00:03:02,459 --> 00:03:09,840
address a valid memory address but

58
00:03:05,879 --> 00:03:15,140
these are address of the functions 

59
00:03:09,840 --> 00:03:19,860
so K Q shows their content of a stack

60
00:03:15,140 --> 00:03:24,440
we can also specify the size for example

61
00:03:19,860 --> 00:03:28,580
we want k q 

62
00:03:24,440 --> 00:03:28,580
 L 

63
00:03:28,739 --> 00:03:38,040
it's my I want to see this 

64
00:03:33,120 --> 00:03:40,319
size of this stack the the length

65
00:03:38,040 --> 00:03:42,540
of the memory that is retrieved from

66
00:03:40,319 --> 00:03:48,659
target debuggee

67
00:03:42,540 --> 00:03:51,659
and also you can't specify the base

68
00:03:48,659 --> 00:03:53,040
for the target module for example if you

69
00:03:51,659 --> 00:03:57,120
want to see

70
00:03:53,040 --> 00:04:00,540
 some addresses based on a special

71
00:03:57,120 --> 00:04:06,120
location for example let's do it based

72
00:04:00,540 --> 00:04:06,120
on RBP register

73
00:04:09,299 --> 00:04:17,040
I specify base here and in this time

74
00:04:13,500 --> 00:04:20,340
it's not based on rsp by default it's

75
00:04:17,040 --> 00:04:24,120
based on the rrsp or a stack pointer but

76
00:04:20,340 --> 00:04:25,440
currently we specify the RBP as the base

77
00:04:24,120 --> 00:04:27,840
pointer

78
00:04:25,440 --> 00:04:29,340
and you can specify all other RI versus

79
00:04:27,840 --> 00:04:32,280
as well

80
00:04:29,340 --> 00:04:33,680
yeah that's it that's enough for the

81
00:04:32,280 --> 00:04:37,380
call stack

82
00:04:33,680 --> 00:04:39,320
if you want to map Windows structures

83
00:04:37,380 --> 00:04:41,960
like EPROCESS

84
00:04:39,320 --> 00:04:47,160
to the

85
00:04:41,960 --> 00:04:51,300
structures for Windows we can use

86
00:04:47,160 --> 00:04:56,040
a DT command DT command just

87
00:04:51,300 --> 00:05:00,240
shows the structure for example if I

88
00:04:56,040 --> 00:05:04,160
want to see the E thread and if you just

89
00:05:00,240 --> 00:05:08,280
don't specify any other

90
00:05:04,160 --> 00:05:11,460
module name then but it just tries to

91
00:05:08,280 --> 00:05:14,419
see the empty module by default but it's

92
00:05:11,460 --> 00:05:17,940
better to specify

93
00:05:14,419 --> 00:05:22,139
 the module name like it's better for

94
00:05:17,940 --> 00:05:25,460
me to use an anti

95
00:05:22,139 --> 00:05:25,460
bang eater

96
00:05:32,600 --> 00:05:39,240
it's also okay if I if I want to use

97
00:05:36,139 --> 00:05:41,699
that process I I will use that process

98
00:05:39,240 --> 00:05:44,580
to find my current EPROCESS or that

99
00:05:41,699 --> 00:05:49,820
thread to find my content but for

100
00:05:44,580 --> 00:05:53,580
current EPROCESS I will map the

101
00:05:49,820 --> 00:05:56,820
EPROCESS to

102
00:05:53,580 --> 00:05:59,100
this address that's so

103
00:05:56,820 --> 00:06:03,919
it's just

104
00:05:59,100 --> 00:06:03,919
and you can see that we can

105
00:06:05,220 --> 00:06:12,060
we can see both the structure aligned

106
00:06:09,419 --> 00:06:16,320
with their data types their position in

107
00:06:12,060 --> 00:06:21,440
a bit in a bit

108
00:06:16,320 --> 00:06:25,020
bit vector of flag for example or

109
00:06:21,440 --> 00:06:28,280
distinctly related to daily process

110
00:06:25,020 --> 00:06:32,639
there are also some some of the

111
00:06:28,280 --> 00:06:35,400
 fields that are null or some of them

112
00:06:32,639 --> 00:06:37,819
that are for example a list entry which

113
00:06:35,400 --> 00:06:41,100
shows that basically the different

114
00:06:37,819 --> 00:06:43,620
components of at least entry

115
00:06:41,100 --> 00:06:46,500
and in different formats

116
00:06:43,620 --> 00:06:50,300
0 y means 

117
00:06:46,500 --> 00:06:54,319
in a binary format and also it

118
00:06:50,300 --> 00:06:57,600
interprets it as a hex 2.

119
00:06:54,319 --> 00:07:00,479
 also one of the things I forget to

120
00:06:57,600 --> 00:07:03,960
say that if you notice the HyperDbg

121
00:07:00,479 --> 00:07:05,819
signature is changed to the K HyperDbg

122
00:07:03,960 --> 00:07:07,319
K HyperDbg means that it's a

123
00:07:05,819 --> 00:07:09,240
kernel debugger relating to high

124
00:07:07,319 --> 00:07:12,300
protection and we are currently

125
00:07:09,240 --> 00:07:14,340
operating at the second core it means

126
00:07:12,300 --> 00:07:16,560
that hybrid Imaging is running and the

127
00:07:14,340 --> 00:07:20,580
stacking chlorine we can also change the

128
00:07:16,560 --> 00:07:23,220
course but it's not for this time in

129
00:07:20,580 --> 00:07:28,819
the future slide we see how we can

130
00:07:23,220 --> 00:07:28,819
change the currently executing code

