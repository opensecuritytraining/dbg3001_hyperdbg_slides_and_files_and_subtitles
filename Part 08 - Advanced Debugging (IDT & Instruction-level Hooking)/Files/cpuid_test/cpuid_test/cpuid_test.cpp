#include <iostream>
#include <Windows.h>
#include <intrin.h>

int main()
{
    int cpuInfo1[4] = { 0 };
    __cpuid(cpuInfo1, 0);

    printf("CPUID Results (EAX = 0):\nEAX: %x\nEBX: %x\nECX: %x\nEDX: %x\n\n\n", cpuInfo1[0], cpuInfo1[1], cpuInfo1[2], cpuInfo1[3]);

    int cpuInfo2[4] = { 0 };
    __cpuid(cpuInfo2, 1);

    printf("CPUID Results (EAX = 1):\nEAX: %x\nEBX: %x\nECX: %x\nEDX: %x\n", cpuInfo2[0], cpuInfo2[1], cpuInfo2[2], cpuInfo2[3]);

    return 0;
}