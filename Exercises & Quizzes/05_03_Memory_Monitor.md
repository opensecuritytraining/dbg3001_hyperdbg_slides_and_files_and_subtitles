## Memory Monitor

### Exercise List:

1. **Monitor any reads**
   Open the 'notepad.exe', and switch to the memory layout of the 'notepad.exe' process using the '.process' command. Set a '!monitor' to monitor any reads to the current process's PCB block (nt!\_EPROCESS). You can use the '$proc' pseudo-register.

2. **Create a list of functions that read/write on PCB**
   Print the 'RIP' register of instructions (from any functions) that READ/WRITE on the 'notepad.exe' PCB block.
   
3. **Add the offset of the modified places**
   Print the offset of the locations that are being modified from the start of the current 'nt!\_EPROCESS' by using the '$context' pseudo-register.
   
4. **Intercept execution**
   Compile the 'hello\_world' program (find the source code in the repo), and intercept any user-mode code execution from its main module.

### Links:

* [hello_world](https://url.hyperdbg.org/ost2/part-5-hello-world)

* [.process, .process2 (show the current process and switch to another process)](https://docs.hyperdbg.org/commands/meta-commands/.process)

* [!monitor (monitor read/write/execute to a range of memory)](https://docs.hyperdbg.org/commands/extension-commands/monitor)

* [printf](https://docs.hyperdbg.org/commands/scripting-language/functions/exports/printf)

* [Pseudo-registers](https://docs.hyperdbg.org/commands/scripting-language/assumptions-and-evaluations#pseudo-registers)

* [lm (view loaded modules)](https://docs.hyperdbg.org/commands/debugging-commands/lm)

* [Event short-circuiting](https://docs.hyperdbg.org/tips-and-tricks/misc/event-short-circuiting)