## Event Calling Stages

### Exercise List:

1. **Compile the program**
   Compile the 'write\_ignore' program (find the source code in the repo).
   
2. **View memory before and after modification**
   Use the '!monitor' command's calling stage to view the memory of the incrementing variable before and after the modification.
   
3. **Reset the value**
   Write a script and use the 'post' calling stage to reset the variable's value to 0 once it's reached a number dividable by 10.

### Links:

* [write_ignore](https://url.hyperdbg.org/ost2/write_ignore)

* [Part 10 - Codes](https://url.hyperdbg.org/ost2/part10-codes)

* [!monitor (monitor read/write/execute to a range of memory)](https://docs.hyperdbg.org/commands/extension-commands/monitor)

* [Event calling stage](https://docs.hyperdbg.org/tips-and-tricks/misc/event-calling-stage)