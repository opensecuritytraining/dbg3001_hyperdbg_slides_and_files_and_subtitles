## Variables & Assignment

### Exercise List:

1. **Create local and global variables**
   Use the '?' command to create local and global variables.

2. **Assign registers to variables**
   Assign general-purpose registers like 'RAX' and 'RCX' to local and global variables and view the results by using the 'print' command.

3. **Change registers**
   Change the value of the general-purpose registers by using the script engine assignment.

### Links:

* [Variables & Assignments](https://docs.hyperdbg.org/commands/scripting-language/variables-and-assignments)

* [? (evaluate and execute expressions and scripts in debuggee)](https://docs.hyperdbg.org/commands/debugging-commands/eval)

* [print (evaluate and print expression in debuggee)](https://docs.hyperdbg.org/commands/debugging-commands/print)