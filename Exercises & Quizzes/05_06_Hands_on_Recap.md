## Hands-on Recap

### Exercise List:

1. **Compile the 'flow\_control'**
   Compile the 'flow\_control' project (Find the corresponding files in the git repo).

2. **Change the flow**
   Use the '!epthook' command to change the flow of the running program (as described in the video).
   
3. **Enable and disable event**
   Enable and disable events using the 'events' command, and see how it changes the flow of the program. 

### Links:

* [flow_control](https://url.hyperdbg.org/ost2/flow_control)

* [x64dbg](https://x64dbg.com)

* [!epthook (hidden hook with EPT - stealth breakpoints)](https://docs.hyperdbg.org/commands/extension-commands/epthook)

* [events (show and modify active/disabled events)](https://docs.hyperdbg.org/commands/debugging-commands/events)