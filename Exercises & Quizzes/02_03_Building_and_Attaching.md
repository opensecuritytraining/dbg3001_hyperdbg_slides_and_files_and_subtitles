## Building and Attaching

### Exercise List:

1. **Download Windows SDK**
   Download Windows SDK and install WinDbg (Debugging Tools for Windows).

2. **Setup KDNET and connect WinDbg**
   Connect to WinDbg using the KDNET.

3. **Setup EfiGuard**
   Use EfiGuard as described in the video.

### Links:

* [Windows SDK](https://developer.microsoft.com/en-us/windows/downloads/windows-sdk)

* [Set up KDNET network kernel debugging manually](https://learn.microsoft.com/en-us/windows-hardware/drivers/debugger/setting-up-a-network-debugging-connection)

* [EfiGuard](https://github.com/Mattiwatti/EfiGuard)

* [How to Use EfiGuard to Disable PatchGuard](https://muffsec.com/blog/how-to-use-efiguard-to-disable-patchguard)

* [.connect (connect to a session)](https://docs.hyperdbg.org/commands/meta-commands/.connect)

* [.debug (prepare and connect to debugger)](https://docs.hyperdbg.org/commands/meta-commands/.debug)