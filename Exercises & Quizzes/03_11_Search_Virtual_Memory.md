## Searching Virtual Memory

### Exercise List:

1. **Search memory**
   Get the start and end address of the HyperDbg modules and search for the byte patterns that correspond to the "VMCALL" assembly instruction (0F 01 C1) within hprdbghv.dll's memory space.
   
2. **Check the results**
   Disassemble the memory at the found addresses to confirm the finds are correct.

### Links:

* [sb, sd, sq (search virtual memory)](https://docs.hyperdbg.org/commands/debugging-commands/s)

* [lm (view loaded modules)](https://docs.hyperdbg.org/commands/debugging-commands/lm)

* [u, u64, u2, u32 (disassemble virtual address)](https://docs.hyperdbg.org/commands/debugging-commands/u)