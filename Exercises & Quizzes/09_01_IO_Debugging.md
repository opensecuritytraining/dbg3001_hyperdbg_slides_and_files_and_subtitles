## I/O Debugging

### Exercise List:

1. **Create a keylogger**
   Intercept any keys pressed in the PS/2 keyboards by using the '!ioin' command. (As shown in the video).
   
2. **Modify keys**
   Use the conditional statements in the script engine to modify the pressed keys.

### Links:

* [!ioin (hook IN instruction execution)](https://docs.hyperdbg.org/commands/extension-commands/ioin)

* [!ioout (hook OUT instruction execution)](https://docs.hyperdbg.org/commands/extension-commands/ioout)

* [Conditionals & Loops](https://docs.hyperdbg.org/commands/scripting-language/conditionals-and-loops)

* [PS/2 Keyboard](https://wiki.osdev.org/PS/2_Keyboard)

* [Interrupts](https://wiki.osdev.org/Interrupts)