## Subsystems & Operation Modes

### Quizzes List:

1. In which operation mode should HyperDbg be operated for both local and remote debugging?

- HyperDbg should be operated in VMI Mode (Virtual Machine Introspection Mode) for both local and remote debugging.

2. Which mode should be used in HyperDbg if you want to halt the system and step in and out through the kernel instructions?

- Debugger Mode should be used in HyperDbg if you want to halt the system and step in and out through the kernel instructions.

3. What does the "Transparent Mode" in HyperDbg aim to achieve?

- The "Transparent Mode" in HyperDbg aims to make itself less detectable by anti-debugging and anti-hypervisor methods. While it doesn't guarantee complete transparency, it significantly increases the difficulty for these methods to detect the presence of the hypervisor and avoids revealing its actions during timing and microarchitectural attacks.

4. Can you provide an example of a HyperDbg command and explain its components?

Example of a HyperDbg command: !measure

!measure: This is the command itself. It's used to enable the Transparent Mode in HyperDbg.
When you run the !measure command, it triggers the Transparent Mode, making HyperDbg attempt to hide its presence from anti-debugging and anti-hypervisor methods. This command is one of the actions you can take to enhance the stealthiness of HyperDbg's operation.

### Links:

* [Operation Modes](https://docs.hyperdbg.org/using-hyperdbg/prerequisites/operation-modes)