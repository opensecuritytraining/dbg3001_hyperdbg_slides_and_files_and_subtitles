## Resource Sharing

### Exercise List:

1. **Share resource atomically**
   Use the debugger and try to make a critical section for accessing a variable in multi-core systems (modifying global variables from different cores).

### Links:

* [spinlock_lock](https://docs.hyperdbg.org/commands/scripting-language/functions/spinlocks/spinlock_lock)

* [spinlock_unlock](https://docs.hyperdbg.org/commands/scripting-language/functions/spinlocks/spinlock_unlock)