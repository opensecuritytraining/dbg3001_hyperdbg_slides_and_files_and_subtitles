## Managing Events

### Exercise List:

1. **Disable events**
   Use the 'events' command to disable the events that you configured on previous exercises.

2. **Disable events**
   Use the 'events' command to re-enable the events that you disabled previously.

3. **Clear events**
   Use the 'events' command to clear the events (completely remove events).

### Links:

* [events (show and modify active/disabled events)](https://docs.hyperdbg.org/commands/debugging-commands/events)