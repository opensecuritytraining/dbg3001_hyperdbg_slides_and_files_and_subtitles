## Hands-on Recap

### Exercise List:

1. **Compile the system-call-test project**
   Compile the 'system-call-test' project and get the process id, you can use the '.start' command to start this process.

2. **Trace and pause the debugger on the nt!NtCreateFile system-calls**
   Employ the '!syscall' command to intercept and pause the debugger once the system-call 'nt!NtCreateFile' is executed.

3. **Find the target path**
   Find the target path passed to the 'nt!NtCreateFile' system-call.
   
4. **Change the SYSRET results**
   Write a script and combine the '!syscall' and the '!sysret' events to find the SYSRET instruction and change the returned result (as shown in the video).

### Links:

* [system-call-test](https://url.hyperdbg.org/ost2/system-call-test)

* [scripts](https://url.hyperdbg.org/ost2/part-6-scripts)

* [!syscall, !syscall2 (hook system-calls)](https://docs.hyperdbg.org/commands/extension-commands/syscall)

* [!sysret, !sysret2 (hook SYSRET instruction execution)](https://docs.hyperdbg.org/commands/extension-commands/sysret)

* [printf](https://docs.hyperdbg.org/commands/scripting-language/functions/exports/printf)

* [.start (start a new process)](https://docs.hyperdbg.org/commands/meta-commands/.start)