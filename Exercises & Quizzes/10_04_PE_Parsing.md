## PE Parsing

### Exercise List:

1. **View Portable Executable (PE) headers and sections**
   Use the '.pe' command to view the headers and sections of a PE file.

### Links:

* [.pe (parse PE file)](https://docs.hyperdbg.org/commands/meta-commands/.pe)