## Customizing Builds

### Exercise List:

1. **Customize builds**
   Modify flags and definitions in the files linked below, customize different aspects of it, and rebuild your customized version of HyperDbg.

### Links:

* [Customize build](https://docs.hyperdbg.org/tips-and-tricks/misc/customize-build)

* [Build & Install](https://docs.hyperdbg.org/getting-started/build-and-install#download-and-install)