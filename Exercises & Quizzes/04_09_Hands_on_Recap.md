## Hands-on Recap

### Exercise List:

1. **Read registers**
   Read different registers using the 'r' command.

2. **Check address validity**
   Check the validity of addresses using the 'check\_address' function in the '?' command.
   
3. **Set breakpoint**
   Set a breakpoint on the 'nt!NtOpenFile' function by using the 'bp' command.

4. **Print the file names**
   Use the 'printf' function in the '?' command to print the names of files passed to this function (As shown in the video).

### Links:

* [r (read or modify registers)](https://docs.hyperdbg.org/commands/debugging-commands/r)

* [? (evaluate and execute expressions and scripts in debuggee)](https://docs.hyperdbg.org/commands/debugging-commands/eval)

* [bp (set breakpoint)](https://docs.hyperdbg.org/commands/debugging-commands/bp)

* [check_address](https://docs.hyperdbg.org/commands/scripting-language/functions/memory/check_address)

* [printf](https://docs.hyperdbg.org/commands/scripting-language/functions/exports/printf)

* [Conditionals & Loops](https://docs.hyperdbg.org/commands/scripting-language/conditionals-and-loops)

* [trace function calls](https://docs.hyperdbg.org/commands/scripting-language/examples/trace-function-calls)

* [printf](https://docs.hyperdbg.org/commands/scripting-language/functions/exports/printf)

* [NtOpenFile function (winternl.h)](https://learn.microsoft.com/en-us/windows/win32/api/winternl/nf-winternl-ntopenfile)