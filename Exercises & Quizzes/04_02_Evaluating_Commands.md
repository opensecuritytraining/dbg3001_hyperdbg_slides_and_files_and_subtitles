## Evaluating Commands

### Exercise List:

1. **Use printf**
   Use the '?' command and the 'printf' function to print different registers (e.g., `? printf("The RAX register is: %llx\n", @rax);` ).

2. **Change registers**
   Use the '?' command to change the 'RAX' register.

3. **View pseudo-registers**
   Use the '?' command and the 'printf' function to print the thread id ($tid).

### Links:

* [? (evaluate and execute expressions and scripts in debuggee)](https://docs.hyperdbg.org/commands/debugging-commands/eval)

* [Assumptions & Evaluations](https://docs.hyperdbg.org/commands/scripting-language/assumptions-and-evaluations)

* [printf](https://docs.hyperdbg.org/commands/scripting-language/functions/exports/printf)

* [print](https://docs.hyperdbg.org/commands/scripting-language/functions/exports/print)