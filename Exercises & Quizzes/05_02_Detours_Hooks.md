## Detours-style Hidden Hooks

### Exercise List:

1. **Use the '!epthook2'**
   As EPT hooks are hidden, use the detours-style hook of the HyperDbg, '!epthook2' and hook the 'nt!ExAllocatePoolWithTag', and show the parameters passed to this function by using the 'printf' function.

### Links:

* [!epthook2 (hidden hook with EPT - detours)](https://docs.hyperdbg.org/commands/extension-commands/epthook2)

* [printf](https://docs.hyperdbg.org/commands/scripting-language/functions/exports/printf)

* [API Hooking with MS Detours](https://www.codeproject.com/Articles/30140/API-Hooking-with-MS-Detours)